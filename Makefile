## Makefile for community detection code

CC = /home/zz124/gcc-4.8/bin/gcc
CXX = /home/zz124/gcc-4.8/bin/g++

NVCC := /usr/local/cuda/bin/nvcc
GENCODE_SM30 := -gencode arch=compute_30,code=sm_30
NVCC_FLAGS := $(GENCODE_SM30) -DUSE_32_BIT_GRAPH -lgomp

##OPTFLAGS = -Xcompiler -Ofast -Xcompiler -fopenmp -pg
OPTFLAGS = -Xcompiler -pg
## CFLAGS = -Ofast -fopenmp -pg
CFLAGS = -fopenmp -pg
CXXFLAGS = -std=c++0x -DUSE_32_BIT_GRAPH # -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC 
LIBS=-L/home/zz124/gcc-4.8/lib -L/home/zz124/gcc-4.8/lib64 -L/usr/local/cuda/lib64 -lcudart -I/usr/local/cuda/include

GOBJFILES = main.o buildNextPhase.o distanceOneVertexColoring.o loadBinary.o loadDimacs.o loadMetis.o loadSimple.o louvainMethod.o loadUtils.o RngStream.o timer.o louvainMethodC.o louvainMethod_kernels.o louvainMethod_kernels_e.o

GTARGET = graphClustering

all: $(GTARGET) 

tqsort: localSort.cu
	/usr/local/cuda/bin/nvcc -g -G -arch=sm_30 localSort.cu -o localSort

tqsort2: localSort.cu
	/usr/local/cuda/bin/nvcc -g -G -arch=sm_30 localSort.cu -o localSort2

$(GTARGET):  $(GOBJFILES) localSort_kernel.cu quickSort.cu bitonicSort.cu
	$(NVCC) $(NVCC_FLAGS) $(LIBS)  $(GOBJFILES) $(OPTFLAGS) test.cu  -o $@

#louvainMethodC.o: louvainMethodC.cpp
#	$(NVCC) $(NVCC_FLAGS) $(LIBS) $(CXXFLAGS) $^ $(OPTFLAGS) -o $@

%.o: %.c
	$(CXX) $(CFLAGS) $(LIBS) -c -o $@ $^

%.o: %.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(LIBS) -c -o $@ $^

%.o: %.cu
	$(NVCC) $(NVCC_FLAGS) -dc -o $@ $^


.PHONY: clean

clean:
	rm -f *~ $(GOBJFILES) $(GTARGET) *.o
