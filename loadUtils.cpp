#include <iostream>

#include "loadUtils.h"

void processGraphData(Graph &g, std::vector<GraphElem> &edgeCount,
		      std::vector<GraphElemPair> &edgeList,
		      const GraphElem nv, const GraphElem ne,
		      const std::vector<double> *weights)
{
  std::vector<GraphElem> ecTmp(nv + 1L);

  std::partial_sum(edgeCount.begin(), edgeCount.end(), ecTmp.begin());
  edgeCount = ecTmp;

  g.setEdgeStartForVertex(0L, 0L);

  for (GraphElem i = 0L; i < nv; i++)
    g.setEdgeStartForVertex(i + 1L, edgeCount[i + 1L]);

  edgeCount.resize(0L);

  auto ecmp = [] (const GraphElemPair &e0, const GraphElemPair &e1)
    { return ((e0.first < e1.first) ||
	      ((e0.first == e1.first) && (e0.second < e1.second))); };

  if (!std::is_sorted(edgeList.begin(), edgeList.end(), ecmp)) {
    std::cout << "Edge list is not sorted" << std::endl;
    std::sort(edgeList.begin(), edgeList.end(), ecmp);
  }
  else
    std::cout << "Edge list is sorted!" << std::endl;

  GraphElem ePos = 0L;

  for (GraphElem i = 0L; i < nv; i++) {
    GraphElem e0, e1;

    g.getEdgeRangeForVertex(i, e0, e1);
    if ((i % 100000L) == 0L)
      std::cout << "Processing edges for vertex: " << i << ", range(" << e0 << ", " << e1 <<
	")" << std::endl;

    for (GraphElem j = e0; j < e1; j++) {
      Edge &edge = g.getEdge(j);

      assert(ePos == j);
      assert(i == edgeList[ePos].first);
      edge.tail = edgeList[ePos].second;
      if (weights)
	edge.weight = (*weights)[ePos];
      else
	edge.weight = 1.0;

      ePos++;
    }
  }
} // processGraphData
