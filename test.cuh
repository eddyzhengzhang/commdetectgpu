#ifndef __TEST_H
#define __TEST_H

#include "graph.h"
#include "edge_decl.h"
#include <map>
#include <vector>

struct KeyValueComm {
  GraphElem commId;
  GraphElem cnt;

  bool operator == (const KeyValueComm &k0) const
  {
    return (this->commId == k0.commId);
  } //operator ==
};

typedef std::vector<GraphElem> GraphElemVector;
//typedef GraphElem* GraphElemVector;

typedef std::vector<KeyValueComm> ClusterLocalMapV;

extern "C" void test(CommVector &cupdate, GraphElemVector &clusterWeight, GraphElem nv, ClusterLocalMapV * clmapv, const Graph &g);

#endif 
