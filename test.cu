#include "test.cuh"
#include <stdio.h>

__global__ void kernel(Comm * cupdate, GraphElem * clusterWeight, GraphElem num, ClusterLocalMapV * clmapv, GraphElem * edgeList) {
	GraphElem threadID = threadIdx.x + blockIdx.x * blockDim.x;
	GraphElem e0, e1;
	for (GraphElem i = threadID; i < num; i += blockDim.x * gridDim.x) {
		clusterWeight[i] = 0L;
		cupdate[i].degree = 0L;
		cupdate[i].size = 0L;
		e0 = edgeList[i];
		e1 = edgeList[i+1];
		//printf("%d %d\n", e0, e1);
	}
}

extern "C" void test(CommVector &cupdate, GraphElemVector &clusterWeight, GraphElem nv, ClusterLocalMapV * clmapv, const Graph &g) { 
	/*for (GraphElem i = 0L; i < nv; i++) {
		clusterWeight[i] = 0L;
		cupdate[i].degree = 0L;
		cupdate[i].size = 0L;
	}*/

	Comm * d_cupdate = NULL;
	cudaMalloc(&d_cupdate, nv * sizeof(Comm));
	//cudaMemcpy(d_cupdate, &cupdate[0], cupdate.size() * sizeof(Comm), cudaMemcpyHostToDevice);

	GraphElem * d_clusterWeight = NULL;
	cudaMalloc(&d_clusterWeight, nv * sizeof(GraphElem));
	//cudaMemcpy(d_clusterWeight, &clusterWeight[0], clusterWeight.size() * sizeof(GraphElem), cudaMemcpyHostToDevice);

	ClusterLocalMapV * d_clmapv = NULL;
	cudaMalloc(&d_clmapv, sizeof(ClusterLocalMapV));

	//Graph * d_g = NULL;
	//cudaMalloc((void**)&d_g, sizeof(Graph));
	//cudaMemcpy(d_g, &g, sizeof(Graph), cudaMemcpyHostToDevice);

	if (!clmapv)
		clmapv = new ClusterLocalMapV;

	GraphElem * d_edgeList = NULL;
	cudaMalloc(&d_edgeList, nv * sizeof(GraphElem));
	//cudaMemcpy(d_edgeList, &(g.edgeListIndexes[0]), nv * sizeof(GraphElem), cudaMemcpyHostToDevice);
	kernel<<<4, 10>>>(d_cupdate, d_clusterWeight, nv, clmapv, d_edgeList);
	cudaDeviceSynchronize();
	GraphElem * temp1 = (GraphElem *)malloc(nv * sizeof(GraphElem));
	Comm * temp2 = (Comm*)malloc(nv * sizeof(Comm));
	//cudaMemcpy(&clusterWeight[0], d_clusterWeight, clusterWeight.size() * sizeof(GraphElem), cudaMemcpyDeviceToHost);
	//cudaMemcpy(&cupdate[0], d_cupdate, cupdate.size() * sizeof(Comm), cudaMemcpyDeviceToHost);
	cudaMemcpy(temp1, d_clusterWeight, nv * sizeof(GraphElem), cudaMemcpyDeviceToHost);
	cudaMemcpy(temp2, d_cupdate, nv * sizeof(GraphElem), cudaMemcpyDeviceToHost);
	//clusterWeight.assign(temp1, temp1+nv);
	//cupdate.assign(temp2, temp2+nv);
	cudaDeviceReset();
	cudaFree(d_cupdate);
	cudaFree(d_clusterWeight);
	cudaFree(d_edgeList);
	cudaFree(d_clmapv);
}
