#ifndef __LOAD_BINARY_H
#define __LOAD_BINARY_H

#include <string>

#include "graph.h"

void loadBinaryFile(Graph *&g, const std::string &fileName);

#endif // __LOAD_BINARY_H
