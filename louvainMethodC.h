#ifndef __LOUVAIN_METHODC_H
#define __LOUVAIN_METHODC_H

#include <map>
#include <unordered_map>
#include <vector>

#include "distanceOneVertexColoring.h"
#include "graph.h"

#include "louvainMethod.h"
struct KeyValueCommC {
  GraphElem commId;
  GraphElem cnt;
};

typedef std::vector<GraphElem> CommunityVector;
typedef std::vector<GraphElem> GraphElemVector;
typedef GraphElem* CommunityArray;
typedef GraphElem* GraphElemArray;
// typedef std::unordered_map<GraphElem, GraphElem> ClusterLocalMap; this one always commented out
//typedef std::map<GraphElem, GraphElem> ClusterLocalMap;
typedef std::vector<KeyValueComm> ClusterLocalMapV;
typedef KeyValueComm* ClusterLocalMapA;
typedef std::unordered_map<ColorElem, ColorElem> ColorReorderMap;

/*double louvainWithDistOneColoring(const Graph &g, CommunityVector &cvect, 
        ColorVector &colors, const ColorElem numColors,
        const double lower, const double thresh);*/
double louvainMethodC(const GraphStruct &g, CommunityArray &cvect, const double lower);

double louvainMethodGPU(const GraphStruct &g, CommunityArray &cvect, const double lower);
#endif //  __LOUVAIN_METHOD_H
