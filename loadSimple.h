#ifndef __LOAD_SIMPLE_H
#define __LOAD_SIMPLE_H

#include <string>

#include "graph.h"

void loadSimpleFile(Graph *&g, const std::string &fileName);

#endif // __LOAD_SIMPLE_H
