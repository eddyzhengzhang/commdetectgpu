#ifndef __EDGE_DECL_H
#define __EDGE_DECL_H

//#include <cstdint>
#include <iostream>

#include <vector>

#ifdef USE_32_BIT_GRAPH
typedef int32_t GraphElem;
typedef float GraphWeight;
#else
typedef int64_t GraphElem;
typedef double GraphWeight;
#endif

struct Edge {
  GraphElem tail;
  GraphWeight weight;

  Edge();
};

typedef std::vector<GraphElem> EdgeIndexes;
typedef GraphElem* EdgeIndexesArray;

inline Edge::Edge()
  : tail(-1), weight(0.0)
{
} // Edge

#endif // __EDGE_DECL_H
