#ifndef __LOAD_DIMACS_H
#define __LOAD_DIMACS_H

#include <string>

#include "graph.h"

void loadDimacsFile(Graph *&g, const std::string &fileName);

#endif // __LOAD_DIMACS_H
