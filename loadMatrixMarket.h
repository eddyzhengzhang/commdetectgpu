#ifndef __LOAD_MATRIX_MARKET_H
#define __LOAD_MATRIX_MARKET_H

#include <string>

#include "graph.h"

void loadMatrixMarketFile(Graph *&g, const std::string &fileName);

#endif // __LOAD_MATRIX_MARKET_H
