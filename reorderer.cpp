#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "graph.h"
#include "loadBinary.h"
#include "loadUtils.h"
#include "timer.h"

static bool metisTextPartition = false;
static std::string inputFileName, outputFileName, partFileName;

static void parseCommandLine(const int argc, char * const argv[]);
static void parsePartitionFile(std::vector<GraphElem> &parts, const std::string &partFileName,
			       const GraphElem nv);

int main(int argc, char *argv[])
{
  parseCommandLine(argc, argv);

  Graph *g = NULL;

  rusage rus;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  loadBinaryFile(g, inputFileName);

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Loaded graph from file: " << inputFileName << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  assert(g);

  double t0, t1, t2, t3;

  t0 = mytimer();
  std::ofstream ofs(outputFileName, std::ofstream::out | std::ofstream::binary |
		    std::ofstream::trunc);
  if (!ofs) {
    std::cerr << "Error opening output file: " << outputFileName << std::endl;
    exit(EXIT_FAILURE);
  }

  GraphElem nv, ne;

  nv = g->getNumVertices();
  ne = g->getNumEdges();

  t2 = mytimer();
  std::vector<GraphElem> parts(nv);
  int nparts;

  parsePartitionFile(parts, partFileName, nv);
  std::vector<GraphElem>::const_iterator miter = std::max_element(parts.begin(), parts.end());
  nparts = (*miter) + 1;
  t3 = mytimer();
  std::cout << "File read and copy time: " << (t3 - t2) << std::endl;

  t2 = mytimer();

  std::vector<GraphElem> nverts(nv), overts(nv);

  GraphElem gpos = 0;
  std::for_each(nverts.begin(), nverts.end(),
		[&gpos] (GraphElem &g) { g = gpos++; });

  std::sort(nverts.begin(), nverts.end(),
	    [&parts] (const GraphElem g0, const GraphElem g1)
	    { return ((parts[g0] < parts[g1]) || ((parts[g0] == parts[g1]) && (g0 < g1))); });

  gpos = 0;
  std::for_each(nverts.begin(), nverts.end(),
		[&overts, &gpos] (const GraphElem g)
		{ overts[g] = gpos++; });
  t3 = mytimer();
  std::cout << "Vertex sort time: " << (t3 - t2) << std::endl;

  t2 = mytimer();
  std::vector<GraphElem> edgeCount(nv + 1);
  std::vector<GraphElemPair> edgeList(ne);
  std::vector<GraphElem> vcnt(nparts);
  GraphElem epos = 0;

  for (GraphElem i = 0; i < nv; i++) {
    const GraphElem origV = nverts[i];
    GraphElem e0, e1;

    vcnt[parts[origV]]++;
    g->getEdgeRangeForVertex(origV, e0, e1);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = g->getEdge(j);
      GraphElemPair &nedge = edgeList[epos];

      nedge.first = i;
      nedge.second = overts[edge.tail];
      edgeCount[i + 1]++;
      epos++;
    }
  }
  delete g;

  Graph *greord = new Graph(nv, ne);
  processGraphData(*greord, edgeCount, edgeList, nv, ne);
  t3 = mytimer();
  std::cout << "Create new graph time: " << (t3 - t2) << std::endl;

  t2 = mytimer();
  std::cout << "Number of partitions to output: " << nparts << std::endl;
  for (int i = 0; i < nparts; i++)
    std::cout << "Vertices in partition " << i << ": " << vcnt[i] << std::endl;

  int32_t header;

  if ((nv < INT32_MAX) && (ne < INT32_MAX))
    header = 32;
  else
    header = 64;

  ofs.write(reinterpret_cast<char *>(&header), sizeof(header));

  ofs.write(reinterpret_cast<char *>(&nv), sizeof(nv));
  ofs.write(reinterpret_cast<char *>(&ne), sizeof(ne));

  std::vector<GraphElem> innerEdges(nparts), crossEdges(nparts);

  int ppos = 0;
  GraphElem vpos = 0;
  for (GraphElem v = 0; v < nv; v++) {
    GraphElem e0, e1;

    greord->getEdgeRangeForVertex(v, e0, e1);

    const int spart = parts[nverts[v]];

    if (spart != ppos) {
      std::cerr << "Incorrect partition for vertex: " << v << ", old id: " << nverts[v] <<
	", partition: " << spart << ", current partition: " << ppos << std::endl;
      std::cerr << "vertex position in partition: " << vpos << std::endl;
      std::cerr << "vcnt[ppos]: " << vcnt[ppos] << std::endl;
      std::cerr << "New id recomputed: " << overts[nverts[v]] << std::endl;
    }

    assert(spart == ppos);
    assert(v == overts[nverts[v]]);

    if (vpos == (vcnt[ppos] - 1)) {
      std::cout << "Processed partition " << ppos << std::endl;
      ppos++;
      vpos = 0;
    }
    else
      vpos++;

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = greord->getEdge(j);
      const int tpart = parts[nverts[edge.tail]];

      ofs.write(reinterpret_cast<char *>(&v), sizeof(v));
      ofs.write(reinterpret_cast<const char *>(&edge.tail), sizeof(edge.tail));

      if (spart == tpart)
	innerEdges[spart]++;
      else
	crossEdges[spart]++;
    }
  }
  assert(ppos = (nparts - 1));
  assert(vpos == 0);

  for (GraphElem v = 0; v < nv; v++) {
    const int spart = parts[nverts[v]];
    const char cspart = static_cast<char>(spart);

    assert(spart == cspart);
    ofs.write(&cspart, sizeof(cspart)); // Write one byte to represent the source partition
  }
  ofs.close();
  t3 = mytimer();
  std::cout << "Write new file time: " << (t3 - t2) << std::endl;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "After writing file" << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  delete greord;

  for (int i = 0; i < nparts; i++) {
    const double ied = innerEdges[i], ced = crossEdges[i];

    std::cout << "Partition: " << i << ", internal edges: " << innerEdges[i] <<
      ", cross edges: " << crossEdges[i] << ", percent: " << ((ced / ied) * 100.0) <<
      std::endl;
  }

  t1 = mytimer();
  std::cout << "Total processing time: " << (t1 - t0) << std::endl;

  return 0;
} // main

void parseCommandLine(const int argc, char * const argv[])
{
  int ret;

  while ((ret = getopt(argc, argv, "f:o:p:m")) != -1) {
    switch (ret) {
    case 'f':
      inputFileName.assign(optarg);
      break;
    case 'o':
      outputFileName.assign(optarg);
      break;
    case 'p':
      partFileName.assign(optarg);
      break;
    case 'm':
      metisTextPartition = true;
      break;
    default:
      assert(0 && "Should not reach here!!");
      break;
    }
  }

  if (inputFileName.empty()) {
    std::cerr << "Must specify an input file name with -f" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (outputFileName.empty()) {
    std::cerr << "Must specify an output file name with -o" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (partFileName.empty()) {
    std::cerr << "Must specify a partition file name with -p" << std::endl;
    exit(EXIT_FAILURE);
  }
} // parseCommandLine

void parsePartitionFile(std::vector<GraphElem> &parts, const std::string &partFileName,
			const GraphElem nv)
{
  std::ifstream ifs;

  if (metisTextPartition)
    ifs.open(partFileName, std::ifstream::in);
  else
    ifs.open(partFileName, std::ifstream::in | std::ifstream::binary);

  if (!ifs) {
    std::cerr << "Error opening partition file: " << partFileName << std::endl;
    exit(EXIT_FAILURE);
  }

  int *iparts = new int[nv];
  if (!metisTextPartition) {
    ifs.read(reinterpret_cast<char *>(iparts), sizeof(iparts[0]) * nv);
    ifs.close();
  }
  else {
    int part;
    GraphElem pos = 0;

    do {
      ifs >> part;
      if (pos < nv)
	iparts[pos] = part;

      if (pos < 10 || pos > (nv - 10))
	std::cout << "Vertex pos: " << pos << ", partition: " << part << std::endl;

      if (!ifs.eof())
	pos++;
    } while (ifs && !ifs.eof());

    std::cout << "Final position: " << pos << ", nv: " << nv << std::endl;
    assert(pos == nv);
  }

  std::copy(iparts, iparts + nv, parts.begin());
  delete [] iparts;
} // parsePartitionFile
