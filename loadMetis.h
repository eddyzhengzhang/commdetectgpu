#ifndef __LOAD_METIS_H
#define __LOAD_METIS_H

#include <string>

#include "graph.h"

void loadMetisFile(Graph *&g, const std::string &fileName);

#endif // __LOAD_METIS_H
