#define MAXPAD 2147483647

__device__ inline void swap(SortVal & a, SortVal & b)
{
	// Alternative swap doesn't use a temporary register:
	// a ^= b;
	// b ^= a;
	// a ^= b;
	
    SortVal tmp = a;
    a = b;
    b = tmp;
}

__device__ inline void swapKey(SortKey & a, SortKey & b)
{
	// Alternative swap doesn't use a temporary register:
	// a ^= b;
	// b ^= a;
	// a ^= b;
	
    SortKey tmp = a;
    a = b;
    b = tmp;
}

__device__ void mergeSort(int beg, int end, SortVal * values, SortKey * keys, SortVal * values_smem, SortKey * keys_smem, int * vmask, SortVal * v_aux, SortKey * k_aux)
{ //merge sort
		int length = end - beg;
		int tid = threadIdx.x;
		int warplane = (threadIdx.x&31);
		int warpid = (threadIdx.x>>5);
		keys_smem[tid] = MAXPAD;
		vmask[tid] = -1;
		keys_smem[tid] = MAXPAD;		

		int start;
		int power2 = 3;
		int real_range;
		unsigned int temp = end-beg;
		temp = __brev(temp);
	  real_range = (1 << (32 - __ffs(temp) + 1));
		real_range = (real_range > 32) ? 32:real_range;



		if ( warplane < length )
		{ //place everything into shared memory
			values_smem[tid] = values[beg + warplane];
			keys_smem[tid] = keys[values_smem[tid]];
		}

		if ( real_range >= 2 )
		{
			int tempidx = 0;
			int baseidx = ((tid>>1)<<1);
			int compidx = baseidx + (1-(tid&1));
			int equal = 0;
			
			equal = (keys_smem[tid] == keys_smem[compidx]);
			tempidx = (keys_smem[tid] > keys_smem[compidx]);
			if (( tid&1) && equal) tempidx += 1;
			/*if ( tempidx < 0 || tempidx > 1 )*/
			/*if ( warplane == 0 )*/
			/*printf("Debug i: error tempidx %d baseidx %d warpid %d\n", tempidx, baseidx, warpid);*/
			SortKey temp1 = keys_smem[tid];
			SortVal temp2 = values_smem[tid];
			keys_smem[baseidx+tempidx] = temp1;
			values_smem[baseidx+tempidx] = temp2; 
		}
		vmask[tid] = -1;
				

		/*if ( blockIdx.x == 0 && tid == 0 )*/
		/*printf("beg %d end %d\n", beg, end);*/

				/*printf("Debugi: tid %d keys_smem[idx] %d vmask[%d] %d start %d direction %d baseidx %d keys_comp %d keys_self %d i %d power2 %d\n", tid, keys_smem[tid], tid, vmask[tid], start, direction, baseidx, keys_smem[baseidx+i/2+start-1], keys_smem[tid], i, power2);*/


		if ( real_range >= 4 )
		{
			int baseidx = ((tid>>2)<<2);
			int oidx = (tid&1);
			if ( tid < baseidx + 2 )
			{
				int comp = (keys_smem[tid] >= keys_smem[baseidx + 2]);
				oidx += comp;
				comp = (keys_smem[tid] >= keys_smem[baseidx + 3]);
				oidx += comp;
				/*if ( oidx > 3 || oidx < 0 )*/
				/*{*/
				/*if ( warplane == 0 )*/
				/*printf("Debugi: error, oidx %d, should be at most %d warpid %d\n", oidx,3, warpid);*/
				/*}*/
				oidx = baseidx + oidx;
				vmask[oidx] = tid;	
			}
					
			unsigned int mask = __ballot((vmask[tid] == -1));
			mask = __brev(mask);
			int nidx;
			if ( vmask[tid] == -1 )
			{
				nidx = __popc(mask >> (32 - warplane) );
				oidx = nidx + (((nidx>>1)+1)<<1) + (warpid << 5);
				/*if ( oidx > baseidx + 3 || oidx < baseidx )*/
				/*if ( warplane == 0 )*/
				/*printf("Debugi: error, oidx %d baseidx %d + 3 warpid %d nidx %d\n", oidx, baseidx, warpid, nidx);*/

				vmask[tid] = oidx;
			}

			SortKey temp1 = keys_smem[vmask[tid]];
			SortVal temp2 = values_smem[vmask[tid]];
			keys_smem[tid] = temp1;
			values_smem[tid] = temp2; 

		}

		/*printf("warplane %d idx %d key %d\n", warplane, values_smem[tid], keys_smem[tid]);*/
		

		
		for ( int i = 8; i <= real_range; i *= 2 )
		{
								//another loop to iterate over power of 2, which is on searching
				//the final combination of i/2 + i/4 + ...
				//then calculate the mask
				//then give indices to the other half 
				start = 0;
				int baseidx = ((tid>>power2)<<power2);
				int j;
				int direction = 1;
				vmask[tid] = -1;
				if ( tid < baseidx + i/2 ) {
					for ( j = i/2; j > 1; j /= 2 )
					{	
						start += (j/2*direction);
						direction = ( keys_smem[tid] >= keys_smem[baseidx + i/2 + start-1] );
						direction = 2*direction - 1;
					/*if ( tid == 0 ) printf("start %d, direction %d, j %d, i %d\n", start, direction, j, i);*/
					}
					if (direction < 0) start += direction;
					int largest = ( keys_smem[tid] >= keys_smem[baseidx + i - 1]);
					start += largest;
					start += (tid&((1<<(power2-1))-1));
					vmask[baseidx + start] = tid;
					/*printf("vmask[%d] %d\n", baseidx+start, vmask[baseidx+start]);*/
					/*if ( start < 0 || start >= i )*/
					/*if ( warplane == 0 )*/
					/*printf("Debugi: error start %d i %d warpid %d base idx %d\n", start, i,warpid, baseidx);*/
				}
	

 
				unsigned int mask =__ballot(vmask[tid] == -1); 
				mask = __brev(mask);
				int newidx = __popc(mask>>(32-warplane));
				if ( vmask[tid] == -1 )
				{
						start = (newidx&((1<<(power2-1))-1)) + (( (newidx>>(power2-1)))<<(power2));
						vmask[tid] = start + warpid*32 + i/2; 
						/*if ( start < 0 || start >= i )*/
						/*printf("Debugi: start %d i %d warpid %d warplane %d baseidx %d vmask[%d] %d\n", start, i, warpid, warplane, baseidx, tid, vmask[tid]);*/

				}
				SortKey temp1 = keys_smem[vmask[tid]];
				SortVal temp2 = values_smem[vmask[tid]];
				keys_smem[tid] = temp1;
				values_smem[tid] = temp2; 
				
				power2++;
				/*if ( beg == 16384 && i == 8 )*/
				/*printf("Debugi: tid %d keys_smem[idx] %d vmask[%d] %d keys_self %d beg %d end %d\n", tid, keys_smem[tid], tid, vmask[tid],  keys_smem[tid], beg, end);*/

	
		}
		
		/*if ( warplane < length-1 && warplane < 31 && keys_smem[tid] > keys_smem[tid+1])*/
		/*printf("Debugi: Error; sequence not sorted tid %d keys1 %d keys2 %d beg %d end %d warplane %d\n", tid, keys_smem[tid], keys_smem[tid+1], beg, end, warplane);*/

		if ( warplane < length )
		{ //place everything into shared memory
			values[beg + warplane] = values_smem[tid];
		}

}

__device__ static void bitonicSort(int beg, int end, SortVal * values, SortKey * keys, SortVal * values_smem, SortKey * keys_smem)
{

    const int tid = threadIdx.x;
		int warplane = threadIdx.x&31;
		int warpid = threadIdx.x>>5;
		int orgkey = keys_smem[tid];
		keys_smem[tid] = MAXPAD;
		int real_range;
		unsigned int temp = end-beg;
		temp = __brev(temp);
	  real_range = (1 << (32 - __ffs(temp) + 1));
		real_range = (real_range > 32) ? 32:real_range;
		/*if ( real_range > 32 ) {*/
		/*if ( warplane == 0 )*/
		/*{*/
		/*printf("Debugi: real_range %d beg %d end %d temp %u __ffs(temp) %d\n", real_range, beg, end, temp, __ffs(temp));*/
		/*}*/
		/*return;*/
		/*}*/
		/*if ( tid == 0 )*/
		/*printf("real_range: %d, end: %d, beg: %d, ffs: %d\n", real_range, end, beg, __ffs(end-beg));*/

		/*if ( warplane == 0 )*/
		/*printf("Debugi : sorting %d to %d, real_range %d\n", beg, end, real_range);*/

    // Copy input to shared mem.
    // 	
    if ( warplane < (end - beg) ) 
		{
			SortVal temp = values[beg+warplane];
			values_smem[tid] = temp;
			keys_smem[tid] = keys[temp];
			/*if ( warpid == 3 ) printf("Before sort: %d vs: %d, ks: %d\n", tid, values_smem[tid], keys_smem[tid]);*/
		}

    // Parallel bitonic sort.
    for (int k = 2; k <= real_range; k *= 2)
    {
        // Bitonic merge:
        for (int j = k / 2; j>0; j /= 2)
        {
            int ixj = warplane ^ j;
            
            if (ixj > warplane)
            {
                if ((warplane & k) == 0)
                {
                    if (keys_smem[warplane + warpid*32] > keys_smem[ixj + warpid*32 ])
                    {
                        swap(values_smem[warplane + warpid*32], values_smem[ixj + warpid*32]);
												swapKey(keys_smem[warplane + warpid*32], keys_smem[ixj + warpid*32]);
                    }
                }
                else
                {
                    if (keys_smem[warplane + warpid*32] < keys_smem[ixj + warpid*32])
                    {
                        swap(values_smem[warplane + warpid*32], values_smem[ixj + warpid*32]);
												swapKey(keys_smem[warplane + warpid*32], keys_smem[ixj + warpid*32]);
                    }
                }
            }   
        }
				
    }

   	// Copy back to global mem.
    // 	
    if ( warplane < (end - beg) ) 
		{
			values[beg+warplane] = values_smem[tid];
			/*if (warpid == 3 ) printf("After sort warp3, %d, beg, %d, %d\n", values[beg+warplane], beg, warplane);*/
			/*printf("Debugi: %d %d %d\n", threadIdx.x, values[beg+warplane], keys[values[beg+warplane]]); */
		}
		keys_smem[tid] = orgkey;
}

