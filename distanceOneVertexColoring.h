#ifndef __DISTANCEONEVERTEXCOLORING_H
#define __DISTANCEONEVERTEXCOLORING_H

#include <iostream>

#include <vector>

#include "graph.h"

#ifdef USE_32_BIT_GRAPH
typedef int32_t ColorElem;
#else
typedef int64_t ColorElem;
#endif

typedef std::vector<ColorElem> ColorVector;

ColorElem distanceOneVertexColoring(const Graph &g, ColorVector &colors);

#endif // __DISTANCEONEVERTEXCOLORING_H
