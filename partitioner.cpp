#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>

#include <patoh.h>

#include "graph.h"
#include "loadBinary.h"
#include "timer.h"

static std::string inputFileName, outputFileName;

static int numPartitions = 1;

static void parseCommandLine(const int argc, char * const argv[]);

int main(int argc, char *argv[])
{
  parseCommandLine(argc, argv);

  Graph *g = NULL;

  rusage rus;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  loadBinaryFile(g, inputFileName);

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Loaded graph from file: " << inputFileName << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;
  std::cout << "Will partition into: " << numPartitions << " partitions" << std::endl;

  assert(g);

  double t0, t1;

  t0 = mytimer();
  std::ofstream ofs(outputFileName, std::ofstream::out | std::ofstream::binary |
		    std::ofstream::trunc);
  if (!ofs) {
    std::cerr << "Error opening output file: " << outputFileName << std::endl;
    exit(EXIT_FAILURE);
  }

  GraphElem nv, ne;

  nv = g->getNumVertices();
  ne = g->getNumEdges();

  int *xpins, *pins, *cwghts, *partvec, *partweights, cut;

  int64_t ne2 = static_cast<int64_t>(ne) * 2L;

  if (ne2 >= std::numeric_limits<int32_t>::max()) {
    std::cerr <<
      "Error: twice the number of edges is greater than PaToH's 32-bit representation: " <<
      ne2 << ", limit: " << std::numeric_limits<int32_t>::max() << std::endl;
    exit(EXIT_FAILURE);
  }

  xpins = new int[ne + 1];
  pins = new int[ne * 2];
  cwghts = new int[nv * 2];
  partvec = new int[nv];
  partweights = new int[numPartitions * 2];

  std::for_each(cwghts, cwghts + (nv * 2),
		[] (int &w) { w = 1; });

  int epos = 0;
  for (GraphElem i = 0; i < nv; i++) {
    GraphElem e0, e1;

    g->getEdgeRangeForVertex(i, e0, e1);
    cwghts[i * 2 + 1] = (e1 - e0);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = g->getEdge(j);

      xpins[j] = epos;
      pins[epos] = i;
      pins[epos + 1] = edge.tail;
      epos += 2;
    }
  }
  xpins[ne] = epos;

  PaToH_Parameters pargs;

  double t2, t3;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Before PaToH calls" << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  t2 = mytimer();
  PaToH_Initialize_Parameters(&pargs, PATOH_CONPART, PATOH_SUGPARAM_DEFAULT);

  pargs._k = numPartitions;
  PaToH_Check_User_Parameters(&pargs, 1);
  PaToH_Alloc(&pargs, nv, ne, 2, cwghts, NULL, xpins, pins);
  PaToH_Part(&pargs, nv, ne, 2, 0, cwghts, NULL, xpins, pins, NULL, partvec, partweights,
	     &cut);
  t3 = mytimer();
  std::cout << "PaToH time: " << (t3 - t2) << std::endl;

  for (int i = 0; i < (numPartitions * 2); i += 2)
    std::cout << "Partition[" << (i / 2) << "] weight: " << partweights[i] << ", " <<
      partweights[i + 1] << std::endl;

  t2 = mytimer();
  ofs.write(reinterpret_cast<char *>(partvec), sizeof(partvec[0]) * nv);
  ofs.close();
  t3 = mytimer();
  std::cout << "Partition file writing time: " << (t3 - t2) << std::endl;
  delete g;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "After writing file" << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  delete [] xpins;
  delete [] pins;
  delete [] cwghts;
  delete [] partvec;
  delete [] partweights;
  PaToH_Free();

  t1 = mytimer();
  std::cout << "Time partitioning graph: " << (t1 - t0) << std::endl;

  return 0;
} // main

void parseCommandLine(const int argc, char * const argv[])
{
  std::string numPartitionsString;
  int ret;

  while ((ret = getopt(argc, argv, "f:o:p:")) != -1) {
    switch (ret) {
    case 'f':
      inputFileName.assign(optarg);
      break;
    case 'o':
      outputFileName.assign(optarg);
      break;
    case 'p':
      numPartitionsString.assign(optarg);
      break;
    default:
      assert(0 && "Should not reach here!!");
      break;
    }
  }

  if (inputFileName.empty()) {
    std::cerr << "Must specify an input file name with -f" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (outputFileName.empty()) {
    std::cerr << "Must specify an output file name with -o" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (!numPartitionsString.empty()) {
    std::istringstream iss(numPartitionsString);

    iss >> numPartitions;

    if (!iss) {
      std::cerr << "Number of partitions must be a number, input was: " <<
	numPartitionsString << std::endl;
      exit(EXIT_FAILURE);
    }
  }
} // parseCommandLine
