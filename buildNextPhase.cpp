#include <cassert>

#include <iostream>
#include <numeric>

#include <omp.h>

#include "buildNextPhase.h"

GraphElem renumberClustersContiguously(CommunityVector &cvect)
{
  const size_t sz = cvect.size();

  ClusterLocalMap clmap;
  ClusterLocalMap::const_iterator storedAlready;  
  GraphElem numUniqueClusters = 0L;

  for (CommunityVector::iterator iter = cvect.begin(); iter != cvect.end(); iter++) {
    if ((*iter >= 0L) && (*iter < sz)) {
      storedAlready = clmap.find(*iter);

      if (storedAlready != clmap.end())
	*iter = storedAlready->second;
      else {
	clmap[*iter] = numUniqueClusters;
	*iter = numUniqueClusters;
	numUniqueClusters++;
      }
    }
  }

  return numUniqueClusters;
} // renumberClustersContigously

void buildNextLevelGraph(const Graph &gin, Graph *&gout, const CommunityVector &cvect,
			 const GraphElem numUniqueClusters)
{
  const GraphElem nvin = gin.getNumVertices();
  const GraphElem nein = gin.getNumEdges();

  GraphElem nvout = numUniqueClusters;
  GraphElem neout = 0L;
  GraphElemVector verticesOut(nvout + 1L);
  std::vector<ClusterLocalMap> cluin;
  std::vector<omp_lock_t> locks;

  cluin.resize(numUniqueClusters);
  locks.resize(numUniqueClusters);

  for (GraphElem i = 0L; i < numUniqueClusters; i++)
    omp_init_lock(&locks[i]);

#pragma omp parallel for default(none), shared(cluin, gin, cvect, verticesOut, locks), reduction(+: neout), schedule(guided)
  for (GraphElem i = 0L; i < nvin; i++) {
    GraphElem e0, e1;
    ClusterLocalMap::iterator localIterator;

    gin.getEdgeRangeForVertex(i, e0, e1);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = gin.getEdge(j);
      const GraphElem ci = cvect[i], ct = cvect[edge.tail];

      // assert((ci < numUniqueClusters) && (ct < numUniqueClusters));

      if (ci >= ct) {
	omp_set_lock(&locks[ci]);

	ClusterLocalMap &clci = cluin[ci];

	localIterator = clci.find(ct);

	if (localIterator != clci.end()) {
	  localIterator->second += edge.weight;
	  omp_unset_lock(&locks[ci]);
	}
	else {
	  clci[ct] = edge.weight;
	  omp_unset_lock(&locks[ci]);

#pragma omp atomic update
	  verticesOut[ci + 1]++;

	  if (ci > ct) {
	    neout++;
#pragma omp atomic update
	    verticesOut[ct + 1]++;
	  }
	}
      }
    }
  }

  for (GraphElem i = 0L; i < numUniqueClusters; i++)
    omp_destroy_lock(&locks[i]);

  {
    GraphElemVector voutTmp(nvout + 1L);

    std::partial_sum(verticesOut.begin(), verticesOut.end(), voutTmp.begin());
    verticesOut = voutTmp;
  }

  //cbe
  //std::cout << "End Structure " << neout << " " << nvout << " vs " << verticesOut[nvout] <<
  //  std::endl;

  GraphElem numEdges = verticesOut[nvout];
  GraphElem realEdges = numEdges - neout;
  GraphElemVector added(nvout);

  gout = new Graph(nvout, numEdges);

  gout->setEdgeStartForVertex(0, verticesOut[0]);

#pragma omp parallel for default(none), shared(cluin, gout, verticesOut, added), firstprivate(nvout), schedule(guided)
  for (GraphElem i = 0L; i < nvout; i++) {
    GraphElem where;
    const ClusterLocalMap &cli = cluin[i];

    gout->setEdgeStartForVertex(i + 1L, verticesOut[i + 1L]);

    for (ClusterLocalMap::const_iterator iter = cli.begin(); iter != cli.end(); iter++) {
#pragma omp atomic capture
      where = added[i]++;

      where += verticesOut[i];

      Edge &edge = gout->getEdge(where);

      edge.tail = iter->first;
      edge.weight = iter->second;

      const GraphElem ifirst = iter->first;

      if (i != ifirst) {
#pragma omp atomic capture
	where = added[ifirst]++;

	where += verticesOut[ifirst];

	Edge &edge = gout->getEdge(where);

	edge.tail = i;
	edge.weight = iter->second;
      }
    }
  }
} // buildNextLevelGraph
