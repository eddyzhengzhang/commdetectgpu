#define WARPSIZE 32

__device__ int quickSortPart(int start, int end, SortVal * A, SortKey * K, SortVal * A_smem, SortKey * K_smem)
{	
	int warplane = (threadIdx.x&31);
	int warpbase = ((threadIdx.x>>5)<<5); // /32*32
	int taskid = start + warplane;
	SortKey pvalue = K[A[end - 1]]; // use the first one as pivot
	int pidx = start;
	int newstart = start;
	for ( int i = taskid; i < end - 1; i += WARPSIZE) //warpsize
	{
		A_smem[threadIdx.x] = A[i];
		/*K_smem[threadIdx.x] = K[A[i]];*/
		SortVal t1= A_smem[threadIdx.x];
		SortKey t2= K[A[i]];
		unsigned int lt = __ballot(t2 <= pvalue);
		lt = __brev(lt);
		int ltcount = __popc(lt); // how many values < pivot value
		int ltidx = __popc(lt>>(32-warplane)); // there is a problem with ballot of the elements that are not running, need to fix it
		unsigned int gt =__ballot(t2 > pvalue);
		gt = __brev(gt);
		int gtidx = __popc(gt>>(32-warplane));
		gtidx = (ltcount) + gtidx;
		int realid = gtidx;
		if ( t2 <= pvalue ) realid = ltidx;
		A_smem[warpbase + realid] = t1; //caveat, here we used the property of in-warp sync
		/*printf("Before: i%d %d realid %d k[a[i]] %d lt %u t2 %d\n", i, A[i], realid, K[A[i]], lt, t2);*/
		A[i] = A_smem[threadIdx.x];
		/*printf("After: i%d %d\n", i, A[i]);*/
		/*if ( warplane == 0 )*/
		/*printf("Debugi: ltcount: %d, pidx: %d, beg: %d, end: %d, pivot: %d, newstart: %d, lt: %u, i:%d, ltidx: %d\n", ltcount, pidx, newstart, end, pvalue, newstart, lt, i,realid);*/
		/*printf(" K[A[%d]] %d tid %d", i, K[A[i]], threadIdx.x);*/
		if ( (pidx + ltcount) <= newstart)
	 { //toopt, this part can be further optimized with the use of a larger shared memory array
			// so that we perform swapping at shared memory and flush it back all at
			// once to reduce divergence
			if (warplane < ltcount)
			{
				t1 = A[pidx + warplane];
				A[pidx + warplane] = A_smem[threadIdx.x];
				A[i] = t1;
				/*printf("Debug: if you see me there is an error\n");*/
			}
		}
		else 
		{
			if (warplane < ((newstart - pidx)))
			{
				t1 = A[pidx + warplane];
				A[pidx + warplane] = A_smem[ltcount - (newstart-pidx) + threadIdx.x];
				A[i + (ltcount-(newstart-pidx))] = t1; 
				/*printf("Debug: if you see me there is an error\n");*/
			}
		}
		pidx += ltcount;
		newstart += WARPSIZE;
	}
	if ( warplane == 0 )
	{
		swap(A[end-1], A[pidx]);		
	}
	return pidx;
}

__device__ void popsize(int & size)
{
	size--;
}

__device__ void pushsize(int & size)
{
	size++;
}

__device__ void pushstack(int * qstack, int idx, int val)
{
	qstack[idx] = val;
	/*if ( threadIdx.x == 0 )*/
	/*printf("idx: %d size: %d ", idx, size);*/
} 

	//a thread warp warp processes a range
	//iterate over the interval
	//bitmask, find all elements that are smaller, if no more, move on
	//swap all elements that are smaller with the first n elements after pivot
	//if it is the first interval, then just do bisection
	//if it is the second interval, do swapping
	//what if starting from pivot there are not enough large elements to swap
	//with, then some elements do not need to be swapped, we only need to
	//calculate until where to swap, the number should be total number of to-swap
	//elements starting from the pivot point, should get a mask from there, then
	//for the remaining to-swap elements, we swap them with the ones before. Can
	//potentially split this into two phases
	//check if pivot + to-swap > start, if so, get the number where, then only
	//swap starting from there, only get from swap there. Put the locations into
	//shared memory array, then swap, then write back, simply atomic might be fine
	//
	//now after we get pivot point, push into stack, perform partition again, push
	//to stack until range is small, then pop out of stack, perform partition,
	//push into stack  
__device__ void quickSort(int start, int end, SortVal * A, SortKey * K, SortVal * A_smem, SortKey * K_smem, int * qstack, int qstacksize, int * vmask, SortVal * A_aux, SortKey * K_aux)
{
	unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	int * qsstack = (int*) &qstack[warpid*qstacksize];
	int tbeg = start; 
	int tend;
	int iwarpid = (threadIdx.x>>5); //warpid within the thread block
	int stpt = iwarpid*32;
	int pivot =0;
	if ( warplane == 0 ) {
		 K_smem[stpt] = 0; //initialize stack size variable before sorting starts
	   K_smem[stpt] += 2; 
	} // only one thread in the warp can update the stack size
	int stacksize = K_smem[stpt];
	if ( warplane == 0 )
	{ 
		pushstack(qsstack, stacksize - 2, start);
		pushstack(qsstack, stacksize - 1, end); //is qstack pointer determined here? 
	}
	/*if (warplane == 0) */
	/*printf("Debugi: stacksize %d\n", stacksize);*/
	/*int debugcount = 0;*/
	/*while ( (debugcount <= 80) && stacksize >= 2 )*/
	while ( K_smem[stpt] >= 2 )
	{
		/*debugcount++;*/
		/*popsize(stacksize);*/
		/*popsize(stacksize);*/
		if ( warplane == 0 )
			K_smem[stpt] -= 2;
	  tbeg = qsstack[K_smem[stpt]];
		tend = qsstack[K_smem[stpt]+1];		
		/*if ( warplane == 0 && warpid == 0 )*/
		/*printf("Debugi: o debug tbeg: %d tend: %d \n", tbeg, tend);*/


		if ( tbeg +32 < tend )
			/*if ( tbeg < tend )*/
		{
			pivot = quickSortPart(tbeg, tend, A, K, A_smem, K_smem);
			if ( (pivot+1) < tend ) {
				if ( warplane == 0 ) {
					K_smem[stpt] += 2;
					pushstack(qsstack, K_smem[stpt]-2, pivot+1);
					pushstack(qsstack, K_smem[stpt]-1, tend);
				}
			}
/*if ( warplane == 0 )*/
				/*printf("Debugi: tbeg %d, tend %d, stacksize %d pivot %d debugcount %d\n", tbeg, tend, K_smem[warpid], pivot, debugcount);*/
			/*printf("Debugi: tbeg %d, tend %d, pivot %d, pvalue %d, qstack %d, %d, %d, %d \n", tbeg, tend, pivot, K[A[pivot]], qsstack[0], qsstack[1], qsstack[2], qsstack[3]);*/

/*if ( warplane == 0 )*/
				/*printf("stacksize %d pushing %d qstack %d, %d, %d, %d \n", stacksize, pivot+1,qsstack[0], qsstack[1], qsstack[2], qsstack[3] );*/

/*if ( warplane == 0 )*/
				/*printf("stacksize %d pushing %d qstack %d, %d, %d, %d \n", stacksize, tend,qsstack[0], qsstack[1], qsstack[2], qsstack[3] );*/

			if ( tbeg < (pivot-1) ){
				if ( warplane == 0 ) {
					K_smem[iwarpid*32] += 2;
					pushstack(qsstack, K_smem[stpt]-2, tbeg);			
					pushstack(qsstack, K_smem[stpt]-1, pivot);
				}
				/*if ( warplane == 0 )*/
				/*printf("stacksize %d pushing %d qstack %d %d %d %d \n", stacksize, tbeg, qsstack[0], qsstack[1], qsstack[2], qsstack[3]);*/

/*if ( warplane == 0 )*/
				/*printf("stacksize %d pushing %d qstack %d %d %d %d \n", stacksize, pivot-1,qsstack[0], qsstack[1], qsstack[2], qsstack[3]);*/
			}
		}
		else if ( tbeg < tend ) 
		{
			int stacksize = K_smem[stpt];
			/*mergeSort(tbeg, tend, A, K, A_smem, K_smem, vmask, A_aux, K_aux);*/
			if ( tbeg != tend -1 )
				bitonicSort(tbeg, tend, A, K, A_smem, K_smem);
			if ( warplane == 0 )
			{
				K_smem[stpt] = stacksize;
			}
		}
		/*printf("\n");*/
		
	}

}
