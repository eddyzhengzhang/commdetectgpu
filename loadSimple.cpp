#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <vector>
#include <utility>

#include "loadSimple.h"
#include "loadUtils.h"
#include "timer.h"

void loadSimpleFile(Graph *&g, const std::string &fileName)
{
  std::ifstream ifs;

  double t0, t1, t2, t3;

  t0 = mytimer();

  ifs.open(fileName.c_str(), std::ifstream::in);
  if (!ifs) {
    std::cerr << "Error opening Simple format file: " << fileName << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  long int maxVertex = -1L, numEdges = 0L, numVertices;

  do {
    long int v0, v1;

    std::getline(ifs, line);

    std::istringstream iss(line);

    iss >> v0 >> v1;

    assert(iss.eof());

    if (v0 > maxVertex)
      maxVertex = v0;

    if (v1 > maxVertex)
      maxVertex = v1;

    numEdges++;
  } while (!ifs.eof());

  std::cout << "Last line read: " << line << std::endl;
  numEdges--;  // Do not consider last line

  numVertices = maxVertex + 1L;

  t1 = mytimer();

  std::cout << "Loading Simple file: " << fileName << ", numvertices: " << numVertices <<
    ", numEdges: " << numEdges << std::endl;
  std::cout << "Edge & vertex count time: " << (t1 - t0) << std::endl;

  t2 = mytimer();

  ifs.close();
  ifs.open(fileName.c_str(), std::ifstream::in);

  std::vector<GraphElem> edgeCount(numVertices + 1L);
  std::vector<GraphElemPair> edgeList(numEdges);

  for (long int i = 0L; i < numEdges; i++) {
    long int v0, v1;

    std::getline(ifs, line);

    std::istringstream iss(line);

    iss >> v0 >> v1;
    assert(iss.eof());

    edgeList[i].first = v0;
    edgeList[i].second = v1;

    edgeCount[v0 + 1L]++;
  }

  ifs.close();

  t3 = mytimer();

  std::cout << "Edge read time: " << (t3 - t2) << std::endl;

  t2 = mytimer();

  std::cout << "Before allocating graph" << std::endl;

  g = new Graph(numVertices, numEdges);

  assert(g);

  std::cout << "Allocated graph: " << g << std::endl;

  processGraphData(*g, edgeCount, edgeList, numVertices, numEdges);

  t3 = mytimer();  

  std::cout << "Graph populate time: " << (t3 - t2) << std::endl;
  std::cout << "Total I/O time: " << (t3 - t0) << std::endl;
} // loadSimpleFile
