#include <algorithm>
#include <iostream>
#include <numeric>
#include <stdio.h>
#include "omp.h"
#include "louvainMethodC.h"

typedef std::vector<double> DoubleVector;
typedef double* DoubleArray;

static ClusterLocalMapV *clmapv = NULL;
static size_t clsz = 0;
#pragma omp threadprivate(clmapv, clsz)

static void sumVertexDegree(const Graph &g, GraphElemArray &vdegree, CommArray &cinfo);
static double calcConstantForSecondTerm(const GraphElemArray &vdegree, GraphElem size);
static void initComm(CommunityArray &pastComm, CommunityArray &currComm, GraphElem size);
static GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
				      ClusterLocalMapV &clmap, DoubleArray &counter,
				      const Graph &g, CommunityVector &currComm,
				      const GraphElem vertex);
static GraphElem getMaxIndex(const ClusterLocalMapV &clmap, const DoubleArray &counter,
			     const GraphElem selfLoop, const CommArray &cinfo,
			     const GraphElem vdegree, const GraphElem currComm,
			     const double constant, const GraphElem e0, const GraphElem e1,
			     const GraphElem vertex);
static void initLouvain(const Graph &g, const CommunityArray &cvect,
			CommunityArray &pastComm, CommunityArray &currComm,
			GraphElemArray &vdegree, GraphElemArray &clusterWeight,
			CommArray &cinfo, CommArray &cupdate,
			double &constantForSecondTerm);
static void executeLouvainIteration(const GraphElem i, const Graph &g,
				    CommunityArray &currComm,
				    const GraphElemArray &vdegree, const CommArray &cinfo, 
				    CommArray &cupdate, const double constantForSecondTerm,
				    GraphElemArray &clusterWeight,
				    const bool updateWeight = false,
				    CommunityArray *const targetComm = NULL);
static void updatecinfo(CommArray &cinfo, const CommArray &cupdate, size_t size);
static double computeModularity(const Graph &g, CommArray &cinfo,
				GraphElemArray &clusterWeight,
				const CommunityArray &currComm,
				const double constantForSecondTerm,
				const bool useColoring = true);
static void cleanCWandCU(const GraphElem nv, GraphElemArray &clusterWeight,
			 CommArray &cupdate);

/*
double louvainWithDistOneColoring(const Graph &g, CommunityVector &cvect,
				  ColorVector &colors, const ColorElem numColors,
				  const double lower, const double thresh)
{
  CommunityVector pastComm, currComm;
  GraphElemVector vdegree;
  GraphElemVector clusterWeight;
  CommVector cinfo, cupdate;

  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();
  const double threshMod = thresh;
  const GraphElem nodeCountCutoff = ceil(static_cast<double>(nv) * 0.20); // Heuristic 20%
  const int nthreads = omp_get_max_threads();

  double constantForSecondTerm;
  double prevMod = lower;
  double currMod = -1.0;
  int numIters = 0;
  bool recolorFlag = false;

  initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
	      constantForSecondTerm);
  // clmapv.resize(ne + nv);

  ColorVector colorPtr, colorIndex, colorAdded, nodeCnt;

 recolor:
  colorPtr.resize(numColors + 1L);
  nodeCnt.resize(numColors);
  if (recolorFlag) {
    colorIndex.resize(nv);
    colorAdded.resize(numColors);
  }

#pragma omp parallel for default(none), shared(colors, colorPtr), schedule(guided)
  for (GraphElem i = 0L; i < nv; i++)
#pragma omp atomic update
    colorPtr[colors[i] + 1]++;

  {
    ColorVector colorPtrPSum(numColors + 1L);

    std::partial_sum(colorPtr.begin(), colorPtr.end(), colorPtrPSum.begin());
    colorPtr = colorPtrPSum;
  }

  int posb = -1;

  for (ColorElem i = 0L; i < numColors; i++) {
    const int cnt = (colorPtr[i + 1] - colorPtr[i]);

    nodeCnt[i] = cnt;
  }
  if (!recolorFlag) {
    ColorVector reorderColors(numColors);

    for (ColorElem i = 0L; i < numColors; i++)
      reorderColors[i] = i;
    
    std::sort(reorderColors.begin(), reorderColors.end(),
	      [&nodeCnt] (const int c0, const int c1)
	      { return (nodeCnt[c0] > nodeCnt[c1]); });

    ColorReorderMap remap;
    int reord = 0;

    for (ColorElem i = 0L; i < numColors; i++) {
      remap.insert(std::make_pair(reorderColors[i], i));
      if (reorderColors[i] != i)
	reord++;
    }

#pragma omp parallel for default(none), shared(remap, colors), schedule(static)
    for (GraphElem i = 0L; i < nv; i++) {
      ColorReorderMap::const_iterator iter = remap.find(colors[i]);

      colors[i] = iter->second;
    }

    colorPtr.clear();
    nodeCnt.clear();
    recolorFlag = true;

    std::cout << "Number of reordered colors: " << reord << std::endl;
    goto recolor;
  }
  else {
    GraphElem curCnt = 0L;
    GraphElem i = 0L;

    while (curCnt < nodeCountCutoff) {
      curCnt += nodeCnt[i];
      posb = i;
      i++;
    }
  }

#pragma omp parallel for default(none), shared(colors, colorPtr, colorAdded, colorIndex), schedule(guided)
  for (GraphElem i = 0L; i < nv; i++) {
    const int color = colors[i];
    GraphElem where;

#pragma omp atomic capture
    where = colorAdded[color]++;

    where += colorPtr[color];
    colorIndex[where] = i;
  }

  std::cout << "End position for large colors: " << posb << ", node count: " <<
    ((posb >= 0) ? nodeCnt[posb] : -1) << ", largest: " << nodeCnt[0] << std::endl;
  std::cout << "Node count cutoff: " << nodeCountCutoff << std::endl;

  do {
    numIters++;

#pragma omp parallel default(none), shared(g, clusterWeight, cupdate, colorPtr, colorIndex, currComm, vdegree, cinfo), firstprivate(constantForSecondTerm, posb, numIters)
    {
      if (!clmapv)
	clmapv = new ClusterLocalMapV;

      const int tid = omp_get_thread_num();

      for (GraphElem ci = 0L; ci <= posb; ci++) {
	cleanCWandCU(nv, clusterWeight, cupdate);

	GraphElem coloradj1 = colorPtr[ci];
	GraphElem coloradj2 = colorPtr[ci + 1];

#pragma omp for schedule(guided)
	for (GraphElem k = coloradj1; k < coloradj2; k++) {
	  const GraphElem i = colorIndex[k];

	  executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				  clusterWeight);
	}
	updatecinfo(cinfo, cupdate);
      }

#pragma omp barrier

      cleanCWandCU(nv, clusterWeight, cupdate);
#pragma omp for schedule(guided)
      for (GraphElem k = colorPtr[posb + 1]; k < colorPtr[numColors]; k++) {
	const GraphElem i = colorIndex[k];

	executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				clusterWeight);
      }
      updatecinfo(cinfo, cupdate);
    }
    
    currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm);
    if ((currMod - prevMod) < threshMod)
      break;

    prevMod = currMod;
  } while(true);

  cvect = currComm;

  return prevMod;
} // louvainWithDistOneColoring
*/

double louvainMethodOld(const Graph &g, CommunityArray &cvect, size_t &cvect_size, const double lower)
{
  CommunityArray pastComm=NULL, currComm=NULL, targetComm=NULL;
  GraphElemArray vdegree=NULL;
  GraphElemArray clusterWeight=NULL;
  CommArray cinfo = NULL, cupdate=NULL;

  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();
  const double threshMod = 0.000001;

  double constantForSecondTerm;
  double prevMod = -1.0;
  double currMod = -1.0;
  int numIters = 0;


  initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
	      constantForSecondTerm);


  if(targetComm != NULL){
      free(targetComm);
  }
  targetComm = (CommunityArray) malloc(sizeof(GraphElem)*nv);

  do {
    numIters++;

#pragma omp parallel default(none), shared(clusterWeight, cupdate, g, currComm, vdegree, cinfo, targetComm), firstprivate(constantForSecondTerm)
    {
      if (!clmapv)
	clmapv = new ClusterLocalMapV;

#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++) {
	clusterWeight[i] = 0L;
	cupdate[i].degree = 0L;
	cupdate[i].size = 0L;
      }
#pragma omp for schedule(guided)
      for (GraphElem i = 0L; i < nv; i++)
	executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				clusterWeight, true, &targetComm);
    }

    currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm, false);
    if ((currMod - prevMod) < threshMod) {
      currComm = pastComm;
      break;
    }

    prevMod = currMod;
    if (prevMod < lower)
      prevMod = lower;

#pragma omp parallel default(none), shared(cinfo, cupdate, pastComm, currComm, targetComm)
    {
      updatecinfo(cinfo, cupdate, nv);

#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++) {
	GraphElem tmp;

	tmp = pastComm[i];
	pastComm[i] = currComm[i];
	currComm[i] = targetComm[i];
	targetComm[i] = tmp;
      }
    }
  } while(true);

  free(cvect);
  cvect = (CommunityArray) malloc(sizeof(GraphElem)*nv);
  for(int i = 0; i < nv; i++){
      cvect[i] = currComm[i];
  }
  //cvect = currComm;
  return prevMod;
} // louvainMethod

double louvainMethod(const Graph &g, CommunityVector &cvect, const double lower){
    CommunityArray cvect_array = (CommunityArray) malloc(sizeof(GraphElem)*cvect.size());
    size_t size = cvect.size();
    for(int i = 0; i < cvect.size(); i++){
        cvect_array[i] = cvect[i];
    }

    double r =  louvainMethodOld(g, cvect_array, size, lower);
    cvect = CommunityVector(cvect_array, cvect_array+size);
}

void sumVertexDegree(const Graph &g, GraphElemArray &vdegree, CommArray &cinfo)
{
  const GraphElem nv = g.getNumVertices();

#pragma omp parallel for default(none), shared(g, vdegree, cinfo), schedule(guided)
  for (GraphElem i = 0; i < nv; i++) {
    GraphElem e0, e1;
    GraphElem tw = 0L;

    g.getEdgeRangeForVertex(i, e0, e1);

    for (GraphElem k = e0; k < e1; k++) {
      const Edge &edge = g.getEdge(k);

      tw += edge.weight;
    }

    vdegree[i] = tw;
    cinfo[i].degree = tw;
    cinfo[i].size = 1L;
  }
} // sumVertexDegree

double calcConstantForSecondTerm(const GraphElemArray &vdegree, GraphElem size)
{
  GraphElem totalEdgeWeightTwice = 0L;
  GraphElem localWeight = 0L;
#pragma omp parallel for default(none), shared(vdegree, size), reduction(+: localWeight), schedule(static)
  for (GraphElem i = 0; i < size; i++)
    localWeight += vdegree[i];
  totalEdgeWeightTwice = localWeight;
  return (1.0 / static_cast<double>(totalEdgeWeightTwice));
} // calcConstantForSecondTerm

void initComm(CommunityArray &pastComm, CommunityArray &currComm, GraphElem size)
{
#pragma omp parallel for default(none), shared(pastComm, currComm,size), schedule(static)
  for (GraphElem i = 0L; i < size; i++) {
    pastComm[i] = i;
    currComm[i] = i;
  }
} // initComm

GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
			       ClusterLocalMapV &clmap, DoubleArray &counter,
			       const Graph &g, CommunityArray &currComm,
			       const GraphElem vertex)
{
  ClusterLocalMapV::const_iterator storedAlready;
  GraphElem numUniqueClusters = 1L;
  GraphElem selfLoop = 0L;
  for (GraphElem j = e0; j < e1; j++) {
    const Edge &edge = g.getEdge(j);
    if (edge.tail == vertex)
      selfLoop += edge.weight;
    KeyValueComm kv;
    kv.commId = currComm[edge.tail];
    kv.cnt = numUniqueClusters;
    ClusterLocalMapV::const_iterator bound = clmap.cbegin() + clsz;
    storedAlready = std::find(clmap.cbegin(), bound, kv);
    if (storedAlready != bound)
      counter[storedAlready->cnt] += edge.weight;
    else {
      assert((e0 + vertex + clsz) < (e1 + vertex + 1));
      clmap[clsz] = kv;
      counter[clsz] = static_cast<double>(edge.weight);
      clsz++;
      numUniqueClusters++;
    }
  }
  return selfLoop;
} // buildLocalMapCounter

GraphElem getMaxIndex(const ClusterLocalMapV &clmap, const DoubleArray &counter,
		      const GraphElem selfLoop, const CommArray &cinfo,
		      const GraphElem vdegree, const GraphElem currComm,
		      const double constant, const GraphElem e0, const GraphElem e1,
		      const GraphElem vertex)
{
  ClusterLocalMapV::const_iterator storedAlready, bound;
  GraphElem maxIndex = currComm;
  double curGain = 0.0, maxGain = 0.0;
  double eix = counter[0] - static_cast<double>(selfLoop);
  double ax = cinfo[currComm].degree - vdegree;
  double eiy = 0.0, ay = 0.0;

  // storedAlready = clmap.begin() + e0 + vertex;
  storedAlready = clmap.begin();
  // bound = clmap.begin() + e0 + clsz + vertex;
  bound = clmap.begin() + clsz;

  do {
    if (currComm != storedAlready->commId) {
      ay = cinfo[storedAlready->commId].degree;
      eiy = counter[storedAlready->cnt];
      curGain = 2.0 * (eiy - eix) - 2.0 * vdegree * (ay - ax) * constant;

      if ((curGain > maxGain) ||
	  ((curGain == maxGain) && (curGain != 0.0) && (storedAlready->commId < maxIndex))) {
	maxGain = curGain;
	maxIndex = storedAlready->commId;
      }
    }
    storedAlready++;
  } while (storedAlready != bound);

  if ((cinfo[maxIndex].size == 1) && (cinfo[currComm].size == 1) && (maxIndex > currComm))
    maxIndex = currComm;

  return maxIndex;
} // getMaxIndex

void initLouvain(const Graph &g, const CommunityArray &cvect, CommunityArray &pastComm,
		 CommunityArray &currComm, GraphElemArray &vdegree,
		 GraphElemArray &clusterWeight, CommArray &cinfo, CommArray &cupdate,
		 double &constantForSecondTerm)
{
  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();

  if(vdegree != NULL){
      free(vdegree);
  }
  vdegree = (GraphElemArray) malloc( sizeof(GraphElem) * nv);

  if(cinfo != NULL){
      free(cinfo);
  }
  cinfo = (CommArray) malloc( sizeof(Comm) * nv);

  if(cupdate!=NULL){
      free(cupdate);
  }
  cupdate = (CommArray) malloc( sizeof(Comm) * nv);

  if(pastComm != NULL){
      free(pastComm);
  }
  pastComm = (CommunityArray) malloc( sizeof(GraphElem) * nv);

  if(currComm != NULL){
      free(currComm);
  }
  currComm = (CommunityArray) malloc( sizeof(GraphElem) * nv);

  if(clusterWeight != NULL){
      free(clusterWeight);
  }
  clusterWeight = (GraphElemArray) malloc(sizeof(GraphElem) * nv);

  sumVertexDegree(g, vdegree, cinfo);
  constantForSecondTerm = calcConstantForSecondTerm(vdegree, nv);

  currComm = cvect;
  for(int i = 0; i < nv; i++){
      currComm[i] = cvect[i];
  }
  initComm(pastComm, currComm, nv);
} // initLouvain

void executeLouvainIteration(const GraphElem i, const Graph &g,
			     CommunityArray &currComm, const GraphElemArray &vdegree,
			     const CommArray &cinfo, CommArray &cupdate,
			     const double constantForSecondTerm, GraphElemArray &clusterWeight,
			     const bool updateWeight, CommunityArray *const targetComm)
{
  int localTarget = -1;
  GraphElem e0, e1, selfLoop = 0L;
  // ClusterLocalMapV clmap;
  DoubleArray counter = (double*)malloc(sizeof(double)*g.getNumVertices());

  g.getEdgeRangeForVertex(i, e0, e1);
  clsz = 0;
  const GraphElem deg = (e1 - e0);

  if (clmapv->size() < (deg + 1))
    clmapv->resize(deg + 1);

  if (e0 != e1) {
    // clmap.reserve(e1 - e0);

    KeyValueComm kv;

    kv.commId = currComm[i];
    kv.cnt = 0;

    // clmapv.at(e0 + i + clsz) = kv;
    (*clmapv)[clsz] = kv;
    counter[clsz] = 0.0;
    clsz++;

    selfLoop = buildLocalMapCounter(e0, e1, *clmapv, counter, g, currComm, i);

    if (updateWeight)
      clusterWeight[i] += static_cast<GraphElem>(counter[0]);
    localTarget = getMaxIndex(*clmapv, counter, selfLoop, cinfo, vdegree[i], currComm[i],
			      constantForSecondTerm, e0, e1, i);
  }
  else
    localTarget = -1;

  if ((localTarget != currComm[i]) && (localTarget != -1)) {
#pragma omp atomic update
    cupdate[localTarget].degree += vdegree[i];
#pragma omp atomic update
    cupdate[localTarget].size++;
#pragma omp atomic update
    cupdate[currComm[i]].degree -= vdegree[i];
#pragma omp atomic update
    cupdate[currComm[i]].size--;
  }
  if (!targetComm)
    currComm[i] = localTarget;
  else
    (*targetComm)[i] = localTarget;
} // executeLouvainIteration

void updatecinfo(CommArray &cinfo, const CommArray &cupdate, size_t csz)
{

#pragma omp for schedule(static)
  for (GraphElem i = 0L; i < csz; i++) {
    cinfo[i].size += cupdate[i].size;
    cinfo[i].degree += cupdate[i].degree;
  }
} // updatecinfo

double computeModularity(const Graph &g, CommArray &cinfo, GraphElemArray &clusterWeight,
			 const CommunityArray &currComm, const double constantForSecondTerm,
			 const bool useColoring)
{
  const GraphElem nv = g.getNumVertices();

  if (useColoring) {
#pragma omp parallel default(none), shared(clusterWeight, g, currComm)
    {
#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++)
	clusterWeight[i] = 0L;

#pragma omp for schedule(guided), nowait
      for (GraphElem i = 0L; i < nv; i++) {
	GraphElem e0, e1;

	g.getEdgeRangeForVertex(i, e0, e1);

	for (GraphElem j = e0; j < e1; j++) {
	  const Edge &edge = g.getEdge(j);

	  if (currComm[edge.tail] == currComm[i])
	    clusterWeight[i] += static_cast<GraphElem>(edge.weight);
	}
      }
    }
  }

  double e_xx = 0.0, a2_x = 0.0;


#pragma omp parallel for default(none), shared(clusterWeight, cinfo), reduction(+: e_xx), reduction(+: a2_x), schedule(static)
  for (GraphElem i = 0L; i < nv; i++) {
    e_xx += clusterWeight[i];
    a2_x += static_cast<double>(cinfo[i].degree) * static_cast<double>(cinfo[i].degree);
  }

  double currMod = (e_xx * constantForSecondTerm) - (a2_x * constantForSecondTerm *
						     constantForSecondTerm);

  std::cout << "e_xx: " << e_xx << ", a2_x: " << a2_x << ", currMod: " << currMod << std::endl;

  return currMod;
} // computeModularity

inline void cleanCWandCU(const GraphElem nv, GraphElemVector &clusterWeight, CommArray &cupdate)
{
#pragma omp for schedule(static)
  for (GraphElem i = 0L; i < nv; i++) {
    clusterWeight[i] = 0L;
    cupdate[i].degree = 0L;
    cupdate[i].size = 0L;
  }
} // cleanCWandCU
