#ifndef __LOAD_UTILS_H
#define __LOAD_UTILS_H

#include <vector>

#include "graph.h"

typedef std::pair<GraphElem, GraphElem> GraphElemPair;

void processGraphData(Graph &g, std::vector<GraphElem> &edgeCount,
		      std::vector<GraphElemPair> &edgeList,
		      const GraphElem nv, const GraphElem ne,
		      const std::vector<double> *weights = NULL);

#endif // __LOAD_UTILS_H
