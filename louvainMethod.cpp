#include <algorithm>
#include <iostream>
#include <numeric>

#include <stdio.h>

#include "omp.h"

#include "louvainMethod.h"

typedef std::vector<double> DoubleVector;

static ClusterLocalMapV *clmapv = NULL;
static size_t clsz = 0;
#pragma omp threadprivate(clmapv, clsz)

static void sumVertexDegree(const Graph &g, GraphElemVector &vdegree, CommVector &cinfo);
static double calcConstantForSecondTerm(const GraphElemVector &vdegree);
static void initComm(CommunityVector &pastComm, CommunityVector &currComm);
static GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
				      ClusterLocalMapV &clmap, DoubleVector &counter,
				      const Graph &g, CommunityVector &currComm,
				      const GraphElem vertex);
static GraphElem getMaxIndex(const ClusterLocalMapV &clmap, const DoubleVector &counter,
			     const GraphElem selfLoop, const CommVector &cinfo,
			     const GraphElem vdegree, const GraphElem currComm,
			     const double constant, const GraphElem e0, const GraphElem e1,
			     const GraphElem vertex);
static void initLouvain(const Graph &g, const CommunityVector &cvect,
			CommunityVector &pastComm, CommunityVector &currComm,
			GraphElemVector &vdegree, GraphElemVector &clusterWeight,
			CommVector &cinfo, CommVector &cupdate,
			double &constantForSecondTerm);
static void executeLouvainIteration(const GraphElem i, const Graph &g,
				    CommunityVector &currComm,
				    const GraphElemVector &vdegree, const CommVector &cinfo, 
				    CommVector &cupdate, const double constantForSecondTerm,
				    GraphElemVector &clusterWeight,
				    const bool updateWeight = false,
				    CommunityVector *const targetComm = NULL);
static void updatecinfo(CommVector &cinfo, const CommVector &cupdate);
static double computeModularity(const Graph &g, CommVector &cinfo,
				GraphElemVector &clusterWeight,
				const CommunityVector &currComm,
				const double constantForSecondTerm,
				const bool useColoring = true);
static void cleanCWandCU(const GraphElem nv, GraphElemVector &clusterWeight,
			 CommVector &cupdate);

double louvainWithDistOneColoring(const Graph &g, CommunityVector &cvect,
				  ColorVector &colors, const ColorElem numColors,
				  const double lower, const double thresh)
{
  CommunityVector pastComm, currComm;
  GraphElemVector vdegree;
  GraphElemVector clusterWeight;
  CommVector cinfo, cupdate;

  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();
  const double threshMod = thresh;
  const GraphElem nodeCountCutoff = ceil(static_cast<double>(nv) * 0.20); // Heuristic 20%
  const int nthreads = omp_get_max_threads();

  double constantForSecondTerm;
  double prevMod = lower;
  double currMod = -1.0;
  int numIters = 0;
  bool recolorFlag = false;

  initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
	      constantForSecondTerm);
  // clmapv.resize(ne + nv);

  ColorVector colorPtr, colorIndex, colorAdded, nodeCnt;

 recolor:
  colorPtr.resize(numColors + 1L);
  nodeCnt.resize(numColors);
  if (recolorFlag) {
    colorIndex.resize(nv);
    colorAdded.resize(numColors);
  }

#pragma omp parallel for default(none), shared(colors, colorPtr), schedule(guided)
  for (GraphElem i = 0L; i < nv; i++)
#pragma omp atomic update
    colorPtr[colors[i] + 1]++;

  {
    ColorVector colorPtrPSum(numColors + 1L);

    std::partial_sum(colorPtr.begin(), colorPtr.end(), colorPtrPSum.begin());
    colorPtr = colorPtrPSum;
  }

  int posb = -1;

  for (ColorElem i = 0L; i < numColors; i++) {
    const int cnt = (colorPtr[i + 1] - colorPtr[i]);

    nodeCnt[i] = cnt;
  }
  if (!recolorFlag) {
    ColorVector reorderColors(numColors);

    for (ColorElem i = 0L; i < numColors; i++)
      reorderColors[i] = i;
    
    std::sort(reorderColors.begin(), reorderColors.end(),
	      [&nodeCnt] (const int c0, const int c1)
	      { return (nodeCnt[c0] > nodeCnt[c1]); });

    ColorReorderMap remap;
    int reord = 0;

    for (ColorElem i = 0L; i < numColors; i++) {
      remap.insert(std::make_pair(reorderColors[i], i));
      if (reorderColors[i] != i)
	reord++;
    }

#pragma omp parallel for default(none), shared(remap, colors), schedule(static)
    for (GraphElem i = 0L; i < nv; i++) {
      ColorReorderMap::const_iterator iter = remap.find(colors[i]);

      colors[i] = iter->second;
    }

    colorPtr.clear();
    nodeCnt.clear();
    recolorFlag = true;

    std::cout << "Number of reordered colors: " << reord << std::endl;
    goto recolor;
  }
  else {
    GraphElem curCnt = 0L;
    GraphElem i = 0L;

    while (curCnt < nodeCountCutoff) {
      curCnt += nodeCnt[i];
      posb = i;
      i++;
    }
  }

#pragma omp parallel for default(none), shared(colors, colorPtr, colorAdded, colorIndex), schedule(guided)
  for (GraphElem i = 0L; i < nv; i++) {
    const int color = colors[i];
    GraphElem where;

#pragma omp atomic capture
    where = colorAdded[color]++;

    where += colorPtr[color];
    colorIndex[where] = i;
  }

  std::cout << "End position for large colors: " << posb << ", node count: " <<
    ((posb >= 0) ? nodeCnt[posb] : -1) << ", largest: " << nodeCnt[0] << std::endl;
  std::cout << "Node count cutoff: " << nodeCountCutoff << std::endl;

  do {
    numIters++;

#pragma omp parallel default(none), shared(g, clusterWeight, cupdate, colorPtr, colorIndex, currComm, vdegree, cinfo), firstprivate(constantForSecondTerm, posb, numIters)
    {
      if (!clmapv)
	clmapv = new ClusterLocalMapV;

      const int tid = omp_get_thread_num();

      for (GraphElem ci = 0L; ci <= posb; ci++) {
	cleanCWandCU(nv, clusterWeight, cupdate);

	GraphElem coloradj1 = colorPtr[ci];
	GraphElem coloradj2 = colorPtr[ci + 1];

#pragma omp for schedule(guided)
	for (GraphElem k = coloradj1; k < coloradj2; k++) {
	  const GraphElem i = colorIndex[k];

	  executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				  clusterWeight);
	}
	updatecinfo(cinfo, cupdate);
      }

#pragma omp barrier

      cleanCWandCU(nv, clusterWeight, cupdate);
#pragma omp for schedule(guided)
      for (GraphElem k = colorPtr[posb + 1]; k < colorPtr[numColors]; k++) {
	const GraphElem i = colorIndex[k];

	executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				clusterWeight);
      }
      updatecinfo(cinfo, cupdate);
    }
    
    currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm);
    if ((currMod - prevMod) < threshMod)
      break;

    prevMod = currMod;
  } while(true);

  cvect = currComm;

  return prevMod;
} // louvainWithDistOneColoring

double louvainMethod(const Graph &g, CommunityVector &cvect, const double lower)
{
  CommunityVector pastComm, currComm, targetComm;
  GraphElemVector vdegree;
  GraphElemVector clusterWeight;
  CommVector cinfo, cupdate;

  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();
  const double threshMod = 0.000001;

  double constantForSecondTerm;
  double prevMod = -1.0;
  double currMod = -1.0;
  int numIters = 0;

  initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
	      constantForSecondTerm);
  targetComm.resize(nv);
  // clmapv.resize(ne + nv);

  do {
    numIters++;

    test(cupdate, clusterWeight, nv, clmapv, g);

#pragma omp parallel default(none), shared(clusterWeight, cupdate, g, currComm, vdegree, cinfo, targetComm), firstprivate(constantForSecondTerm)
    {
      if (!clmapv)
	clmapv = new ClusterLocalMapV;

#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++) {
	clusterWeight[i] = 0L;
	cupdate[i].degree = 0L;
	cupdate[i].size = 0L;
      }
#pragma omp for schedule(guided)
      for (GraphElem i = 0L; i < nv; i++)
	executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
				clusterWeight, true, &targetComm);
    }

			//testing abe
		for ( GraphElem i = 0; i < nv; i++ )
		{
			printf("Testing: clusterweight %d is %d\n", i, clusterWeight[i]);
		}



    currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm, false);
 		printf("Testing: Iter %d const %lf currMod %lf prevMod %lf\n", numIters, constantForSecondTerm, currMod, prevMod);
		for (GraphElem i = 0L; i < nv; i++)
			printf("Testing: node i %d targetcomm %d\n", i, targetComm[i]);
		printf("\n");


   if ((currMod - prevMod) < threshMod) {
      currComm = pastComm;
			printf("Testing: moving on to next outer loop iteration\n");
      break;
    }

    prevMod = currMod;
    if (prevMod < lower)
      prevMod = lower;

#pragma omp parallel default(none), shared(cinfo, cupdate, pastComm, currComm, targetComm)
    {
      updatecinfo(cinfo, cupdate);



#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++) {
	GraphElem tmp;

	tmp = pastComm[i];
	pastComm[i] = currComm[i];
	currComm[i] = targetComm[i];
	targetComm[i] = tmp;
      }
    }
		//testing abe
		for ( GraphElem i = 0; i < nv; i++ ) 
		{
			printf("Testing: comm %d size %d degree %d\n", i, cinfo[i].size, cinfo[i].degree);
		}
	
  } while(true);

  cvect = currComm;

  return prevMod;
} // louvainMethod

void sumVertexDegree(const Graph &g, GraphElemVector &vdegree, CommVector &cinfo)
{
  const GraphElem nv = g.getNumVertices();

#pragma omp parallel for default(none), shared(g, vdegree, cinfo), schedule(guided)
  for (GraphElem i = 0; i < nv; i++) {
    GraphElem e0, e1;
    GraphElem tw = 0L;

    g.getEdgeRangeForVertex(i, e0, e1);

    for (GraphElem k = e0; k < e1; k++) {
      const Edge &edge = g.getEdge(k);

      tw += edge.weight;
    }

    vdegree[i] = tw;
    cinfo[i].degree = tw;
    cinfo[i].size = 1L;
  }
} // sumVertexDegree

double calcConstantForSecondTerm(const GraphElemVector &vdegree)
{
  GraphElem totalEdgeWeightTwice = 0L;
  GraphElem localWeight = 0L;

  const size_t vsz = vdegree.size();

#pragma omp parallel for default(none), shared(vdegree), reduction(+: localWeight), schedule(static)
  for (GraphElem i = 0; i < vsz; i++)
    localWeight += vdegree[i];

  totalEdgeWeightTwice = localWeight;

  return (1.0 / static_cast<double>(totalEdgeWeightTwice));
} // calcConstantForSecondTerm

void initComm(CommunityVector &pastComm, CommunityVector &currComm)
{
  const size_t csz = currComm.size();

  assert(csz == pastComm.size());

#pragma omp parallel for default(none), shared(pastComm, currComm), schedule(static)
  for (GraphElem i = 0L; i < csz; i++) {
    pastComm[i] = i;
    currComm[i] = i;
  }
} // initComm

GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
			       ClusterLocalMapV &clmap, DoubleVector &counter,
			       const Graph &g, CommunityVector &currComm,
			       const GraphElem vertex)
{
  ClusterLocalMapV::const_iterator storedAlready;
  GraphElem numUniqueClusters = 1L;
  GraphElem selfLoop = 0L;

  for (GraphElem j = e0; j < e1; j++) {
    const Edge &edge = g.getEdge(j);

    if (edge.tail == vertex)
      selfLoop += edge.weight;

    KeyValueComm kv;

    kv.commId = currComm[edge.tail];
    kv.cnt = numUniqueClusters;

    // storedAlready = clmap.find(currComm[edge.tail]);
    // ClusterLocalMapV::const_iterator bound = clmap.cbegin() + e0 + vertex + clsz;
    ClusterLocalMapV::const_iterator bound = clmap.cbegin() + clsz;

    // storedAlready = std::find(clmap.cbegin() + e0 + vertex, bound, kv);
    storedAlready = std::find(clmap.cbegin(), bound, kv);
    if (storedAlready != bound)
      counter[storedAlready->cnt] += edge.weight;
    else {
      // clmap[currComm[edge.tail]] = numUniqueClusters;
//       if ((e0 + vertex + clsz) >= (e1 + vertex + 1)) {
// 	std::cout << "Out of bounds: e0: " << e0 << ", e1: " << e1 << ", clsz: " << clsz <<
// 	  std::endl;
// 	std::cout << "storedAlready: " << storedAlready->commId << ", bound: " <<
// 	  bound->commId << std::endl;
// 	std::cout << "Looking for: " << kv.commId << std::endl;
// 	for (auto iter = clmap.cbegin() + e0 + vertex; iter != bound; iter++)
// 	  std::cout << iter->commId << " ";
// 	std::cout << std::endl;
//       }
      
      assert((e0 + vertex + clsz) < (e1 + vertex + 1));
      clmap[clsz] = kv;
      clsz++;
      counter.push_back(static_cast<double>(edge.weight));
      numUniqueClusters++;
    }
  }

  return selfLoop;
} // buildLocalMapCounter

GraphElem getMaxIndex(const ClusterLocalMapV &clmap, const DoubleVector &counter,
		      const GraphElem selfLoop, const CommVector &cinfo,
		      const GraphElem vdegree, const GraphElem currComm,
		      const double constant, const GraphElem e0, const GraphElem e1,
		      const GraphElem vertex)
{
  ClusterLocalMapV::const_iterator storedAlready, bound;
  GraphElem maxIndex = currComm;
  double curGain = 0.0, maxGain = 0.0;
  double eix = counter[0] - static_cast<double>(selfLoop);
  double ax = cinfo[currComm].degree - vdegree;
  double eiy = 0.0, ay = 0.0;

	//printf("Testing: %d eix %lf ax %lf \n", vertex, eix, ax);

  // storedAlready = clmap.begin() + e0 + vertex;
  storedAlready = clmap.begin();
  // bound = clmap.begin() + e0 + clsz + vertex;
  bound = clmap.begin() + clsz;

  do {
    if (currComm != storedAlready->commId) {
      ay = cinfo[storedAlready->commId].degree;
      eiy = counter[storedAlready->cnt];
      curGain = 2.0 * (eiy - eix) - 2.0 * vdegree * (ay - ax) * constant;

			if ( vertex == 11 ) printf("Testing for vertex %d, eix %lf, ax %lf, eiy %lf, ay %lf vdegree %d commid %d\n", vertex, eix, ax, eiy, ay, vdegree, storedAlready->commId);
      if ((curGain > maxGain) ||
	  ((curGain == maxGain) && (curGain != 0.0) && (storedAlready->commId < maxIndex))) {
				if ( vertex == 11 ) printf("Testing: replaced %d with %d, ogain %lf, ngain %lf node %d eix %lf ax %lf eiy %lf ax %lf\n", maxIndex, storedAlready->commId,maxGain, curGain, vertex, eix, ax, eiy, ay, vertex);
	maxGain = curGain;
	maxIndex = storedAlready->commId;
      }
    }
    storedAlready++;
  } while (storedAlready != bound);


	if ( vertex == 1) printf("Testing: currComm %d maxIndex %d\n", currComm, maxIndex);
  if ((cinfo[maxIndex].size == 1) && (cinfo[currComm].size == 1) && (maxIndex > currComm))
    maxIndex = currComm;

  return maxIndex;
} // getMaxIndex

void initLouvain(const Graph &g, const CommunityVector &cvect, CommunityVector &pastComm,
		 CommunityVector &currComm, GraphElemVector &vdegree,
		 GraphElemVector &clusterWeight, CommVector &cinfo, CommVector &cupdate,
		 double &constantForSecondTerm)
{
  const GraphElem nv = g.getNumVertices();
  const GraphElem ne = g.getNumEdges();

  vdegree.resize(nv);
  cinfo.resize(nv);
  cupdate.resize(nv);
  pastComm.resize(nv);
  currComm.resize(nv);
  clusterWeight.resize(nv);

  sumVertexDegree(g, vdegree, cinfo);
  constantForSecondTerm = calcConstantForSecondTerm(vdegree);

  currComm = cvect;
  initComm(pastComm, currComm);
} // initLouvain

void executeLouvainIteration(const GraphElem i, const Graph &g,
			     CommunityVector &currComm, const GraphElemVector &vdegree,
			     const CommVector &cinfo, CommVector &cupdate,
			     const double constantForSecondTerm, GraphElemVector &clusterWeight,
			     const bool updateWeight, CommunityVector *const targetComm)
{
  int localTarget = -1;
  GraphElem e0, e1, selfLoop = 0L;
  // ClusterLocalMapV clmap;
  DoubleVector counter;

  g.getEdgeRangeForVertex(i, e0, e1);
  clsz = 0;
  const GraphElem deg = (e1 - e0);

  if (clmapv->size() < (deg + 1))
    clmapv->resize(deg + 1);

  if (e0 != e1) {
    // clmap.reserve(e1 - e0);

    KeyValueComm kv;

    kv.commId = currComm[i];
    kv.cnt = 0;

    // clmapv.at(e0 + i + clsz) = kv;
    (*clmapv)[clsz] = kv;
    clsz++;
    // clmap.push_back(kv);
    counter.push_back(0.0);

    selfLoop = buildLocalMapCounter(e0, e1, *clmapv, counter, g, currComm, i);

    if (updateWeight)
      clusterWeight[i] += static_cast<GraphElem>(counter[0]);
    localTarget = getMaxIndex(*clmapv, counter, selfLoop, cinfo, vdegree[i], currComm[i],
			      constantForSecondTerm, e0, e1, i);
  }
  else
    localTarget = -1;

  if ((localTarget != currComm[i]) && (localTarget != -1)) {
#pragma omp atomic update
    cupdate[localTarget].degree += vdegree[i];
#pragma omp atomic update
    cupdate[localTarget].size++;
#pragma omp atomic update
    cupdate[currComm[i]].degree -= vdegree[i];
#pragma omp atomic update
    cupdate[currComm[i]].size--;
  }
  if (!targetComm)
    currComm[i] = localTarget;
  else
    (*targetComm)[i] = localTarget;
} // executeLouvainIteration

void updatecinfo(CommVector &cinfo, const CommVector &cupdate)
{
  size_t csz = cinfo.size();

#pragma omp for schedule(static)
  for (GraphElem i = 0L; i < csz; i++) {
    cinfo[i].size += cupdate[i].size;
    cinfo[i].degree += cupdate[i].degree;
  }
} // updatecinfo

double computeModularity(const Graph &g, CommVector &cinfo, GraphElemVector &clusterWeight,
			 const CommunityVector &currComm, const double constantForSecondTerm,
			 const bool useColoring)
{
  const GraphElem nv = g.getNumVertices();

  if (useColoring) {
#pragma omp parallel default(none), shared(clusterWeight, g, currComm)
    {
#pragma omp for schedule(static)
      for (GraphElem i = 0L; i < nv; i++)
	clusterWeight[i] = 0L;

#pragma omp for schedule(guided), nowait
      for (GraphElem i = 0L; i < nv; i++) {
	GraphElem e0, e1;

	g.getEdgeRangeForVertex(i, e0, e1);

	for (GraphElem j = e0; j < e1; j++) {
	  const Edge &edge = g.getEdge(j);

	  if (currComm[edge.tail] == currComm[i])
	    clusterWeight[i] += static_cast<GraphElem>(edge.weight);
	}
      }
    }
  }

  double e_xx = 0.0, a2_x = 0.0;

  assert((cinfo.size() == nv) && (clusterWeight.size() == nv));

#pragma omp parallel for default(none), shared(clusterWeight, cinfo), reduction(+: e_xx), reduction(+: a2_x), schedule(static)
  for (GraphElem i = 0L; i < nv; i++) {
    e_xx += clusterWeight[i];
    a2_x += static_cast<double>(cinfo[i].degree) * static_cast<double>(cinfo[i].degree);
  }

  double currMod = (e_xx * constantForSecondTerm) - (a2_x * constantForSecondTerm *
						     constantForSecondTerm);

	std::cout << "e_xx: " << e_xx << ", a2_x: " << a2_x << ", currMod: " << currMod << std::endl;

  return currMod;
} // computeModularity

inline void cleanCWandCU(const GraphElem nv, GraphElemVector &clusterWeight, CommVector &cupdate)
{
#pragma omp for schedule(static)
  for (GraphElem i = 0L; i < nv; i++) {
    clusterWeight[i] = 0L;
    cupdate[i].degree = 0L;
    cupdate[i].size = 0L;
  }
} // cleanCWandCU
