#include <stdio.h>
#include <cuda.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>

typedef int32_t SortVal;
typedef int32_t SortKey;
typedef int32_t SortList;
typedef int32_t SortRange;

#include "bitonicSort.cu"
#include "quickSort.cu"


long gettime(struct timeval start, struct timeval end)
{
	return ((end.tv_sec * 1000000 + end.tv_usec)
		  - (start.tv_sec * 1000000 + start.tv_usec));
}


///Local sort kernel for all nodes in the graph
//
//
//
__global__ void localSortKernel(SortRange * R, SortList * L, SortVal * A, SortKey * K, int warpnum, int * qstack, int qstacksize)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = tid >> 5;
	/*int warplane = threadIdx.x&31;// for debugging purpose  */ 
	extern __shared__ SortKey smem[]; //CAVEAT, here we assume both key and val are of the same type
	SortKey * k_smem = smem;
	SortVal * v_smem = (SortVal *) &smem[blockDim.x];
	int * c_smem = (int *) & v_smem[blockDim.x];
	int * vmask = (int *) & c_smem[blockDim.x/32];
	SortKey * k_aux = (SortKey *) &vmask[blockDim.x];
	SortVal * v_aux = (SortVal *) &k_aux[blockDim.x];

	if (warpid < warpnum)
	{
		for ( int i = R[warpid]; i < R[warpid+1]; i++ )
		{
			int start = L[i];
			int end = L[i+1]; 
			/*if ( (end - start) <= 32 && (end - start) > 1 ) mergeSort(start, end, A, K, v_smem, k_smem, vmask, v_aux, k_aux);*/
			if ( (end - start) > 1 ) {
				if ( (end - start) <= 32  ) bitonicSort(start, end, A, K, v_smem, k_smem);
				else quickSort(start, end, A, K, v_smem, k_smem, qstack, qstacksize, vmask, v_aux, k_aux);
			}
			/*if ( warplane == 0 )*/
			/*printf("Debug: start: %d, end: %d\n", start, end);*/
			/*if ( warpid == 3 ) */
			/*printf("%d: %d\t", tid, A[tid]);*/

		}
	}
}

