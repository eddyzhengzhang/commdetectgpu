#include <cstdlib>

#include <sys/resource.h>
#include <sys/time.h>

double mytimer(void)
{
  static long int start = 0L, startu;

  const double million = 1000000.0;

  timeval tp;

  if (start == 0L) {
    gettimeofday(&tp, NULL);

    start = tp.tv_sec;
    startu = tp.tv_usec;
  }

  gettimeofday(&tp, NULL);

  return (static_cast<double>(tp.tv_sec - start) + (static_cast<double>(tp.tv_usec - startu) /
						    million));
}
