#ifndef __GRAPH_H
#define __GRAPH_H

#include <cassert>

#include <algorithm>
#include <ostream>
#include <vector>

#include <stdio.h>
#include "edge_decl.h"

struct Comm {
  GraphElem size;
  GraphElem degree;
};
typedef std::vector<Comm> CommVector;
typedef Comm* CommArray;

typedef struct{
  GraphElem numVertices;
  GraphElem numEdges;
  Edge *edgeList;
  EdgeIndexesArray edgeListIndexes;
}GraphStruct;

typedef std::vector<Edge> EdgeList;
class Graph {
public:

  GraphElem numVertices;
  GraphElem numEdges;
  EdgeIndexes edgeListIndexes;
  EdgeList edgeList;

public:
  Graph(const GraphElem onv, const GraphElem one);
  Graph(const Graph &othis);
  ~Graph();

  GraphElem getNumVertices() const;
  GraphElem getNumEdges() const;
  void getEdgeRangeForVertex(const GraphElem vertex, GraphElem &e0, GraphElem &e1) const;
  const Edge &getEdge(const GraphElem edge) const;

  void setEdgeStartForVertex(const GraphElem vertex, const GraphElem e0);
  Edge &getEdge(const GraphElem edge);

  friend std::ostream &operator <<(std::ostream &os, const Graph &g);

public:
  Graph();
  Graph &operator = (const Graph &othis);
};

inline Graph::Graph()
  : numVertices(0), numEdges(0)
{
} // Graph

inline Graph::Graph(const GraphElem onv, const GraphElem one)
  : numVertices(onv), numEdges(one)
{
  edgeListIndexes.resize(numVertices + 1);
  edgeList.resize(numEdges);

  //std::for_each(edgeListIndexes.begin(), edgeListIndexes.end(),
//		[] (GraphElem &idx) { idx = 0; } );
  for (std::vector<GraphElem>::iterator temp = edgeListIndexes.begin(); temp!=edgeListIndexes.end(); ++temp) {
	*temp = 0;
  }
} // Graph

inline Graph::Graph(const Graph &othis)
  : numVertices(othis.numVertices), numEdges(othis.numEdges)
{
  edgeListIndexes.resize(numVertices + 1);
  edgeList.resize(numEdges);

  std::copy(othis.edgeListIndexes.begin(), othis.edgeListIndexes.end(),
	    edgeListIndexes.begin());
  std::copy(othis.edgeList.begin(), othis.edgeList.end(), edgeList.begin());
} // Graph

inline Graph::~Graph()
{
} // ~Graph

inline GraphElem Graph::getNumVertices() const
{
  return numVertices;
} // getNumVertices

inline GraphElem Graph::getNumEdges() const
{
  return numEdges;
} // getNumEdges

inline void Graph::getEdgeRangeForVertex(const GraphElem vertex, GraphElem &e0, GraphElem &e1) const
{
  assert((vertex >= 0) && (vertex < numVertices));

  e0 = edgeListIndexes[vertex];
  e1 = edgeListIndexes[vertex + 1];
  //printf("%d %d\n", e0, e1);
} // getEdgeRangeForVertex

inline const Edge &Graph::getEdge(const GraphElem edge) const
{
  assert((edge >= 0) && (edge < numEdges));

  return edgeList[edge];
} // getEdge

inline void Graph::setEdgeStartForVertex(const GraphElem vertex, const GraphElem e0)
{
  assert((vertex >= 0) && (vertex <= numVertices));
  assert((e0 >= 0) && (e0 <= numEdges));

  edgeListIndexes[vertex] = e0;
} // setEdgeRangeForVertex

inline Edge &Graph::getEdge(const GraphElem edge)
{
  if ((edge < 0) || (edge >= numEdges))
    std::cerr << "ERROR: out of bounds access: " << edge << ", max: " << numEdges << std::endl;

  assert((edge >= 0) && (edge < numEdges));

  return edgeList[edge];  
} // getEdge

inline std::ostream &operator <<(std::ostream &os, const Graph &g)
{
  os << "Number of vertices: " << g.numVertices << ", number of edges: " << g.numEdges <<
    std::endl;

  for (GraphElem i = 0; i < g.numVertices; i++) {
    const GraphElem lb = g.edgeListIndexes[i], ub = g.edgeListIndexes[i + 1];

    os << "Vertex: " << i << ", number of neighbors: " << ub - lb << std::endl;

    for (GraphElem j = lb; j < ub; j++) {
      const Edge &edge = g.getEdge(j);

      os << "Edge to: " << edge.tail << ", weight: " << edge.weight << std::endl;
    }
  }

  return os;
} // operator <<

#endif // __GRAPH_H
