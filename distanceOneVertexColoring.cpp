#include <cassert>
#include <cstdint>

#include <algorithm>
#include <deque>
#include <iostream>
#include <set>
#include <unordered_set>
#include <vector>

#include "distanceOneVertexColoring.h"
#include "RngStream.h"
#include "timer.h"

typedef std::vector<double> RandVec;
typedef std::vector<ColorElem> ColorQueue;
typedef std::vector<bool> BitVector;
// typedef std::unordered_set<ColorElem> NeighborColors;
typedef std::set<ColorElem> NeighborColors;

static const GraphElem MaxDegree = 4096;

static void generateRandomNumbers(RandVec &randVec);
static GraphElem resolveConflicts(const Graph &g, ColorVector &colors, const ColorQueue &q,
				  ColorQueue &qtmp, const size_t qsz, const RandVec &randVec,
				  GraphElem &qtmpPos);
static void rebalanceColors(const Graph &g, ColorVector &colors, const ColorElem numColors);

ColorElem distanceOneVertexColoring(const Graph &g, ColorVector &colors)
{
  const GraphElem nv = g.getNumVertices();

  RandVec randVec(nv);

  colors.resize(nv);

#pragma omp parallel for default(none), shared(colors), schedule(static)
  for (GraphElem i = 0L; i < nv; i++)
    colors[i] = -1;

  generateRandomNumbers(randVec);

  ColorQueue q(nv), qtmp;
  GraphElem qtmpPos = 0L;

#pragma omp parallel for default(none), shared(q), schedule(static)
  for (GraphElem i = 0; i < nv; i++)
    q[i] = i;

  ColorElem nconflicts = 0;
  int nloops = 0;

  GraphElem realMaxDegree = -1L;

#pragma omp parallel for default(none), shared(g), reduction(max: realMaxDegree), schedule(static)
  for (GraphElem i = 0L; i < nv; i++) {
    GraphElem e0, e1, er;

    g.getEdgeRangeForVertex(i, e0, e1);
    er = e1 - e0;
    if (er > realMaxDegree)
      realMaxDegree = er;
  }

  static_assert(sizeof(int) == sizeof(int32_t), "int should be 32-bit in size");
  assert((realMaxDegree < INT32_MAX) && (realMaxDegree > 0L));

  std::cout << "Maximum degree for the graph: " << realMaxDegree << std::endl;

  do {
    size_t qsz = q.size();

    qtmp.resize(qsz);

#pragma omp parallel default(none), shared(q, qtmp, qtmpPos, randVec, g, colors, qsz, nloops, nconflicts, std::cerr)
    {
#pragma omp for firstprivate(nloops, qsz), schedule(guided)
      for (GraphElem qi = 0L; qi < qsz; qi++) {
	GraphElem v = q[qi];
	GraphElem e0, e1;
	ColorElem maxColor = -1, adjColor = -1;

	g.getEdgeRangeForVertex(v, e0, e1);

	BitVector mark(MaxDegree, false);

	for (GraphElem k = e0; k < e1; k++) {
	  const Edge &edge = g.getEdge(k);

	  adjColor = colors[edge.tail];
	  if (adjColor >= 0) {
	    if (adjColor >= MaxDegree) {
	      std::cerr << "Maximum number of colors exceeded: " << adjColor << std::endl;
	      exit(EXIT_FAILURE);
	    }

	    mark[adjColor] = true;
	    if (adjColor > maxColor)
	      maxColor = adjColor;
	  }
	}

	ColorElem myColor;

	for (myColor = 0; myColor <= maxColor; myColor++)
	  if (mark[myColor] != true)
	    break;

	if (myColor == maxColor)
	  myColor++;

	colors[v] = myColor;
      }

#pragma omp atomic update
      nconflicts += resolveConflicts(g, colors, q, qtmp, qsz, randVec, qtmpPos);

#pragma omp single
      {
	q.resize(qtmpPos);
      }

#pragma omp for schedule(static)
      for (GraphElem qi = 0L; qi < qtmpPos; qi++)
	q[qi] = qtmp[qi];
    }

    std::cout << "Iteration: " << nloops << ", conflicts: " << nconflicts << std::endl;
    nloops++;
    nconflicts = 0;
    qtmpPos = 0L;
  } while (!q.empty());


  ColorElem ncolors = -1;
  size_t csz = colors.size();

#pragma omp parallel for default(none), shared(colors), reduction(max: ncolors), firstprivate(csz), schedule(static)
  for (size_t ci = 0U; ci < csz; ci++)
    if (colors[ci] > ncolors)
      ncolors = colors[ci];

  std::cout << "Total number of colors used: " << ncolors << std::endl;
  std::cout << "Total number of conflicts overall: " << nconflicts << std::endl;
  std::cout << "Number of rounds: " << nloops << std::endl;

  ColorElem myconflicts = 0;

#pragma omp parallel for default(none), shared(g, colors), reduction(+: myconflicts), schedule(guided)
  for (GraphElem v = 0; v < nv; v++) {
    GraphElem e0, e1;

    g.getEdgeRangeForVertex(v, e0, e1);

    for (GraphElem k = e0; k < e1; k++) {
      const Edge &edge = g.getEdge(k);

      if (v == edge.tail)
	continue;

      if (colors[v] == colors[edge.tail])
	myconflicts++;
    }
  }

  myconflicts /= 2;

  if (myconflicts > 0) {
    std::cout << "WARNING: Conflicts detected after resolution: " << myconflicts << std::endl;
    exit(EXIT_FAILURE);
  }
  else
    std::cout << "SUCCESS: No conflicts found" << std::endl;

//   double t0, t1;

//   t0 = mytimer();
//   rebalanceColors(g, colors, ncolors + 1);
//   t1 = mytimer();
//   std::cout << "Rebalancing time: " << (t1 - t0) << std::endl;

  return ncolors;
} // distanceOneVertexColoring

void generateRandomNumbers(RandVec &randVec)
{
  unsigned long int seed[] = { 1UL, 2UL, 3UL, 4UL, 5UL, 6UL };

  RngStream::SetPackageSeed(seed);
  RngStream rngArray;

  for (RandVec::iterator iter = randVec.begin(); iter != randVec.end(); iter++)
    *iter = rngArray.RandU01();
} // generateRandomNumbers

GraphElem resolveConflicts(const Graph &g, ColorVector &colors, const ColorQueue &q,
			   ColorQueue &qtmp, const size_t qsz, const RandVec &randVec,
			   GraphElem &qtmpPos)
{
  GraphElem nconflicts = 0;

#pragma omp for schedule(guided)
  for (GraphElem qi = 0L; qi < qsz; qi++) {
    GraphElem v = q[qi];
    GraphElem e0, e1;

    g.getEdgeRangeForVertex(v, e0, e1);

    for (GraphElem k = e0; k < e1; k++) {
      const Edge &edge = g.getEdge(k);

      if (colors[v] == colors[edge.tail])
	if ((randVec[v] < randVec[edge.tail]) || ((randVec[v] == randVec[edge.tail]) &&
						  (v < edge.tail))) {
	  GraphElem qtInsPos;

// #pragma omp atomic capture
// 	  qtInsPos = qtmpPos++;
	  qtInsPos = __sync_fetch_and_add(&qtmpPos, 1);

	  qtmp[qtInsPos] = v;
	  colors[v] = -1;
	  nconflicts++;
	  break;
	}
    }
  }

  return nconflicts;
} // resolveConflicts

void rebalanceColors(const Graph &g, ColorVector &colors, const ColorElem numColors)
{
  const GraphElem nv = g.getNumVertices();

  ColorVector binSizes(numColors);
  RandVec randVec(nv);

  generateRandomNumbers(randVec);

#pragma omp parallel default(none), shared(colors, binSizes), firstprivate(numColors)
  {
#pragma omp for schedule(guided)
    for (GraphElem i = 0; i < nv; i++)
// #pragma omp atomic update
//       binSizes[colors[i]]++;
      __sync_fetch_and_add(&binSizes[colors[i]], 1);
  }

  ColorElem tsz = 0, maxsz;

  for (ColorElem i = 0; i < numColors; i++)
    tsz += binSizes[i];

  const ColorElem fl = tsz / numColors, ce = (tsz + numColors - 1) / numColors;

  ColorQueue q(nv), qtmp;
  ColorElem qtmpPos = 0;
  ColorElem nconflicts = 0;
  int nloops = 0;

  std::cout << "Number of bins: " << numColors << ", ceil: " << ce << ", floor: " << fl <<
    std::endl;

#pragma omp parallel for default(none), shared(q), schedule(static)
  for (GraphElem i = 0; i < nv; i++)
    q[i] = i;

  do {
    size_t qsz = q.size();

    qtmp.resize(qsz);

#pragma omp parallel default(none), shared(g, colors, binSizes, q, qtmp, qtmpPos, randVec, nconflicts), firstprivate(numColors, ce, qsz)
    {
      // NeighborColors neighborColors;

#pragma omp for schedule(guided)
      for (GraphElem qi = 0; qi < qsz; qi++) {
	BitVector mark(MaxDegree, false);
	GraphElem i = q[qi];
	const ColorElem myc = colors[i];
	GraphElem e0, e1;

	if (binSizes[myc] <= ce)
	  continue;

	g.getEdgeRangeForVertex(i, e0, e1);
	// neighborColors.clear();

	for (GraphElem j = e0; j < e1; j++) {
	  const Edge &edge = g.getEdge(j);
	  const ColorElem tcolor = colors[edge.tail];

	  // neighborColors.insert(colors[edge.tail]);
	  mark[tcolor] = true;
	}

	ColorElem pos;

	for (pos = 0; pos < numColors; pos++) {
	  if ((pos == myc) || (mark[pos] == true)/* (neighborColors.count(pos) > 0) */)
	    continue;
	  if (binSizes[pos] < ce)
	    break;
	}

	if (pos >= numColors)
	  continue;

	if (binSizes[pos] < ce) {
	  colors[i] = pos;
// #pragma omp atomic update
// 	  binSizes[pos]++;
	  __sync_fetch_and_add(&binSizes[pos], 1);
// #pragma omp atomic update
// 	  binSizes[myc]--;
	  __sync_fetch_and_sub(&binSizes[myc], 1);
	}
      }

// #pragma omp atomic update
//       nconflicts += resolveConflicts(g, colors, q, qtmp, qsz, randVec, qtmpPos);
      __sync_fetch_and_add(&nconflicts, resolveConflicts(g, colors, q, qtmp, qsz, randVec,
							 qtmpPos));
    }

    q.resize(qtmpPos);
    for (GraphElem qi = 0; qi < qtmpPos; qi++)
      q[qi] = qtmp[qi];

    qtmpPos = 0;
    maxsz = *std::max_element(binSizes.begin(), binSizes.end());
    std::cout << "Iteration: " << nloops << ", conflicts: " << nconflicts << ", max: " <<
      maxsz << ", ceil: " << ce << std::endl;
    nloops++;
  } while ((maxsz > ce) && !q.empty());
} // rebalanceColors
