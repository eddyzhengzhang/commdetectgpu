#ifndef __LOUVAIN_METHOD_KERNELS_H
#define __LOUVAIN_METHOD_KERNELS_H
typedef std::vector<GraphElem> CommunityVector;
typedef std::vector<GraphElem> GraphElemVector;
typedef GraphElem* CommunityArray;
typedef GraphElem* GraphElemArray;

extern "C" void louvain_execution_cuda(GraphElem numVertices, 
        GraphElem numEdges, Edge *edgeList, EdgeIndexesArray edgeListIndexes, 
        CommunityArray comm, CommArray cinfo, int maxPartition, 
        float constant, GraphElemArray vdegree);

#endif
