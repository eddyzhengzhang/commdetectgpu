#include "stdio.h"
#include <iostream>
#include <cuda.h>
#include "graph.h"
#include "edge_decl.h"
#include <thrust/sort.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "louvainMethod_kernels.cuh"
#include <thrust/inner_product.h>


//TODO change ints to GraphElem
typedef struct{
    double gain;
    int comm;
}ModGain;


/* 
 * A functor used to find the community with the max gain.
 * In the case of equality the lower community id is returned for
 * improved convergence.
 */
struct max_gain : public thrust::binary_function<ModGain, ModGain, ModGain> {
    __host__ __device__ ModGain operator()(const ModGain &a,const ModGain &b) const{
        if(a.gain > b.gain){
            return a;
        }
        if(a.gain < b.gain){
            return b;
        }
        if(a.comm < b.comm){
            return a;
        }
        return b;
    }
};

struct degree2 : public thrust::binary_function<Comm, Comm, Comm> {
    __host__ __device__ double operator()(const Comm &a,const Comm &b) const{
        return static_cast<double>(a.degree) * static_cast<double>(b.degree);
    }
};


//TODO optimize with shared memory
/* 
 * Stores the total edge weight of self loops  for each vertex its community.
 */
__global__ void getSelfDegree(Edge *edgeList, int *keys, int *selfDegree, int numVertices,
        int start, int n){
    int tid = blockDim.x*blockIdx.x+threadIdx.x;
    if(tid < n){
        if(start + keys[tid]/numVertices == edgeList[tid].tail){
            atomicAdd(selfDegree+keys[tid]/numVertices, edgeList[tid].weight);
        }
    }
}

/*
 * Initializes new keys which is used for the max reduce
 * It is the vertex id for each reduced edge weight
 * Also the community for each gain is initialized
 */
__global__ void getNewKeys(int *new_keys, int *keys, ModGain *gain, int start, int numVertices, int n){
    int tid = blockDim.x*blockIdx.x+threadIdx.x;
    if(tid < n){
        new_keys[tid] = start+keys[tid]/numVertices;
        gain[tid].comm = keys[tid]%numVertices;//start?
    }
}

/*
 * maps the gain to each vertex X community
 */
__global__ void map_modularity_change(float *sumEdgeWeights, 
        GraphElem *vdegrees, double constant, 
        CommArray cinfo, int *new_keys, int *keys, 
        float *commWeights, int *selfDegree, 
        ModGain *gain, int numVertices, int n){
    int tid = blockDim.x*blockIdx.x+threadIdx.x;
    if(tid < n){
        double vdegree = vdegrees[new_keys[tid]];
        double eix = commWeights[ keys[tid]/numVertices ] - selfDegree[ keys[tid]/numVertices ];
        double ax = cinfo[ new_keys[tid] ].degree - vdegree;
        double ay = cinfo[gain[tid].comm].degree;
        double eiy = sumEdgeWeights[tid];
        gain[tid].gain = 2.0 * (eiy - eix) - 2.0 * vdegree * (ay - ax) * constant;
    }
}

/* 
 * Finds the total edge weight for each vertex's community
 */
__global__ void getCurrCommWeights(float *commWeights, GraphElem *comm, float *edgeWeights, 
        int *keys, int *new_keys, ModGain *gain, int numVertices, int n){
    int tid = blockDim.x*blockIdx.x+threadIdx.x;
    if(tid < n){
        if(comm[new_keys[tid]] == gain[tid].comm){
            commWeights[ keys[ tid ]/numVertices ] = edgeWeights[tid];
        }
    }
}

/*
 * Updates cupdate given the max gain for each vertex.
 * If the max gain community and current community is the
 * same we do not update
 */
__global__ void update(CommArray cupdate, ModGain *max_gain, GraphElem *vdegree, 
        CommArray cinfo, GraphElem *currComm, GraphElem *target, int n){
    int tid = blockDim.x*blockIdx.x+threadIdx.x;
    if(tid < n){
        GraphElem maxComm = max_gain[tid].comm;
        GraphElem curr = currComm[tid];
        if(maxComm != curr && 
                !(cinfo[maxComm].size == 1 && cinfo[curr].size == 1 && maxComm > curr)){
            atomicAdd(&cupdate[maxComm].degree, vdegree[tid]);
            atomicAdd(&cupdate[maxComm].size, 1);
            atomicAdd(&cupdate[curr].degree, -vdegree[tid]);
            atomicAdd(&cupdate[curr].size, -1);
            target[tid] = maxComm;
        }
    }
}



double computeModularity_cuda(thrust::device_vector<float> &d_cur_comm_weights, 
        thrust::device_vector<Comm> &d_cinfo, int n, double constant, double e_xx){
    thrust::plus<double> b1;
    degree2 b2;
    //double e_xx = thrust::reduce(d_cur_comm_weights.begin(), d_cur_comm_weights.begin()+n, 0.0);
    double a2_x = thrust::inner_product(d_cinfo.begin(), d_cinfo.begin() + n,
            d_cinfo.begin(), 0, b1, b2);
    double currMod = (e_xx * constant) - (a2_x * constant*constant);

    std::cout << "GPU: " << std::endl;
    std::cout << "e_xx: " << e_xx << ", a2_x: " << a2_x << ", currMod: " << currMod << std::endl;
    return currMod;
}

extern "C" void louvain_execution_gpu()
{

}


/*
 * Calcuates the next iterations community.
 */
extern "C" void louvain_execution_cuda(GraphElem numVertices, 
        GraphElem numEdges, Edge *edgeList, EdgeIndexesArray edgeListIndexes, 
        CommunityArray comm, CommArray cinfo, int maxPartition, 
        float constant, GraphElemArray vdegree/*, Comm* cvect*/){
    //cvect is final comm

    //init all arrays
    thrust::device_vector<Comm> d_cinfo(cinfo, cinfo+numVertices);
    thrust::device_vector<GraphElem> d_vdegrees(vdegree, vdegree+numVertices);
    //thrust::device_vector<Edge> d_edgeList;
    thrust::host_vector<int> h_keys;
    //new keys is just vertex for each reduced edge and needed for reduction
    thrust::device_vector<int> d_keys, d_reduce_keys, d_edge_weights, d_new_keys, d_selfDegree;
    thrust::device_vector<ModGain> d_gain, d_max_gain;
    thrust::device_vector<float> d_edgeWeights, d_reduce_vals;
    thrust::device_vector<float> d_cur_comm_weights;//TODO: could be saved from prev iteration?
    thrust::device_vector<GraphElem> d_comm(comm, comm+numVertices);
    thrust::device_vector<GraphElem> d_target(comm, comm+numVertices);
    thrust::device_vector<Comm> d_cupdate(cinfo, cinfo+numVertices);
    d_cur_comm_weights.resize(maxPartition);
    h_keys.resize(maxPartition);
    d_edgeWeights.resize(maxPartition);
    d_keys.resize(maxPartition);
    d_selfDegree.resize(maxPartition);
    d_new_keys.resize(maxPartition);
    d_reduce_vals.resize(maxPartition);
    d_reduce_keys.resize(maxPartition);
    d_edge_weights.resize(maxPartition);
    d_gain.resize(maxPartition);
    d_max_gain.resize(maxPartition);
    //d_edgeList.resize(maxPartition);
    double e_xx=0;
    int start = 0;
    int end = 0;
    while(start < numVertices){
        //reset partition specific values
        thrust::fill(d_cur_comm_weights.begin(), d_cur_comm_weights.end(), 0);
        thrust::fill(d_edge_weights.begin(), d_edge_weights.end(), 0);
        thrust::fill(d_selfDegree.begin(), d_selfDegree.end(), 0);
        thrust::fill(d_keys.begin(), d_keys.end(), 0);
        thrust::fill(h_keys.begin(), h_keys.end(), 0);
        thrust::fill(d_new_keys.begin(), d_new_keys.end(), 0);
        thrust::fill(d_reduce_keys.begin(), d_reduce_keys.end(), 0);
        thrust::fill(d_reduce_vals.begin(), d_reduce_vals.end(), 0);
        ModGain gtmp;
        gtmp.gain = 0;
        gtmp.comm = 0;
        thrust::fill(d_gain.begin(), d_gain.end(), gtmp);
        thrust::fill(d_max_gain.begin(), d_max_gain.end(), gtmp);

        //get next partition
        GraphElem total = 0;
        while((int)(total + edgeListIndexes[end+1]-edgeListIndexes[end]) < maxPartition && end < numVertices){
            total += edgeListIndexes[end+1]-edgeListIndexes[end];
            end++;
        }
        printf("First partition at %d end %d, total %d\n", start, end, total);
        //create unique keys
        //TODO will zipping vertex, community be better since needed later
        int left = edgeListIndexes[start+1]-edgeListIndexes[start];
        int v=0;
        for(int i = 0; i < total; i++){
            h_keys[i] = comm[ edgeList[edgeListIndexes[start]+i].tail ] + v*numVertices;
            left--;
            if(left <= 0){
                v++;
                left = edgeListIndexes[v+start+1]-edgeListIndexes[v+start];
            }
        }
        //TODO move alloc memory above
        thrust::device_vector<Edge> d_edgeList(edgeList+edgeListIndexes[start],
                edgeList+edgeListIndexes[end]);

        d_keys = h_keys;
        thrust::sort_by_key(d_keys.begin(), d_keys.begin()+total, d_edgeList.begin());

        thrust::host_vector<Edge> tmp = d_edgeList;
        //TODO optimize dont copy to host, dont know how with structs...
        for(int i = 0; i < total; i++){
            d_edgeWeights[i] = tmp[i].weight;
        }

        thrust::pair<thrust::device_vector<int>::iterator, thrust::device_vector<float>::iterator> itrs;
        itrs = thrust::reduce_by_key(d_keys.begin(), d_keys.begin()+total, 
                d_edgeWeights.begin(), d_reduce_keys.begin(), d_reduce_vals.begin());
        int sumUniqueClusters  = itrs.second - d_reduce_vals.begin();

        //TODO unique num threads, blocks per kernel
        int threads = 256;
        int blocks = (total+255)/256;
        getNewKeys<<<blocks,threads>>>(thrust::raw_pointer_cast(&d_new_keys[0]), 
                thrust::raw_pointer_cast(&d_reduce_keys[0]), 
                thrust::raw_pointer_cast(&d_gain[0]), 
                start, numVertices, sumUniqueClusters );
        getSelfDegree<<<blocks,threads>>>(thrust::raw_pointer_cast(&d_edgeList[0]), 
                thrust::raw_pointer_cast(&d_keys[0]), 
                thrust::raw_pointer_cast(&d_selfDegree[0]), 
                numVertices, start, total);

        getCurrCommWeights<<<blocks, threads>>>(thrust::raw_pointer_cast(&d_cur_comm_weights[0]), 
                thrust::raw_pointer_cast(&d_comm[0]), 
                thrust::raw_pointer_cast(&d_reduce_vals[0]), 
                thrust::raw_pointer_cast(&d_reduce_keys[0]), 
                thrust::raw_pointer_cast(&d_new_keys[0]),
                thrust::raw_pointer_cast(&d_gain[0]),
                numVertices, sumUniqueClusters);

        std::cout << "Cur com weigh: ";
        for(int i =0 ; i < sumUniqueClusters; i++){
            std::cout << d_cur_comm_weights[i] << " ";
        }
        std::cout << std::endl;
        map_modularity_change<<<blocks,threads>>>(thrust::raw_pointer_cast(&d_reduce_vals[0]), 
                thrust::raw_pointer_cast(&d_vdegrees[0]), constant, 
                thrust::raw_pointer_cast(&d_cinfo[0]),
                thrust::raw_pointer_cast(&d_new_keys[0]), 
                thrust::raw_pointer_cast(&d_reduce_keys[0]), 
                thrust::raw_pointer_cast(&d_cur_comm_weights[0]), 
                thrust::raw_pointer_cast(&d_selfDegree[0]), 
                thrust::raw_pointer_cast(&d_gain[0]),
                numVertices, sumUniqueClusters);
        //TODO would zipping the gain and communityID be faster than a struct?
        //the thrust powerpoint says its 3x faster

        //keys vertex
        //reduce by max of modgain.gain
        thrust::equal_to<int> binary_pred;
        max_gain op;
        thrust::reduce_by_key(d_new_keys.begin(), d_new_keys.begin()+sumUniqueClusters, 
                d_gain.begin(), d_reduce_keys.begin(), d_max_gain.begin(), binary_pred, op);

        update<<<(v-255)/256,256>>>(thrust::raw_pointer_cast(&d_cupdate[0]),
               thrust::raw_pointer_cast(&d_max_gain[0]),
               thrust::raw_pointer_cast(&d_vdegrees[0]),
               thrust::raw_pointer_cast(&d_cinfo[0]),
               thrust::raw_pointer_cast(&d_comm[0]),
               thrust::raw_pointer_cast(&d_target[0]),
               v);

        e_xx += thrust::reduce(d_cur_comm_weights.begin(), d_cur_comm_weights.begin()+v, 0.0);

        //update next comm
        start = end;
        std::cout << "next\n";

    }
    double modularity = computeModularity_cuda(d_cur_comm_weights, d_cinfo, numVertices, constant, e_xx);
}
