#include <cmath>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "loadDimacs.h"
#include "loadUtils.h"

void loadDimacsFile(Graph *&g, const std::string &fileName)
{
  std::ifstream ifs;

  ifs.open(fileName.c_str());
  if (!ifs) {
    std::cerr << "Error opening Dimacs format file: " << fileName << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  char comment;

  do {
    std::getline(ifs, line);
    comment = line[0];
  } while (comment == 'c');

  long int numVertices, numEdges, value = 0L;
  {
    std::string p, sp;

    std::istringstream iss(line);

    iss >> p >> sp >> numVertices >> numEdges;
    if (!iss || iss.fail() || p[0] != 'p') {
      std::cerr << "Error parsing DIMACS format: " << line << std::endl;
      std::cerr << "p: " << p << ", p[0]: " << p[0] << ", sp: " << sp <<
	", numVertices: " << numVertices << ", numEdges: " << numEdges << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  std::cout << "Loading DIMACS file: " << fileName << ", numvertices: " << numVertices <<
    ", numEdges: " << numEdges << std::endl;

  std::vector<GraphElem> edgeCount(numVertices + 1L);
  std::vector<GraphElemPair> edgeList(numEdges * 2L);

  for (long int i = 0; i < numEdges; i++) {
    std::string label;
    long int source, dest;
    double weight;

    ifs >> label >> source >> dest >> weight;

    if (!ifs || ifs.fail()) {
      std::cerr << "Error parsing DIMACS edge" << std::endl;
      exit(EXIT_FAILURE);
    }

    if ((i % 100000L) == 0L)
      std::cout << "Reading edge: (" << source << ", " << dest << ") with weight: " <<
	weight << std::endl;

    GraphElemPair &fedge = edgeList[2L * i];
    GraphElemPair &bedge = edgeList[(2L * i) + 1L];

    fedge.first = source - 1L;
    fedge.second = dest - 1L;

    bedge.first = dest - 1L;
    bedge.second = source - 1L;

    edgeCount[fedge.first + 1L]++;
    edgeCount[bedge.first + 1L]++;
  }

  ifs.close();

  numEdges *= 2L;
  g = new Graph(numVertices, numEdges);

  processGraphData(*g, edgeCount, edgeList, numVertices, numEdges);
} // loadDimacsFile
