#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "graph.h"
#include "loadBinary.h"
#include "loadUtils.h"
#include "timer.h"

static std::string inputFileName, clustersFileName;

static void parseCommandLine(const int argc, char * const argv[]);
static void parseClustersFile(std::vector<GraphElem> &parts, const std::string &partFileName,
			      const GraphElem nv);

int main(int argc, char *argv[])
{
  parseCommandLine(argc, argv);

  Graph *g = NULL;

  rusage rus;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  loadBinaryFile(g, inputFileName);

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Loaded graph from file: " << inputFileName << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  assert(g);

  double t0, t1, t2, t3;

  t0 = mytimer();

  GraphElem nv, ne;

  nv = g->getNumVertices();
  ne = g->getNumEdges();

  t2 = mytimer();
  std::vector<GraphElem> parts(nv);
  int nparts;

  parseClustersFile(parts, clustersFileName, nv);
  std::vector<GraphElem>::const_iterator miter = std::max_element(parts.begin(), parts.end());
  nparts = (*miter) + 1;
  std::cout << "Number of clusters: " << nparts << std::endl;
  t3 = mytimer();
  std::cout << "File read and copy time: " << (t3 - t2) << std::endl;

  std::vector<GraphElem> clusterWeight(nparts);

  for (GraphElem i = 0; i < nv; i++) {
    GraphElem e0, e1;

    g->getEdgeRangeForVertex(i, e0, e1);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = g->getEdge(j);

      if (parts.at(edge.tail) == parts.at(i))
	clusterWeight.at(parts.at(i)) += edge.weight;
    }
  }

  std::vector<GraphElem> degrees(nparts);
  GraphElem localWeight = 0;

  for (GraphElem i = 0; i < nv; i++) {
    GraphElem e0, e1;
    GraphElem tw = 0;

    g->getEdgeRangeForVertex(i, e0, e1);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = g->getEdge(j);

      tw += edge.weight;
    }

    localWeight += tw;
  }

  for (int i = 0; i < nparts; i++) {
    GraphElem tw = 0;

    for (GraphElem j = 0; j < nv; j++) {
      GraphElem e0, e1;

      if (parts.at(j) == i) {
	g->getEdgeRangeForVertex(j, e0, e1);

	tw += (e1 - e0);
      }
    }

    degrees.at(i) = tw;

    std::cout << "Cluster: " << i << ", tw: " << tw << std::endl;
  }

  double constantForSecondTerm = 1.0 / static_cast<double>(localWeight);
  double e_xx = 0.0, a2_x = 0.0;

  for (int i = 0; i < nparts; i++) {
    e_xx += clusterWeight.at(i);
    a2_x += static_cast<double>(degrees.at(i)) * static_cast<double>(degrees.at(i));
   }
  
  double currMod = (e_xx * constantForSecondTerm) - (a2_x * constantForSecondTerm *
						     constantForSecondTerm);

  std::cout << "e_xx: " << e_xx << ", a2_x: " << a2_x << ", currMod: " << currMod << std::endl;
  std::cout << "constantForSecondTerm: " << constantForSecondTerm << std::endl;

  t1 = mytimer();
  std::cout << "Total processing time: " << (t1 - t0) << std::endl;

  delete g;

  return 0;
} // main

void parseCommandLine(const int argc, char * const argv[])
{
  int ret;

  while ((ret = getopt(argc, argv, "f:c:")) != -1) {
    switch (ret) {
    case 'f':
      inputFileName.assign(optarg);
      break;
    case 'c':
      clustersFileName.assign(optarg);
      break;
    default:
      assert(0 && "Should not reach here!!");
      break;
    }
  }

  if (inputFileName.empty()) {
    std::cerr << "Must specify an input file name with -f" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (clustersFileName.empty()) {
    std::cerr << "Must specify a clusters file name with -c" << std::endl;
    exit(EXIT_FAILURE);
  }
} // parseCommandLine

void parseClustersFile(std::vector<GraphElem> &parts, const std::string &partFileName,
		       const GraphElem nv)
{
  std::ifstream ifs;

  ifs.open(partFileName, std::ifstream::in);

  if (!ifs) {
    std::cerr << "Error opening clusters file: " << partFileName << std::endl;
    exit(EXIT_FAILURE);
  }

  int *iparts = new int[nv];
  int part;
  GraphElem pos = 0;

  do {
    ifs >> part;
    if (pos < nv)
      iparts[pos] = part;

    if (pos < 10 || pos > (nv - 10))
      std::cout << "Vertex pos: " << pos << ", partition: " << part << std::endl;

    if (!ifs.eof())
      pos++;
  } while (ifs && !ifs.eof());

  std::cout << "Final position: " << pos << ", nv: " << nv << std::endl;
  assert(pos == nv);

  std::copy(iparts, iparts + nv, parts.begin());
  delete [] iparts;
} // parseClustersFile
