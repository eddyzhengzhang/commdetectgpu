#include <cassert>
#include <cstdint>

#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <vector>
#include <utility>

#include "loadBinary.h"
#include "loadUtils.h"
#include "timer.h"

void loadBinaryFile(Graph *&g, const std::string &fileName)
{
  std::ifstream ifs;

  double t0, t1, t2, t3;

  t0 = mytimer();

  ifs.open(fileName.c_str(), std::ifstream::in | std::ifstream::binary);
  if (!ifs) {
    std::cerr << "Error opening binary format file: " << fileName << std::endl;
    exit(EXIT_FAILURE);
  }

  long int nv, ne;
  int32_t header, nvi, nei;

  ifs.read(reinterpret_cast<char *>(&header), sizeof(header));

  assert((header == 32) || (header == 64));

  if (header == 32) {
    ifs.read(reinterpret_cast<char *>(&nvi), sizeof(nvi));
    ifs.read(reinterpret_cast<char *>(&nei), sizeof(nei));

    nv = nvi;
    ne = nei;
  }
  else {
    ifs.read(reinterpret_cast<char *>(&nv), sizeof(nv));
    ifs.read(reinterpret_cast<char *>(&ne), sizeof(ne));
  }

  std::cout << "Loading " << header << "-bit binary file: " << fileName << ", numvertices: " <<
    nv << ", numEdges: " << ne << std::endl;

  std::vector<GraphElem> edgeCount(nv + 1L);
  std::vector<GraphElemPair> edgeList;

  if (header == 32) {
    int32_t *edgeListRaw = new int32_t[ne * 2L];

    ifs.read(reinterpret_cast<char *>(edgeListRaw), sizeof(int32_t) * (ne * 2L));

    edgeList.resize(ne);
    for (long int i =0L; i < ne; i++) {
      edgeList[i].first = edgeListRaw[(2L * i)];
      edgeList[i].second = edgeListRaw[(2L * i) + 1L];

      edgeCount[edgeList[i].first + 1L]++;
    }
    delete [] edgeListRaw;
  }
  else {
    int64_t *edgeListRaw = new int64_t[ne * 2L];

    ifs.read(reinterpret_cast<char *>(edgeListRaw), sizeof(int64_t) * (ne * 2L));

    edgeList.resize(ne);
    for (long int i = 0L; i < ne; i++) {
      edgeList[i].first = edgeListRaw[(2L * i)];
      edgeList[i].second = edgeListRaw[(2L * i) + 1L];

      edgeCount[edgeList[i].first + 1L]++;
    }
    delete [] edgeListRaw;
  }
  ifs.close();

  t2 = mytimer();

  std::cout << "File read time: " << (t2 - t0) << std::endl;

  t2 = mytimer();

  std::cout << "Before allocating graph" << std::endl;

  g = new Graph(nv, ne);

  assert(g);

  std::cout << "Allocated graph: " << g << std::endl;

  processGraphData(*g, edgeCount, edgeList, nv, ne);

  t3 = mytimer();  

  std::cout << "Graph populate time: " << (t3 - t2) << std::endl;
  std::cout << "Total I/O time: " << (t3 - t0) << std::endl;
} // loadBinaryFile
