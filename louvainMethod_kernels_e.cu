#include "stdio.h"
#include <iostream>
#include <cuda.h>
#include "graph.h"
#include "edge_decl.h"
//#include "louvainMethod_kernels_e.cuh"
#include "gpuParam.h"
#include "localSort_kernel.cu"



/* kernel functions start from here */
__global__ void initGraphCommKernel(GraphElem * inPastCommG, GraphElem* inCurrCommG, GraphElem nv)
{
	int tid = blockIdx.x*BLOCK_SIZE + threadIdx.x;
	int totalthreads = blockDim.x * gridDim.x;
	for ( int i = tid; i < nv; i+= totalthreads)
	{
		inPastCommG[i] = i;
		inCurrCommG[i] = i;
	}
}


__global__ void	localSortKernelTest(GraphElem* loadPartG, GraphElem* edgeRangeG, GraphElem* edgeEndG, GraphElem nv, GraphElem warpnum)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	if ( warpid >= warpnum ) return;

	int warptaskBeg = loadPartG[warpid];
	int warptaskEnd = loadPartG[warpid+1];

	for ( int i = warptaskBeg; i < warptaskEnd; i++ )
	{
		/*for ( int j = edgeRangeG[i] + warplane; j < edgeRangeG[i+1]; j += 32)*/
		/*{*/
		/*printf("Testing: edgeEnd[%d] %d\n", j, edgeEndG[j]);*/
		/*}*/
		printf("\n");
	}
}

__global__ void initNweightKernel(GraphElem* degreeG, GraphElem * edgeRangeG, GraphWeight* edgeWeightG, GraphElem ne, GraphElem * loadPartG, int warpnum)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	if ( warpid >= warpnum ) return;

	int warptaskBeg = loadPartG[warpid];
	int warptaskEnd = loadPartG[warpid+1];

	/*if ( warplane == 0 )*/
	/*printf("warpid %d", warpid);*/

	for ( int i = warptaskBeg; i < warptaskEnd; i++ )
	{
		for ( int j = edgeRangeG[i] + warplane; j < edgeRangeG[i+1]; j += 32)
		{
			atomicAdd(&degreeG[i], (GraphElem)edgeWeightG[j]);
		}
		/*printf("warpid %d i %d\n", warpid, i);*/
	}
}


__global__ void initCommInfo(GraphElem * commInfoSizeG, GraphElem * commInfoDegreeG, GraphElem * degreeG, GraphElem nv)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	int totalthread = blockDim.x * blockDim.x;

	for ( int i = tid; i < nv; i += totalthread )
	{
		commInfoDegreeG[i] = degreeG[i];
		commInfoSizeG[i] = 1;
	}
}


__global__ void initConstant2ndTermKernel(GraphElem * degreeG, float * const2ndTermG, GraphElem nv)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int totalthreads = blockDim.x * gridDim.x;
	int taskid = tid;
	for ( taskid = tid; taskid < nv; taskid += totalthreads )
	{
		atomicAdd(const2ndTermG, degreeG[taskid]);
	}
}

__global__ void getConst2ndTermKernel(float * const2ndTermG)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if ( tid == 0 )
		const2ndTermG[0] = 1.0/const2ndTermG[0];
}

__global__ void getAxKernel(GraphElem * commInfoDegreeG, GraphElem * inCurrCommG, GraphElem * degreeG, GraphElem * axG, GraphElem nv)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int totalthreads = blockDim.x * gridDim.x;
	for ( int i = tid; i < nv; i += totalthreads)
	{
		axG[i] = commInfoDegreeG[inCurrCommG[i]] - degreeG[i];
	}
}

__global__ void getEixKernel(GraphElem* loadPartG, GraphElem * edgeRangeG, GraphElem * edgeEndG, GraphWeight * edgeWeightG, GraphElem * inCurrCommG, GraphElem * eixG, int warpnum, GraphWeight* sameClusterWeightG )
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	if ( warpid >= warpnum ) return;

	int warptaskBeg = loadPartG[warpid];
	int warptaskEnd = loadPartG[warpid+1];


	//loop until we find out a region that correspond to the community the node
	//itself is in
	for ( int i = warptaskBeg; i < warptaskEnd; i++ )
	{
		GraphElem currComm = inCurrCommG[i];
		int sameComm;
		int j;
		for ( j = edgeRangeG[i]+warplane; j < edgeRangeG[i+1]; j += 32 )
		{
			GraphElem neighComm = inCurrCommG[edgeEndG[j]];
			unsigned int poll = __ballot(currComm == neighComm);
			if ( poll != 0 ) break;
		}
		sameComm = j;
		//we have found the position where the same comm starts
		int neighId;
		GraphElem neighComm;
		for ( j = sameComm; j < edgeRangeG[i+1]; j += 32 )
		{
			neighComm = inCurrCommG[edgeEndG[j]];
			neighId = edgeEndG[j];
			if ( neighId != i && neighComm == currComm) atomicAdd(&eixG[i], edgeWeightG[j]);
			if ( neighComm == currComm) { 
				atomicAdd(&sameClusterWeightG[i], edgeWeightG[j]);
				/*printf("Debugging same cluster: %d org %d new %d edge %d endG %d currComm %d neighComm %d\n", i, (int)sameClusterWeightG[i], (int)edgeWeightG[j], j, edgeEndG[j], currComm, neighComm);*/
			}
			if ( i == 11 ) printf("Debugging eix for node %d neighId %d i %d neighComm %d currComm %d, eix %d\n", i, neighId, i, neighComm, currComm, eixG[i]);
			unsigned int poll = __ballot(currComm == neighComm);
			if ( poll == 0 ) break;
		}
		/*if ( warplane == 0 ) {*/
		/*printf("samecluster %d is %f warpid %d\n", i, sameClusterWeightG[i], warpid);*/
		/*}*/
	}
	
}

__global__ void setIdxKernel(GraphElem * idxG, GraphElem nv)
{
	int tid = threadIdx.x + blockDim.x*blockIdx.x;
	int total_threads = blockDim.x*gridDim.x;

	for ( int i = tid; i < nv; i+= total_threads )
			idxG[i] = i;

}

__global__ void setCommCopyKernel(GraphElem * edgeEndG, GraphElem * inCurrCommG, GraphElem * commCopyG, GraphElem ne)
{
		int tid = threadIdx.x + blockDim.x*blockIdx.x;
	int total_threads = blockDim.x*gridDim.x;

	for ( int i = tid; i < ne; i+= total_threads )
	{
			commCopyG[i] = inCurrCommG[edgeEndG[i]];
			/*printf("Testing commCopy %d %d\n", i, commCopyG[i]);*/
	}
}

__global__ void assignEdgeWeightCopy(GraphWeight * edgeWeightCopyG, GraphWeight * edgeWeightG,  GraphElem * idxG, GraphElem ne)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int total_threads = blockDim.x * gridDim.x;

	for ( int i = tid; i < ne; i += total_threads )
	{
		edgeWeightCopyG[i] = edgeWeightG[idxG[i]];
		/*printf("Testing idxG[%d] %d\n", i, idxG[i]);*/
	}

}

__global__ void copyKernel(GraphWeight * edgeWeightG, GraphWeight * edgeWeightCopyG, GraphElem ne)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int total_threads = blockDim.x * gridDim.x;

	for ( int i = tid; i < ne; i += total_threads )
	{
		edgeWeightG[i] = edgeWeightCopyG[i];
		printf("Testing edge weight copy edgeWeight[%d] %f\n", i, edgeWeightG[i]);
	}

}

__global__ void getTargetCommKernel(GraphElem * loadPartG, int warpnum, GraphElem * inCurrCommG, GraphElem * inTargetCommG, GraphElem * edgeRangeG, GraphElem * edgeEndG, GraphWeight * edgeWeightG,  GraphElem * degreeG, GraphElem* commInfoDegreeG, GraphElem* commInfoSizeG, GraphElem * eixG, GraphElem * axG, double const2ndTermG )
{
	extern __shared__ double weightSum[];
	GraphElem * comm = (GraphElem *) &weightSum[blockDim.x];
	double * cGain = (double *) & comm[blockDim.x];
	GraphWeight * carryVal = (GraphWeight *) &cGain[blockDim.x];
	double * maxGain = (double *) &carryVal[blockDim.x>>5];
	GraphElem * maxGainCommId = (GraphElem*) &maxGain[blockDim.x>>5];
	double const2ndTerm = const2ndTermG;
	
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int warpid = (tid>>5);
	int warplane = (tid&31);
	if ( warpid >= warpnum ) return;


	int warptaskBeg = loadPartG[warpid];
	int warptaskEnd = loadPartG[warpid+1];

	/*if ( warplane == 0 && warpid == 0) {*/
	/*printf("warp id %d warptaskBeg %d warptaskEnd %d\n", warpid, warptaskBeg, warptaskEnd);*/
	/*printf("weightsum smem space %d\n", blockDim.x * sizeof(double));*/
	/*printf("comm smem space %d\n", blockDim.x * sizeof(GraphElem));*/
	/*printf("cGain smem space %d\n", blockDim.x * sizeof(double));*/
	/*printf("carryVal smem space %d\n", (blockDim.x>>5) * sizeof(GraphWeight));*/
	/*printf("maxGain smem space %d\n", (blockDim.x>>5) * sizeof(double));*/
	/*printf("maxGainCommId smem space %d\n", (blockDim.x>>5) * sizeof(GraphElem));*/
	/*}*/
	for ( int i = warptaskBeg; i < warptaskEnd; i++ )
	{
		GraphElem currComm = inCurrCommG[i];
		weightSum[threadIdx.x] = 0;
		comm[threadIdx.x] = 0;
		int j;
		if ( warplane == 0 )
		{
			maxGain[warpid] = 0;
			maxGainCommId[warpid] = currComm;
			cGain[warpid] = 0;
			carryVal[warpid] = 0;
		}
		int cloop = 0;
		/*if ( warplane == 0 )*/
		/*printf("Testing: eixG %d is %d, axG %d\n", i, eixG[i], axG[i]);*/
		for ( j = edgeRangeG[i]+warplane; j < edgeRangeG[i+1]; j += 32 )
		{
			GraphElem neighComm = inCurrCommG[edgeEndG[j]];
			GraphWeight neighWeight = edgeWeightG[j];
			comm[threadIdx.x] = neighComm;
			weightSum[threadIdx.x] = neighWeight;
			cGain[threadIdx.x] = 0;
			/*if ( i == 9) printf("cur node %d neigh %d neighComm %d currComm %d nCommComp %d weight %f warpid %d edgeRange %d-%d\n", i, edgeEndG[j], neighComm, currComm,inCurrCommG[edgeEndG[j+1]], neighWeight, warpid, edgeRangeG[i], edgeRangeG[i+1]);*/
			if ( warplane == 0 ) weightSum[warpid<<5] += carryVal[warpid];
			if ( (warplane>=1) && neighComm == comm[threadIdx.x-1])
				weightSum[threadIdx.x] += weightSum[threadIdx.x-1]; 
			if ( (warplane>=2) && neighComm == comm[threadIdx.x-2])
				weightSum[threadIdx.x] += weightSum[threadIdx.x-2]; 
			if ( (warplane>=4) && neighComm == comm[threadIdx.x-4])
				weightSum[threadIdx.x] += weightSum[threadIdx.x-4];
			if ( (warplane>=8) && neighComm == comm[threadIdx.x-8])
				weightSum[threadIdx.x] += weightSum[threadIdx.x-8]; 
			if ( (warplane>=16) && neighComm == comm[threadIdx.x-16])
				weightSum[threadIdx.x] += weightSum[threadIdx.x-16];
			/*if ( warpid == 0) printf("cur node %d neigh %d weight %f warpid %d edgeRange %d-%d\n", i, edgeEndG[j], neighWeight, warpid, edgeRangeG[i], edgeRangeG[i+1]);*/
			comm[threadIdx.x] = 0;
			if ( warplane == 0 ) carryVal[warpid] = 0;	
			if ( neighComm != currComm ) {
				if ( j == edgeRangeG[i+1] - 1 || ( j!= edgeRangeG[i+1] - 1 && neighComm != inCurrCommG[edgeEndG[j+1]]) ) {
					//put calculated value into shared memory, ey, ay
					//perform atomic exchange
					//double array shared mem to store currGain, then perform maxim look up
					double eiy = (double) weightSum[threadIdx.x];
					double ay = commInfoDegreeG[neighComm];
					double currGain = 2.0*(eiy - eixG[i]) - 2.0*degreeG[i]*(ay - axG[i])*const2ndTerm;
					cGain[threadIdx.x] = currGain;
					comm[threadIdx.x] = 1; //used to store the value that needs to be checked
					/*if ( i == 11 ) */
					/*printf("Warpid %d tid %d cGain %lf const2ndTerm %lf eiy %lf ay %lf neighnode %d maxGain %lf eix %d ax %d j %d\n", i ,threadIdx.x, cGain[threadIdx.x], const2ndTermG, eiy, ay, edgeEndG[j], maxGain[warpid], eixG[i], axG[i], j);*/
					
					if ( cGain[threadIdx.x] < maxGain[warpid] && maxGainCommId[warpid]!= -1 && comm[threadIdx.x] ) 
						comm[threadIdx.x] = 0;
				}
			
			}
			if ( j != edgeRangeG[i+1] - 1 && warplane == 31 && (neighComm != inCurrCommG[edgeEndG[j+1]])) {
					carryVal[warpid] = weightSum[threadIdx.x]; 
			}

			if ( warplane == 0 )
			{
				for ( int k = (warpid<<5); k < (warpid<<5) + 32; k++ )
				{
					int cid =edgeRangeG[i]+32*cloop + k-(warpid<<5);
					int nodeid = edgeEndG[cid]; 
					/*if ( i == 11 ) printf("%d comm[%d] %d cid %d edgeRangeG[%d+1] eix %lf ai %lf eiy %lf ay %d\n", i, k, comm[k], cid, edgeRangeG[i+1], eixG[i], axG[i], weightSum[k], commInfoDegreeG[edgeEndG[k]] );*/
					if ( comm[k]==1 && cid < edgeRangeG[i+1])
					{
						/*if ( i == 11 ) printf("i %d really executed cgain %d %lf maxGain %lf maxGainCommId %d\n", i, k, cGain[k], maxGain[warpid], maxGainCommId[warpid]);*/
						GraphElem tempCommId = inCurrCommG[nodeid];
						if ( maxGainCommId[warpid] == - 1 || ( maxGainCommId[warpid]!= -1 && 
						( cGain[k] > maxGain[warpid] || ( cGain[k] != 0 && cGain[k] == maxGain[warpid] && tempCommId < maxGainCommId[warpid] )  )) )
						{
							/*if ( i == 11 )*/
							/*printf("Replaced comm %d with %d prev gain %lf, curr gain %lf k %d eix %lf ax %lf eiy %lf ay %lf\n", maxGainCommId[warpid], tempCommId, maxGain[warpid], cGain[k], k, edgeRangeG[i], edgeRangeG[i+1], warpid, (double)eixG[currComm], (double)axG[currComm], (double)weightSum[k], (double)commInfoDegreeG[tempCommId]);*/
							maxGainCommId[warpid] = tempCommId;
							maxGain[warpid] = cGain[k];
							
						}	
					}
				}
				/*if ( warplane ==0 && i == 1)*/
				/*printf("warpid %d node %d has max gain neighbour %d\n", warpid, i, maxGainCommId[warpid]);*/
			}
			
			
						cloop++;
		}
		//find out gain and target id, if haven't reached other loop, then do not
		//write it back
		//last thread write th maxim back
		if ( warplane == 0)
		{
			int maxCid = maxGainCommId[warpid];
			int curCid = inCurrCommG[i];
			if ( !( commInfoSizeG[maxCid] == 1 && commInfoSizeG[curCid] == 1 &&  maxCid > curCid) )
			{
				inTargetCommG[i] = maxGainCommId[warpid];
				/*if ( warpid == 5 ) printf("node %d maxgaincomsize %d currCommSize %d maxGainCommId %d currCommId %d\n", i, commInfoSizeG[maxCid], commInfoSizeG[curCid], maxCid, curCid );*/
			}
			else {
				inTargetCommG[i] = inCurrCommG[i];
			}
		}
		/*if ( warplane == 0 ) printf("warp %d node %d targetcomm %d maxGainCommId %d\n", warpid, i, inTargetCommG[i], maxGainCommId[warpid]);*/
	}

}

__device__ void reduceDoubleBlock(double * vsem)
{ 
	double mysum = vsem[threadIdx.x];
	int thid = threadIdx.x;
	if ( BLOCK_SIZE >= 1024 )
	{
	if (thid < 512)
		{
			vsem[thid] = mysum = mysum + vsem[thid+512];
		}
		__syncthreads();

	}
	if ( BLOCK_SIZE >= 512 )
	{
		if (thid < 256)
		{
			vsem[thid] = mysum = mysum + vsem[thid+256];
		}
		__syncthreads();
	}
	if ( BLOCK_SIZE >= 256 )
	{
		if (thid < 128)
		{
			vsem[thid] = mysum = mysum + vsem[thid+128];
		}
		__syncthreads();
	}
	if ( BLOCK_SIZE >= 128 )
	{
		if (thid < 64)
		{
			vsem[thid] = mysum = mysum + vsem[thid+64];
		}
		__syncthreads();
	}
	if ( thid < 32 )
	{
		volatile double * stemp = vsem;
		if ( BLOCK_SIZE >= 64 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+32]; 
		}
		if ( BLOCK_SIZE >= 32 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+16]; 
		}
		if ( BLOCK_SIZE >= 16 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+8]; 
		}
		if ( BLOCK_SIZE >= 8 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+4]; 
		}
		if ( BLOCK_SIZE >= 4 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+2]; 
		}
		if ( BLOCK_SIZE >= 2 )
		{
			stemp[thid] = mysum = mysum + stemp[thid+1]; 
		}


	}


}


__global__ void getE_xxKernel(GraphWeight * sameClusterWeightG, double * e_xxG, GraphElem nv)
{
	extern __shared__ double smemWeight[];
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	smemWeight[threadIdx.x] = 0;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		smemWeight[threadIdx.x] += (double) sameClusterWeightG[i];
	}

	__syncthreads();

	reduceDoubleBlock(smemWeight);
	if ( threadIdx.x == 0 && tid < nv) e_xxG[blockIdx.x] = smemWeight[threadIdx.x];
}



__global__ void getA2_xKernel(GraphElem * commInfoDegreeG, double * a2_xG, GraphElem nv)
{
	extern __shared__ double smemDegree[];
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	smemDegree[threadIdx.x] = 0;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		smemDegree[threadIdx.x] += (double) commInfoDegreeG[i] * (double) commInfoDegreeG[i];
		/*printf("Debugging: tid %d commInfoDegreeG %d smemdegree %lf\n", tid, commInfoDegreeG[i], smemDegree[threadIdx.x]);*/
	}

	__syncthreads();

	reduceDoubleBlock(smemDegree);
	/*for ( int i = tid; i < nv; i += gridsize) */
	/*{*/
	/*printf("Debugging: tid %d commInfoDegreeG %d smemdegree %lf\n", tid, commInfoDegreeG[i], smemDegree[threadIdx.x]);*/
	/*}*/

	if ( threadIdx.x == 0 && tid < nv ) {
		a2_xG[blockIdx.x] = smemDegree[threadIdx.x];
		printf("a2_xG %lf bidx %d\n", a2_xG[blockIdx.x], blockIdx.x);
	}
}


__global__ void	getFinalEandA2Kernel(double * e_xxG, double * a2_xG, int nv)
{
	extern __shared__ double sum[];
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	sum[threadIdx.x] = 0;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		sum[threadIdx.x] = e_xxG[i];
		/*printf("Debugging: tid %d commInfoDegreeG %d smemdegree %lf\n", tid, commInfoDegreeG[i], smemDegree[threadIdx.x]);*/
	}

	__syncthreads();

	reduceDoubleBlock(sum);
	if ( tid == 0 ) e_xxG[tid] = sum[0];

	sum[threadIdx.x] = 0;
	__syncthreads();

	for ( int i = tid; i < nv; i += gridsize) 
	{
		sum[threadIdx.x] = a2_xG[i];
		/*printf("Debugging: tid %d commInfoDegreeG %d smemdegree %lf\n", tid, commInfoDegreeG[i], smemDegree[threadIdx.x]);*/
	}

	__syncthreads();

	reduceDoubleBlock(sum);
	if ( tid == 0 ) a2_xG[tid] = sum[0];



}

__global__ void revertCommKernel(GraphElem * inCurrCommG, GraphElem * inPastCommG, GraphElem nv)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		inCurrCommG[i] = inPastCommG[i];
	}

}

__global__ void updateCommInfoKernel(GraphElem * inCurrCommG, GraphElem* inTargetCommG, GraphElem * commInfoSizeG, GraphElem* commInfoDegreeG, GraphElem* degreeG, GraphElem nv)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		GraphElem currComm = inCurrCommG[i];
		GraphElem targetComm = inTargetCommG[i];
		if ( targetComm >= nv )
		{
			printf("Error: tid %d targetcomm %d nv %d\n", tid, targetComm, nv );
		}
		if ( currComm != targetComm && targetComm != -1)	
		{
			atomicAdd(&commInfoSizeG[targetComm], 1);
			atomicSub(&commInfoSizeG[currComm],1);
			atomicAdd(&commInfoDegreeG[targetComm], degreeG[i]);
			atomicSub(&commInfoDegreeG[currComm], degreeG[i]);
		}
	}

}

__global__ void switchCommInfoKernel(GraphElem * inPastCommG, GraphElem * inCurrCommG, GraphElem* inTargetCommG, GraphElem nv)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
		GraphElem temp = inCurrCommG[i];
		inCurrCommG[i] = inTargetCommG[i];
		inTargetCommG[i] = inPastCommG[i];
		inPastCommG[i] = temp;
	}
}

__global__ void updateCurrCommKernel(GraphElem * inCurrCommG, GraphElem* inTargetCommG, GraphElem * inPastCommG, GraphElem nv)
{
	int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int gridsize = blockDim.x * gridDim.x;

	for ( int i = tid; i < nv; i += gridsize) 
	{
			if ( inTargetCommG[i] != - 1) {
				GraphElem temp = inCurrCommG[i];
				inCurrCommG[i] = inTargetCommG[i];
				inPastCommG[i] = temp;
			}
	}
}

//kernel functions will be added here
//
extern "C" void initGraphComm(GraphElem * inPastCommG, GraphElem * inCurrCommG, GraphElem nv)
{
	//need to add a loop here later
	initGraphCommKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(inPastCommG, inCurrCommG, nv);
	gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );

}

extern "C" void initWeightedDegree(GraphElem * commInfoSizeG, GraphElem * commInfoDegreeG, GraphElem * degreeG, GraphElem * edgeRangeG, GraphElem *
edgeEndG, GraphWeight * edgeWeightG, GraphElem nv, GraphElem ne, GraphElem *
loadPartG, int warpnum)
{ //implement a simplified version here, every thread takes one node and do the summation
	//determine the rangenum based on edgeRangeG, then every thread gets a share,
	//no need to do warp level implemenation at this moment
	cudaMemset(degreeG, 0, nv*sizeof(GraphElem));
			/*initNweightKernel<<<BLO*/
	int newBlockNum = ((warpnum << 5)/BLOCK_SIZE) + 1;
	printf("testing: newBlockNum %d\n", newBlockNum);
	initNweightKernel<<<newBlockNum, BLOCK_SIZE>>>(degreeG, edgeRangeG, edgeWeightG, ne, loadPartG, warpnum);
	gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );

	initCommInfo<<<newBlockNum, BLOCK_SIZE>>>(commInfoSizeG, commInfoDegreeG,degreeG, nv);
	gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );

}

extern "C" void initConstant2ndTerm(GraphElem * degreeG, float * const2ndTermG,
GraphElem nv, GraphElem *
loadPartG, int warpnum)
{
	cudaMemset(const2ndTermG, 0, sizeof(float));
	initConstant2ndTermKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(degreeG, const2ndTermG, nv);
	getConst2ndTermKernel<<<1,1>>>(const2ndTermG);
}

extern "C" void sortByComm(GraphElem* edgeRangeG, GraphElem * edgeEndG, GraphElem *
														inCurrCommG, GraphElem nv, GraphElem ne, GraphElem *
														loadPartG, int warpnum, GraphElem * idxG1, GraphElem * commCopyG1, GraphWeight * edgeWeightG)
{
	int* qstackG;
	int newBlockNum = ((warpnum << 5)/BLOCK_SIZE) + 1;
	cudaMalloc((void **)&qstackG, ne*sizeof(int)); //this can be further optimized  since only maxim degree of chunksize is useful
	int chunksize = ne/warpnum;
	int smsize = BLOCK_SIZE*sizeof(SortKey) + BLOCK_SIZE*sizeof(SortVal) + BLOCK_SIZE*sizeof(int) + (BLOCK_SIZE/32)*sizeof(int);
	GraphElem * idxG, *commCopyG;
	GraphWeight *edgeWeightCopyG;
	cudaMalloc((void **)&idxG, ne*sizeof(GraphElem));
	cudaMalloc((void **)&commCopyG, ne*sizeof(GraphElem));
	cudaMalloc((void **)&edgeWeightCopyG, ne*sizeof(GraphElem));

	fprintf(stderr, "qstackG size %d warpnum %d chunksize %d\n", ne*sizeof(int), warpnum, chunksize);
	//prep for idx search
	setIdxKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(idxG, ne);
	setCommCopyKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(edgeEndG, inCurrCommG, commCopyG, ne);
	localSortKernel<<<newBlockNum, BLOCK_SIZE, smsize>>>(loadPartG, edgeRangeG, idxG, commCopyG, warpnum, qstackG, chunksize);
	assignEdgeWeightCopy<<<BLOCK_NUM, BLOCK_SIZE>>>(edgeWeightCopyG, edgeWeightG, idxG, ne); // tr
	copyKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(edgeWeightG, edgeWeightCopyG, ne);
   // try to put edgeweight back

	localSortKernel<<<newBlockNum, BLOCK_SIZE, smsize>>>(loadPartG, edgeRangeG, edgeEndG, inCurrCommG, warpnum, qstackG, chunksize );

	cudaFree(qstackG); 
	cudaFree(idxG);
	cudaFree(commCopyG);
	cudaFree(edgeWeightCopyG);
}

extern "C" void calcEixAx(GraphElem * degreeG, GraphElem * inCurrCommG, GraphElem * commInfoDegreeG, GraphElem* edgeRangeG, GraphElem* edgeEndG, GraphWeight*
edgeWeightG, GraphElem *eixG,
GraphElem *axG, GraphElem nv, GraphElem ne, GraphElem *
loadPartG, int warpnum, GraphWeight * sameClusterWeightG)
{
	//will not use atomic here, will add things up so that every thread warp
	//handles one node at one time, we do not need self loop array here
	//ax can be calculated easily
	int newBlockNum = ((warpnum << 5)/BLOCK_SIZE) + 1;
	cudaMemset(sameClusterWeightG, 0, nv*sizeof(GraphWeight));
	cudaMemset(eixG, 0, nv*sizeof(GraphElem));
	/*cudaMemset(axG, 0, nv*sizeof(GraphElem));*/
	getAxKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(commInfoDegreeG, inCurrCommG, degreeG, axG, nv);
	getEixKernel<<<newBlockNum, BLOCK_SIZE>>>(loadPartG, edgeRangeG, edgeEndG, edgeWeightG, inCurrCommG, eixG, warpnum, sameClusterWeightG);
}

extern "C" void calcTarget(GraphElem* edgeRangeG, GraphElem* edgeEndG, GraphWeight * edgeWeightG, GraphElem * degreeG, GraphElem*
inCurrCommG, GraphElem* inTargetCommG, GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG, GraphElem nv,
GraphElem ne, GraphElem* eixG, GraphElem * axG, GraphElem *
loadPartG, int warpnum, double const2ndTermG)
{
    //perform check for every warp, only update max at certain conditions
		int newBlockNum = ((warpnum << 5)/BLOCK_SIZE) + 1;
		cudaMemset(inTargetCommG, -1, nv*sizeof(GraphElem));
		printf("newBlockNum: %d\n", newBlockNum);
		int smemsize =  BLOCK_SIZE*sizeof(double)*2+BLOCK_SIZE*sizeof(GraphElem)+ sizeof(GraphWeight)*BLOCK_SIZE/32 + BLOCK_SIZE/32*sizeof(GraphElem) + BLOCK_SIZE/32*sizeof(double);
		printf("smemsize %d const2ndTermG %lf sizeof(const) %d\n", smemsize, const2ndTermG, sizeof(const2ndTermG));
		double testc = (double)const2ndTermG;
    getTargetCommKernel<<<newBlockNum, BLOCK_SIZE, smemsize>>>(loadPartG, warpnum, inCurrCommG, inTargetCommG, edgeRangeG, edgeEndG, edgeWeightG, degreeG, commInfoDegreeG, commInfoSizeG, eixG, axG, testc);
}


extern "C" void	calcModularity(GraphElem* inCurrCommG, GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG, GraphWeight* sameClusterWeightG, double const2ndTermG, double* modG, double &prevMod, GraphElem *
loadPartG, int warpnum, GraphElem nv)
{
		double * e_xxG;
		double * a2_xG;
		cudaMalloc( (void **)&e_xxG, BLOCK_NUM*sizeof(double));
		cudaMalloc((void **)&a2_xG, BLOCK_NUM*sizeof(double));
		double * e_xxAndA2_x = (double *) malloc (2*sizeof(double));

		int isPowerof2 = 0;
		for ( int i = 2; i <= 1024; i*=2 )
		{
			if ( BLOCK_SIZE == i )
			{
				isPowerof2 = 1;
				break;
			}
		}
		if ( isPowerof2 != 1 ) {
			printf("Error: block size needs to be power of 2 \n");
			exit(1);
		}

		getE_xxKernel<<<BLOCK_NUM, BLOCK_SIZE, BLOCK_SIZE *sizeof(double)>>>(sameClusterWeightG, e_xxG, nv);
		getA2_xKernel<<<BLOCK_NUM, BLOCK_SIZE, BLOCK_SIZE *sizeof(double)>>>(commInfoDegreeG, a2_xG, nv);
		
		if ( BLOCK_NUM > BLOCK_SIZE )  {
			printf("block num cannot be greater than block size at this version\n");
			exit(1);
		}
		getFinalEandA2Kernel <<<1, BLOCK_SIZE, BLOCK_SIZE*sizeof(double)>>>(e_xxG, a2_xG, BLOCK_NUM);
		

		/*need another level of summation, plus do not use the same input output array*/

		cudaMemcpy(&e_xxAndA2_x[0], e_xxG, sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(&e_xxAndA2_x[1], a2_xG, sizeof(double), cudaMemcpyDeviceToHost);
		cudaFree(e_xxG);
		cudaFree(a2_xG);
		printf("e_xx %lf, a2_x %lf\n", e_xxAndA2_x[0], e_xxAndA2_x[1]);
		prevMod = ( (e_xxAndA2_x[0]*const2ndTermG - e_xxAndA2_x[1]*const2ndTermG*const2ndTermG));
}



extern "C" void revertComm(GraphElem* inCurrCommG, GraphElem* inPastCommG, GraphElem* inTargetCommG, GraphElem nv)
{
		revertCommKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(inCurrCommG, inPastCommG, nv );
}


extern "C" void updateTarget(GraphElem* inCurrCommG, GraphElem* inTargetCommG, GraphElem* inPastCommG, GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG, GraphElem* degreeG, GraphElem nv, GraphElem ne)
{
		printf("Testing: before updateCommInfoKernel BLOCK_NUM %d BLOCK_SIZe %d\n", BLOCK_NUM, BLOCK_SIZE);
		updateCommInfoKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(inCurrCommG, inTargetCommG, commInfoSizeG, commInfoDegreeG, degreeG, nv);
		/*updateCommInfoKernel<<<1,1>>>(inCurrCommG, inCurrCommG, inCurrCommG, inCurrCommG, inCurrCommG, nv);*/
		gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );

	/*switchCommInfoKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(inPastCommG, inCurrCommG, inTargetCommG, nv);*/


		printf("Testing: before updateCurrComm \n");
		updateCurrCommKernel<<<BLOCK_NUM, BLOCK_SIZE>>>(inCurrCommG, inTargetCommG, inPastCommG, nv);
		gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );


}


