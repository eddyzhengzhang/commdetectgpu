#include <cctype>
#include <cmath>
#include <cstring>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "loadMatrixMarket.h"
#include "loadUtils.h"

void loadMatrixMarketFile(Graph *&g, const std::string &fileName)
{
  std::ifstream ifs;

  ifs.open(fileName.c_str());
  if (!ifs) {
    std::cerr << "Error opening Matrix Market format file: " << fileName << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  bool isComplex = false, isPattern = false, isSymmetric = false, isGeneral = false;

  std::getline(ifs, line);
  {
    std::string ls[5];

    std::istringstream iss(line);
    iss >> ls[0] >> ls[1] >> ls[2] >> ls[3] >> ls[4];

    if (!iss) {
      std::cerr << "Error parsing matrix market file: " << fileName << std::endl;
      exit(EXIT_FAILURE);
    }

    std::cout << "Processing: " << ls[0] << " " << ls[1] << " " << ls[2] << " " << ls[3] <<
      " " << ls[4] << std::endl;
    for (int i = 1; i < 5; i++)
      std::transform(ls[i].begin(), ls[i].end(), ls[i].begin(), toupper);

    if (ls[0] != "%%MatrixMarket") {
      std::cerr << "Error in the first line of: " << fileName << " " << ls[0] <<
	", expected: %%MatrixMarket" << std::endl;
      exit(EXIT_FAILURE);
    }

    if (ls[1] != "MATRIX") {
      std::cerr << "The object type should be matrix for file: " << fileName << std::endl;
      exit(EXIT_FAILURE);
    }

    if (ls[2] != "COORDINATE") {
      std::cerr << "The object type should be coordinate for file: " << fileName << std::endl;
      exit(EXIT_FAILURE);
    }

    if (ls[3] == "COMPLEX") {
      std::cout << "Warning will only process the real part" << std::endl;
      isComplex = true;
    }

    if (ls[3] == "PATTERN") {
      std::cout << "Note: matrix type is pattern.  All weights will be set to 1.0" <<
	std::endl;
      isPattern = true;
    }

    if (ls[4] == "GENERAL")
      isGeneral = true;
    else if (ls[4] == "SYMMETRIC") {
      std::cout << "Note: matrix type is symmetric" << std::endl;
      isSymmetric = true;
    }
  }

  if (!isSymmetric) {
    std::cerr << "Error: matrix market type should be symmetric for file: " << fileName <<
      std::endl;
    exit(EXIT_FAILURE);
  }

  do {
    std::getline(ifs, line);
  } while (line[0] == '%');

  long int numVertices, numEdges;
  {
    long int ns, nt, ne;

    std::istringstream iss(line);

    iss >> ns >> nt >> ne;
    if (!iss || iss.fail()) {
      std::cerr << "Error parsing Matrix Market format: " << line << std::endl;
      exit(EXIT_FAILURE);
    }

    numVertices = ns;
    numEdges = ne;
  }

  std::cout << "Loading Matrix Market file: " << fileName << ", numvertices: " << numVertices <<
    ", numEdges: " << numEdges << std::endl;

  std::vector<GraphElem> edgeCount(numVertices + 1L);
  std::vector<GraphElemPair> edgeList(numEdges * 2L);
  std::vector<double> edgeWeight(numEdges * 2L);

  long int nnz = 0;

  for (long int i = 0; i < numEdges; i++) {
    long int source, dest;
    double weight = 1.0;

    if (isPattern)
      ifs >> source >> dest;
    else
      ifs >> source >> dest >> weight;

    if (!ifs || ifs.fail()) {
      std::cerr << "Error parsing Matrix Market edge" << std::endl;
      exit(EXIT_FAILURE);
    }

    source--; // 1-based indexing
    dest--;

    assert((source >= 0L) && (source < numVertices));
    assert((dest >= 0L) && (dest < numVertices));

    if ((i % 1000000L) == 0L)
      std::cout << "Reading edge: (" << source << ", " << dest << ") with weight: " <<
	weight << std::endl;

    if (source != dest) {
      GraphElemPair &fedge = edgeList[nnz];
      GraphElemPair &bedge = edgeList[nnz + 1];
      double &fweight = edgeWeight[nnz];
      double &bweight = edgeWeight[nnz + 1];

      weight = fabs(weight);

      fedge.first = source;
      fedge.second = dest;
      fweight = weight;

      bedge.first = dest;
      bedge.second = source;
      bweight = weight;

      edgeCount[fedge.first + 1L]++;
      edgeCount[bedge.first + 1L]++;
      nnz += 2L;
    }
  }
  ifs.close();

  std::cout << "New number of edges: " << nnz << ", old: " << numEdges << std::endl;
  // assert((numEdges * 2L) == nnz);

  numEdges = nnz;
  g = new Graph(numVertices, numEdges);
  edgeList.resize(numEdges);
  edgeWeight.resize(numEdges);

  processGraphData(*g, edgeCount, edgeList, numVertices, numEdges, &edgeWeight);
} // loadMatrixMarketFile
