#include "localSort_kernel.cu"

//Local sort function that calls the local sort kernel
//
void localSort(SortRange * R_CPU, SortRange * R, SortList* L_CPU, SortList * L, SortVal * A, SortKey * K, int num, int rangenum, int &warp_num, int &block_num, int block_size, int * qstack)
{
	// looks like L already contains the node degree information, we should divide
	// L by block_num
	int part_num = (L_CPU[rangenum])%warp_num ? (float)L_CPU[rangenum]/warp_num+1:(float)L_CPU[rangenum]/warp_num;
	// printf("DEBUG:L_CPU[num]:%d,  part_num %d\n",L_CPU[rangenum], part_num);
	int max_num = 0;
	// find maximum degree
	for ( int i = 1; i <= rangenum; i++ )
	{
		if ( (L_CPU[i] - L_CPU[i-1]) > max_num )
			max_num = L_CPU[i] - L_CPU[i-1];
	}
	// printf("DEBUG: max_num is %d\n", max_num); 
	if ( part_num < max_num )
	{
		part_num = max_num;
		/*warp_num = (L[num-1] -1)%part_num ? (L[num-1]-1)/part_num:(L[num-1]-1)/part_num + 1;*/
		/*//need to update block_num as well*/
		/*//tocontinue here*/
	} 
	warp_num = 0;
	int pcounter = 0;
	for ( int i = 0; i < rangenum; i++)
	{
		if (L_CPU[i] >= pcounter*part_num)
		{
			R_CPU[pcounter] = i;
			pcounter++;
			warp_num++;
		}
	}
	//printf("DEBUG: warp range determined\n");

	if (R_CPU[pcounter] != rangenum) R_CPU[pcounter] = rangenum;

	/*printf("DEBUG: ");*/
	/*for ( int i = 0; i <= warp_num; i++ )*/
	/*printf("%d ", R_CPU[i]);*/
	/*printf("\n");*/

	block_num = (warp_num*32)%block_size ? (warp_num*32)/block_size+1:(warp_num*32)/block_size;

	/*printf("DEBUG: before start local sort, pcounter: %d, block_num: %d, block_size: %d, warp_num: %d, max_num: %d\n", pcounter, block_num, block_size, warp_num, max_num);*/
	cudaMemcpy(R,R_CPU,(pcounter+1)*sizeof(SortRange),cudaMemcpyHostToDevice);
	
	printf("Debug: block_num %d, block_size %d, pcounter %d \n", block_num, block_size, pcounter);

	localSortKernel<<<block_num, block_size, (1*block_size*sizeof(SortKey) + 1*block_size*sizeof(SortVal) + (block_size/32)*sizeof(int) + block_size*sizeof(int))>>>(R,L,A,K,warp_num, qstack, max_num); //caveat, block_size needs to be multiply of 32
	gpuErrchk( cudaPeekAtLastError() );
	gpuErrchk( cudaDeviceSynchronize() );
}

//partition of an array before further splitting into two quick sort calls
int quickSortPartCPU(int beg, int end, SortVal * A_CPU, SortKey * K_CPU)
{
	int p = beg;
	SortKey pkey = K_CPU[A_CPU[p]];
	SortVal tempval;
	int low = beg-1;
	int high = end;
	while ( true )
	{
		do 
		{
			high--;
		} while ( (high > beg) && (K_CPU[A_CPU[high]] >= pkey) );
		
		do
		{
			low++;
		} while ( (low < end-1) && (K_CPU[A_CPU[low]] < pkey) );

		if ( low < high )
		{//swap
			tempval = A_CPU[low];
			A_CPU[low] = A_CPU[high];
			A_CPU[high] = tempval;	
		}
		else
		{
			return high;
		}
	}
}

//recursive function to do quick sort for a range of values
void quickSortCPU(int start, int end, SortVal * A_CPU, SortKey * K_CPU)
{
	if ( start < (end-1))
	{
		int p = quickSortPartCPU(start, end, A_CPU, K_CPU);
		quickSortCPU(start, p+1, A_CPU, K_CPU);
		quickSortCPU(p+1, end, A_CPU, K_CPU);
	}

}

//local sort CPU
//performing local sort on CPU as a test function for GPU local sort
void localSortCPU(SortList * L_CPU, SortVal * A_CPU, SortKey * K_CPU, int arraysize, int rangenum)
{
	for ( int i = 0; i < rangenum; i++ )
	{
		quickSortCPU(L_CPU[i], L_CPU[i+1], A_CPU, K_CPU);
	}

}

//Testing function for local sort
//
int main( )
{
	/*printf("Empty test function for now... \n");*/
	int arraysize = 6400*512; //640000000; //512000000; //maximum number tried 640 million
	int rangenum = 512; //3200;//200 ;	
	int warpnum = 512; //320;//100;
	int wrangenum = warpnum;

	SortList * L_CPU = (SortList *) malloc ( (rangenum+1)*sizeof(SortList));
	SortVal * A_CPU = (SortVal *) malloc( arraysize*sizeof(SortVal));
	SortVal * A_CPU_test = (SortVal *) malloc( arraysize*sizeof(SortVal));
	SortKey * K_CPU = (SortKey *) malloc( arraysize*sizeof(SortKey));
	SortRange * R_CPU = (SortRange *) malloc ( (wrangenum+1)*sizeof(SortRange));

	SortList *L, *A, *K, *R;
	int * qstack;
	cudaMalloc(&L, (rangenum+1)*sizeof(SortList));
	cudaMalloc(&A, arraysize*sizeof(SortVal));
	cudaMalloc(&K, arraysize*sizeof(SortKey));
	cudaMalloc(&R, (wrangenum+1)*sizeof(SortRange));
	cudaMalloc(&qstack, arraysize*sizeof(int));

	for ( int i = 0; i < arraysize; i++ )
	{
		A_CPU[i] = i;
		A_CPU_test[i] = A_CPU[i];
		K_CPU[i] = i*3 + (i/2);//  (rand()); //4 -i%4; // rand(); // i%4;// i%4;
	}

	for ( int i = 0; i < rangenum; i++)
	{
		L_CPU[i]=i*(arraysize/rangenum);
	}
	
	/*printf("L: ");*/
	L_CPU[rangenum] = arraysize;
	/*for ( int i = 0; i <= rangenum; i++ )*/
	/*printf("%d ", L_CPU[i]);*/
	/*printf("\n");*/


	for ( int i = 0; i < wrangenum; i++ )
	{
		R_CPU[i] = i*(rangenum/wrangenum);
	}
	R_CPU[wrangenum] = rangenum;
	
	/*printf("R: ");*/
	/*for ( int i = 0; i <= wrangenum; i++ )*/
	/*printf("%d ", R_CPU[i]);*/
	/*printf("\n");*/

	cudaMemcpy(L, L_CPU, (rangenum+1)*sizeof(SortList), cudaMemcpyHostToDevice);
	cudaMemcpy(A, A_CPU, arraysize*sizeof(SortVal), cudaMemcpyHostToDevice);
	cudaMemcpy(K, K_CPU, arraysize*sizeof(SortKey), cudaMemcpyHostToDevice);
	cudaMemcpy(R, R_CPU, (wrangenum+1)*sizeof(SortRange), cudaMemcpyHostToDevice);
	

	int blocksize = 256;
	int blocknum = warpnum*32/256 + 1;

	/*printf("DEBUG: before local sort\n");*/

	struct timeval start, end;

	gettimeofday(&start, NULL);
	localSort(R_CPU, R, L_CPU, L, A, K, arraysize, rangenum, warpnum, blocknum, blocksize, qstack);
	cudaThreadSynchronize();
	gettimeofday(&end, NULL);

	printf("GPU time %ld\n", gettime(start,end));

	cudaMemcpy(A_CPU, A, arraysize*sizeof(SortVal), cudaMemcpyDeviceToHost);
	
	/*printf("DEBUG: after local sort\n");*/

	//testing code starting from here
	//
	int testflag = 1;
	for ( int i = 0; i < rangenum; i++ )
	{
		testflag = 1;
		for ( int j = L_CPU[i]; j < L_CPU[i+1]-1; j++ )
			/*for ( int j = L_CPU[i]; j < L_CPU[i+1]; j++ )*/
		{
			/*if ( i == 256 )*/
			/*printf("Debugo: A_CPU[%d]: %d, K_CPU[A_CPU[%d]]: %d\n", j, A_CPU[j], A_CPU[j], K_CPU[A_CPU[j]]);*/
			/*printf("%d %d %d\t%d\t",j,j+1, A_CPU[j], A_CPU[j+1]);*/
			/*printf("%d\t", A_CPU[j]);*/
			if ( K_CPU[A_CPU[j]] > K_CPU[A_CPU[j+1]] )
			{
				testflag = 0;
				/*break;*/
			}
		}
		if ( testflag ==0)
		{
			for ( int j = L_CPU[i]; j < L_CPU[i+1]; j++ )
				printf("Debugo: unsort i %d A_CPU[%d]: %d, K_CPU[%d]:%d, A_CPU[%d]: %d, K_CPU[%d]: %d\n", i, j, A_CPU[j], A_CPU[j], K_CPU[A_CPU[j]], j+1, A_CPU[j+1],A_CPU[j+1] , K_CPU[A_CPU[j+1]]);
		}
	}

	/*printf("\n");*/

	printf("GPU testing result(1 means pass, 0 means fail): %d\n", testflag);

	printf("CPU testing results: \n");

	gettimeofday(&start, NULL);
	localSortCPU(L_CPU, A_CPU_test, K_CPU, arraysize, rangenum);
	gettimeofday(&end, NULL);
	printf("CPU time %ld\n", gettime(start,end));

	//testing code starting from here
	//
	testflag = 1;
	for ( int i = 0; i < rangenum; i++ )
	{
		for ( int j = L_CPU[i]; j < L_CPU[i+1]-1; j++ )
		{
			if ( K_CPU[A_CPU_test[j]] > K_CPU[A_CPU_test[j+1]] )
			{
				testflag = 0;
				break;
			}
		}
	}

	printf("CPU testing result(1 means pass, 0 means fail): %d\n", testflag);

	//test if cpu gpu results are matching
	for ( int i = 0; i < rangenum; i++ )
	{
		for ( int j = L_CPU[i]; j < L_CPU[i+1]; j++ )
		{
			if ( K_CPU[A_CPU_test[j]] != K_CPU[A_CPU[j]] )
			{
				testflag = 0;
				break;
			}
		}
	}
	printf("CPU GPU results matching test (1 means pass, 0 means fail): %d\n", testflag);
}
