#ifndef __BUILD_NEXT_PHASE_H
#define __BUILD_NEXT_PHASE_H

#include "louvainMethod.h"

GraphElem renumberClustersContiguously(CommunityVector &cvect);
void buildNextLevelGraph(const Graph &gin, Graph *&gout, const CommunityVector &cvect,
			 const GraphElem numUniqueClusters);

#endif // __BUILD_NEXT_PHASE_H
