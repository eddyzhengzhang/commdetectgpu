#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <string>

#include "graph.h"
#include "loadDimacs.h"
#include "loadMatrixMarket.h"
#include "loadMetis.h"
#include "loadSimple.h"
#include "timer.h"

static std::string inputFileName, outputFileName;

static bool matrixMarketFormat = false;
static bool dimacsFormat = false;
static bool pajekOnceFormat = false;
static bool pajekTwiceFormat = false;
static bool metisFormat = false;
static bool simpleFormat = false;
static bool coloring = false;
static bool output = false;
static bool vertexFollowing = false;

static void parseCommandLine(const int argc, char * const argv[]);

int main(int argc, char *argv[])
{
  parseCommandLine(argc, argv);

  // Only the following formats supported for now
  assert(dimacsFormat || metisFormat || simpleFormat || matrixMarketFormat);

  Graph *g = NULL;

  rusage rus;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  if (dimacsFormat)
    loadDimacsFile(g, inputFileName);
  else if (metisFormat)
    loadMetisFile(g, inputFileName);
  else if (simpleFormat)
    loadSimpleFile(g, inputFileName);
  else if (matrixMarketFormat)
    loadMatrixMarketFile(g, inputFileName);
  else {
    std::cerr << "Format not specified correctly!" << std::endl;
    if (g)
      delete g;
    return 1;
  }

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Loaded graph from file: " << inputFileName << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  assert(g);

  double t0, t1;

  t0 = mytimer();
  std::ofstream ofs(outputFileName.c_str(), std::ofstream::out | std::ofstream::binary |
		    std::ofstream::trunc);
  if (!ofs) {
    std::cerr << "Error opening output file: " << outputFileName << std::endl;
    exit(EXIT_FAILURE);
  }

  long int nv, ne;
  int header;

  nv = g->getNumVertices();
  ne = g->getNumEdges();

  if ((nv < INT32_MAX) && (ne < INT32_MAX))
    header = 32;
  else
    header = 64;

  ofs.write(reinterpret_cast<char *>(&header), sizeof(header));

  int nvi, nei;

  if (header == 32) {
    nvi = nv;
    nei = ne;

    ofs.write(reinterpret_cast<char *>(&nvi), sizeof(nvi));
    ofs.write(reinterpret_cast<char *>(&nei), sizeof(nei));
  }
  else {
    ofs.write(reinterpret_cast<char *>(&nv), sizeof(nv));
    ofs.write(reinterpret_cast<char *>(&ne), sizeof(ne));
  }

  for (long int v = 0L; v < nv; v++) {
    GraphElem e0, e1;

    g->getEdgeRangeForVertex(v, e0, e1);

    for (GraphElem j = e0; j < e1; j++) {
      const Edge &edge = g->getEdge(j);
      int vi, ti;

      if (header == 32) {
	vi = v;
	ti = edge.tail;

	ofs.write(reinterpret_cast<char *>(&vi), sizeof(vi));
	ofs.write(reinterpret_cast<char *>(&ti), sizeof(ti));
      }
      else {
	ofs.write(reinterpret_cast<char *>(&v), sizeof(v));
	ofs.write(reinterpret_cast<const char *>(&edge.tail), sizeof(edge.tail));
      }
    }
  }

  ofs.close();
  delete g;

  t1 = mytimer();

  std::cout << "Time writing binary file: " << (t1 - t0) << std::endl;

  return 0;
} // main

void parseCommandLine(const int argc, char * const argv[])
{
  int ret;

  while ((ret = getopt(argc, argv, "f:o:mdptes")) != -1) {
    switch (ret) {
    case 'f':
      inputFileName.assign(optarg);
      break;
    case 'o':
      outputFileName.assign(optarg);
      break;
    case 'm':
      matrixMarketFormat = true;
      break;
    case 'd':
      dimacsFormat = true;
      break;
    case 'p':
      pajekOnceFormat = true;
      break;
    case 't':
      pajekTwiceFormat = true;
      break;
    case 'e':
      metisFormat = true;
      break;
    case 's':
      simpleFormat = true;
      break;
    default:
      assert(0 && "Should not reach here!!");
      break;
    }
  }

  if ((matrixMarketFormat || dimacsFormat || pajekOnceFormat || pajekTwiceFormat ||
       metisFormat || simpleFormat) == false) {
    std::cerr << "Must select a file format for the input file!" << std::endl;
    exit(EXIT_FAILURE);
  }
  const bool fileFormat[] = { matrixMarketFormat, dimacsFormat, pajekOnceFormat,
			      pajekTwiceFormat, metisFormat, simpleFormat };
  const int numFormats = sizeof(fileFormat) / sizeof(fileFormat[0]);

  int numTrue = 0;
  for (int i = 0; i < numFormats; i++)
    if (numTrue == 0 && fileFormat[i])
      numTrue++;
    else if (numTrue > 0 && fileFormat[i]) {
      std::cerr << "Must select a SINGLE file format for the input file!" << std::endl;
      exit(EXIT_FAILURE);
    }

  if (inputFileName.empty()) {
    std::cerr << "Must specify an input file name with -f" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (outputFileName.empty()) {
    std::cerr << "Must specify an output file name with -o" << std::endl;
    exit(EXIT_FAILURE);
  }
} // parseCommandLine
