#ifndef __LOUVAIN_METHOD_H
#define __LOUVAIN_METHOD_H

#include <map>
#include <unordered_map>
#include <vector>

#include "distanceOneVertexColoring.h"
#include "graph.h"

struct KeyValueComm {
  GraphElem commId;
  GraphElem cnt;

  bool operator == (const KeyValueComm &k0) const
  {
    return (this->commId == k0.commId);
  } //operator ==
};

typedef std::vector<GraphElem> CommunityVector;
typedef std::vector<GraphElem> GraphElemVector;
typedef GraphElem* CommunityArray;
typedef GraphElem* GraphElemArray;
// typedef std::unordered_map<GraphElem, GraphElem> ClusterLocalMap; this one always commented out
typedef std::map<GraphElem, GraphElem> ClusterLocalMap;
typedef std::vector<KeyValueComm> ClusterLocalMapV;
typedef std::unordered_map<ColorElem, ColorElem> ColorReorderMap;

double louvainWithDistOneColoring(const Graph &g, CommunityVector &cvect, 
				  ColorVector &colors, const ColorElem numColors,
				  const double lower, const double thresh);
double louvainMethod(const Graph &g, CommunityVector &cvect, const double lower);

#endif //  __LOUVAIN_METHOD_H
