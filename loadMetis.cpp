#include <fstream>
#include <iostream>
#include <sstream>

#include "loadMetis.h"

void loadMetisFile(Graph *&g, const std::string &fileName)
{
  std::ifstream ifs;

  ifs.open(fileName.c_str());
  if (!ifs) {
    std::cerr << "Error opening Metis format file: " << fileName << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  char comment;

  do {
    std::getline(ifs, line);
    comment = line[0];
  } while (comment == '%');

  long int numVertices, numEdges, value = 0L;
  {
    std::istringstream iss(line);

    iss >> numVertices >> numEdges;
    if (!iss.eof())
      iss >> value;
  }

  numEdges *= 2; // Metis' edges are not directional

  std::cout << "Loading Metis file: " << fileName << ", numvertices: " << numVertices <<
    ", numEdges: " << numEdges << std::endl;

  g = new Graph(numVertices, numEdges);

  long int edgePos = 0L;

  g->setEdgeStartForVertex(0L, 0L);

  switch (value) {
  case 10:
    std::cout << "Metis format vertex weights ignored" << std::endl;
  case 0: // No weights in Metis file
    for (long int i = 0; i < numVertices; i++) {
      long int j = 0L, neighbor;

      std::getline(ifs, line);
      std::istringstream iss(line);

      while (!iss.eof()) {
	iss >> neighbor;
	if (!iss || iss.eof())
	  break;
	j++;
	Edge &edge = g->getEdge(edgePos);
	edge.tail = neighbor - 1;
	edge.weight = 1.0;
	edgePos++;
      }
      g->setEdgeStartForVertex(i + 1L, edgePos);
    }
    break;
  case 11:
    std::cout << "Metis format vertex weights ignored" << std::endl;
  case 1:
    for (long int i = 0; i < numVertices; i++) {
      long int j = 0L, neighbor;
      double weight;

      std::getline(ifs, line);
      std::istringstream iss(line);

      while (!iss.eof()) {
	iss >> neighbor >> weight;
	if (!iss || iss.eof())
	  break;
	j++;
	Edge &edge = g->getEdge(edgePos);
	edge.tail = neighbor - 1;
	edge.weight = weight;
	edgePos++;
      }

      g->setEdgeStartForVertex(i + 1L, edgePos);
    }
    break;
  default:
    std::cerr << "Inconsistent value for weight flag in Metis format: " << value << std::endl;
    exit(EXIT_FAILURE);
  }

  ifs.close();
} // loadMetisFile
