//here we will have CUDA kernel files
//Author: Eddy Z. Zhang
extern "C" void initGraphComm(GraphElem * inPastCommG, GraphElem * inCurrCommG, GraphElem nv);

extern "C" void initWeightedDegree(GraphElem * commInfoSizeG, GraphElem * commInfoDegreeG, GraphElem * degreeG, GraphElem * edgeRangeG, GraphElem *
edgeEndG, GraphWeight * edgeWeightG, GraphElem nv, GraphElem ne, GraphElem *
loadPartG, int warpnum);

extern "C" void initConstant2ndTerm(GraphElem * degreeG, float * const2ndTermG,
GraphElem nv,GraphElem * loadPartG, int warpnum );

extern "C" void sortByComm(GraphElem* edgeRangeG, GraphElem * edgeEndG, GraphElem *
inCurrComm, GraphElem nv, GraphElem ne, GraphElem *
loadPartG, int warpnum, GraphElem * idxG, GraphElem * sortCommG, GraphWeight * edgeWeightCopyG);

extern "C" void calcEixAx(GraphElem * degreeG, GraphElem * inCurrCommG, GraphElem * commInfoDegreeG, GraphElem* edgeRangeG, GraphElem* edgeEndG, GraphWeight*
edgeWeightG, GraphElem *eixG,
GraphElem *axG, GraphElem nv, GraphElem ne, GraphElem *
loadPartG, int warpnum, GraphWeight * sameClusterWeightG);

extern "C" void calcTarget(GraphElem* edgeRangeG, GraphElem* edgeEndG, GraphWeight * edgeWeightG, GraphElem * degreeG, GraphElem*
inCurrCommG, GraphElem* inTargetCommG, GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG, GraphElem nv,
GraphElem ne, GraphElem* eixG, GraphElem * axG, GraphElem *
loadPartG, int warpnum, double const2ndTermG);


extern "C" void calcModularity(GraphElem* inCurrCommG, GraphElem* commInfoSizeG,
GraphElem* commInfoDegreeG, GraphWeight* sameClusterWeightG, double
const2ndTermG, double* modG, double &prevMod, GraphElem *
loadPartG, int warpnum, GraphElem nv);

extern "C" void revertComm(GraphElem* inCurrCommG, GraphElem* inPastCommG, GraphElem* inTargetCommG, GraphElem nv);


extern "C" void updateTarget(GraphElem* inCurrCommG, GraphElem* inTargetCommG, GraphElem* inPastCommG, GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG, GraphElem * degreeG, GraphElem nv, GraphElem ne);


