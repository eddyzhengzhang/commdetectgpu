#include <algorithm>
#include <iostream>
#include <numeric>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <memory.h>

#include <stdio.h>

#include "omp.h"
#include "timer.h"

#include "gpuParam.h"

#include "louvainMethodC.h"
#include "louvainMethod_kernels.cuh"
#include "louvainMethod_kernels_e.cuh"

typedef std::vector<double> DoubleVector;
typedef double* DoubleArray;

static ClusterLocalMapA clmapv = NULL;
static size_t clsz = 0;
#pragma omp threadprivate(clmapv, clsz)

static void sumVertexDegree(const GraphStruct &g, GraphElemArray &vdegree, CommArray &cinfo);
static double calcConstantForSecondTerm(const GraphElemArray &vdegree);
static void initComm(CommunityArray &pastComm, CommunityArray &currComm, size_t size);
static GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
        ClusterLocalMapA &clmap, DoubleArray &counter,
        const GraphStruct &g, CommunityArray &currComm,
        const GraphElem vertex);
static GraphElem getMaxIndex(const ClusterLocalMapA &clmap, const DoubleArray &counter,
        const GraphElem selfLoop, const CommArray &cinfo,
        const GraphElem vdegree, const GraphElem currComm,
        const double constant, const GraphElem e0, const GraphElem e1,
        const GraphElem vertex);
static void initLouvain(const GraphStruct &g, const CommunityArray &cvect,
        CommunityArray &pastComm, CommunityArray &currComm,
        GraphElemArray &vdegree, GraphElemArray &clusterWeight,
        CommArray &cinfo, CommArray &cupdate,
        double &constantForSecondTerm);
static void executeLouvainIteration(const GraphElem i, const GraphStruct &g,
        CommunityArray &currComm,
        const GraphElemArray &vdegree, const CommArray &cinfo, 
        CommArray &cupdate, const double constantForSecondTerm,
        GraphElemArray &clusterWeight,
        const bool updateWeight = false,
        CommunityArray *const targetComm = NULL);
static void updatecinfo(CommArray &cinfo, const CommArray &cupdate, size_t csv);
static double computeModularity(const GraphStruct &g, CommArray &cinfo,
        GraphElemArray &clusterWeight,
        const CommunityArray &currComm,
        const double constantForSecondTerm,
        const bool useColoring = true);
static void cleanCWandCU(const GraphElem nv, GraphElemArray &clusterWeight,
        CommArray &cupdate);

//needs to be malloc prv
void GraphInit(GraphStruct *&g, const GraphElem onv, const GraphElem one){
    g->numVertices = onv;
    g->numEdges = one;
    g->edgeListIndexes = (EdgeIndexesArray) malloc(sizeof(GraphElem)*g->numVertices+1);
    g->edgeList = (Edge*)malloc(sizeof(Edge)*g->numEdges);
    for(int i = 0; i < g->numVertices+1; i++){
        g->edgeListIndexes[i] = 0;
    }
}
void getEdgeRangeForVertex(const GraphStruct *const &g, const GraphElem vertex, GraphElem &e0, GraphElem &e1) 
{
  e0 = g->edgeListIndexes[vertex];
  e1 = g->edgeListIndexes[vertex + 1];
}


/*
   double louvainWithDistOneColoring(const Graph &g, CommunityVector &cvect,
   ColorVector &colors, const ColorElem numColors,
   const double lower, const double thresh)
   {
   CommunityVector pastComm, currComm;
   GraphElemVector vdegree;
   GraphElemVector clusterWeight;
   CommVector cinfo, cupdate;

   const GraphElem nv = g.getNumVertices();
   const GraphElem ne = g.getNumEdges();
   const double threshMod = thresh;
   const GraphElem nodeCountCutoff = ceil(static_cast<double>(nv) * 0.20); // Heuristic 20%
//const int nthreads = omp_get_max_threads();
const int nthreads = 1;

double constantForSecondTerm;
double prevMod = lower;
double currMod = -1.0;
int numIters = 0;
bool recolorFlag = false;

initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
constantForSecondTerm);
// clmapv.resize(ne + nv);

ColorVector colorPtr, colorIndex, colorAdded, nodeCnt;

recolor:
colorPtr.resize(numColors + 1L);
nodeCnt.resize(numColors);
if (recolorFlag) {
colorIndex.resize(nv);
colorAdded.resize(numColors);
}

#pragma omp parallel for default(none), shared(colors, colorPtr), schedule(guided)
for (GraphElem i = 0L; i < nv; i++)
#pragma omp atomic update
colorPtr[colors[i] + 1]++;

{
ColorVector colorPtrPSum(numColors + 1L);

std::partial_sum(colorPtr.begin(), colorPtr.end(), colorPtrPSum.begin());
colorPtr = colorPtrPSum;
}

int posb = -1;

for (ColorElem i = 0L; i < numColors; i++) {
const int cnt = (colorPtr[i + 1] - colorPtr[i]);

nodeCnt[i] = cnt;
}
if (!recolorFlag) {
ColorVector reorderColors(numColors);

for (ColorElem i = 0L; i < numColors; i++)
reorderColors[i] = i;

std::sort(reorderColors.begin(), reorderColors.end(),
[&nodeCnt] (const int c0, const int c1)
{ return (nodeCnt[c0] > nodeCnt[c1]); });

ColorReorderMap remap;
int reord = 0;

for (ColorElem i = 0L; i < numColors; i++) {
remap.insert(std::make_pair(reorderColors[i], i));
if (reorderColors[i] != i)
    reord++;
    }

#pragma omp parallel for default(none), shared(remap, colors), schedule(static)
for (GraphElem i = 0L; i < nv; i++) {
    ColorReorderMap::const_iterator iter = remap.find(colors[i]);

    colors[i] = iter->second;
}

colorPtr.clear();
nodeCnt.clear();
recolorFlag = true;

std::cout << "Number of reordered colors: " << reord << std::endl;
goto recolor;
}
else {
    GraphElem curCnt = 0L;
    GraphElem i = 0L;

    while (curCnt < nodeCountCutoff) {
        curCnt += nodeCnt[i];
        posb = i;
        i++;
    }
}

#pragma omp parallel for default(none), shared(colors, colorPtr, colorAdded, colorIndex), schedule(guided)
for (GraphElem i = 0L; i < nv; i++) {
    const int color = colors[i];
    GraphElem where;

#pragma omp atomic capture
    where = colorAdded[color]++;

    where += colorPtr[color];
    colorIndex[where] = i;
}

std::cout << "End position for large colors: " << posb << ", node count: " <<
((posb >= 0) ? nodeCnt[posb] : -1) << ", largest: " << nodeCnt[0] << std::endl;
std::cout << "Node count cutoff: " << nodeCountCutoff << std::endl;

do {
    numIters++;

#pragma omp parallel default(none), shared(g, clusterWeight, cupdate, colorPtr, colorIndex, currComm, vdegree, cinfo), firstprivate(constantForSecondTerm, posb, numIters)
    {
        if (!clmapv)
            clmapv = new ClusterLocalMapV;

        const int tid = omp_get_thread_num();

        for (GraphElem ci = 0L; ci <= posb; ci++) {
            cleanCWandCU(nv, clusterWeight, cupdate);

            GraphElem coloradj1 = colorPtr[ci];
            GraphElem coloradj2 = colorPtr[ci + 1];

#pragma omp for schedule(guided)
            for (GraphElem k = coloradj1; k < coloradj2; k++) {
                const GraphElem i = colorIndex[k];

                executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
                        clusterWeight);
            }
            updatecinfo(cinfo, cupdate);
        }

#pragma omp barrier

        cleanCWandCU(nv, clusterWeight, cupdate);
#pragma omp for schedule(guided)
        for (GraphElem k = colorPtr[posb + 1]; k < colorPtr[numColors]; k++) {
            const GraphElem i = colorIndex[k];

            executeLouvainIteration(i, g, currComm, vdegree, cinfo, cupdate, constantForSecondTerm,
                    clusterWeight);
        }
        updatecinfo(cinfo, cupdate);
    }

    currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm);
    if ((currMod - prevMod) < threshMod)
        break;

    prevMod = currMod;
} while(true);

cvect = currComm;

return prevMod;
} // louvainWithDistOneColoring
*/


void initLouvainGPU(const GraphStruct &g, GraphElem* &edgeEndC, GraphWeight* &edgeWeightC, GraphElem* &edgeEndG, GraphWeight* &edgeWeightG, GraphElem* &edgeRangeG, GraphElem * &degreeG
										, GraphElem* &commInfoSizeG, GraphElem* &commInfoDegreeG
 										, GraphElem * &inPastCommG, GraphElem * &inCurrCommG, GraphElem * &inTargetCommG, GraphWeight* &sameClusterWeightG, GraphWeight * &selfLoopG, double &const2ndTermG, GraphElem *
loadPartG, int warpnum)
{
		const int nv = g.numVertices;
		const int ne = g.numEdges;

		edgeEndC = (GraphElem *) malloc( ne*sizeof(GraphElem) );
		edgeWeightC = (GraphWeight *) malloc (ne*sizeof(GraphElem));

			

		cudaMalloc((void**)&edgeEndG, ne*sizeof(GraphElem));
		cudaMalloc((void**)&edgeWeightG, ne*sizeof(GraphWeight));
		cudaMalloc((void**)&degreeG, nv*sizeof(GraphWeight));
		cudaMalloc((void**)&commInfoSizeG, nv*sizeof(GraphElem));
		cudaMalloc((void**)&commInfoDegreeG, nv*sizeof(GraphElem));
		cudaMalloc((void**)&inPastCommG, nv*sizeof(GraphElem));
		cudaMalloc((void**)&inCurrCommG, nv*sizeof(GraphElem));
		cudaMalloc((void**)&inTargetCommG, nv*sizeof(GraphElem));
		cudaMalloc((void**)&sameClusterWeightG, nv*sizeof(GraphWeight));
		cudaMalloc((void**)&selfLoopG, nv*sizeof(GraphWeight));
		cudaMalloc((void**)&edgeRangeG, (nv+1)*sizeof(GraphElem));
		cudaMalloc((void**)&const2ndTermG, sizeof(double));
		float* cst2G;
		cudaMalloc((void**)&cst2G, sizeof(double));

		printf("Testing: before copy to edgeRangeG, size %d\n", nv + 1);

		cudaMemcpy(edgeRangeG, g.edgeListIndexes, (nv+1)*sizeof(GraphElem), cudaMemcpyHostToDevice);



		for ( int i = 0; i < nv; i++ )
		for(int j = g.edgeListIndexes[i]; j < g.edgeListIndexes[i+1]; j++ )
		{
			const Edge &edge = g.edgeList[j];
			edgeEndC[j] = edge.tail;
			edgeWeightC[j] = edge.weight;
			printf("Testing: node %d edge %d pointo %d weight %d\n", i, j, edgeEndC[j], (int)edgeWeightC[j] );
		}

		cudaMemcpy(edgeEndG, edgeEndC, ne*sizeof(GraphElem), cudaMemcpyHostToDevice);
		cudaMemcpy(edgeWeightG, edgeWeightC, ne*sizeof(GraphElem), cudaMemcpyHostToDevice);




		printf("Testing: before initGraphComm \n");
		//
		initGraphComm(inPastCommG, inCurrCommG, nv);//initialize community for every node or carry degree from cvect?
		printf("Testing: before initWeightedDegree \n");

		

	int newBlockNum = ((warpnum << 5)/BLOCK_SIZE) + 1;
	printf("testing: newBlockNum %d warpnum %d BLOCK_SIZE %d\n", newBlockNum, warpnum, BLOCK_SIZE);
		initWeightedDegree(commInfoSizeG, commInfoDegreeG, degreeG, edgeRangeG, edgeEndG, edgeWeightG, nv, ne, loadPartG, warpnum); //initialize weighed degree for every node
	
		printf("Testing: before initConstant2ndTerm \n");
		initConstant2ndTerm(degreeG, cst2G, nv, loadPartG, warpnum); //calculate constant
		
		//cudaMemset(commInfoSizeG, 1, nv*sizeof(GraphElem));

		//stopped here, needato cast cst2G to doulbe and copy it to the real
		//constant
		float cst2C;

		printf("Testing: before copying to cst2C\n");
		cudaMemcpy(&cst2C, cst2G, sizeof(float), cudaMemcpyDeviceToHost);
		const2ndTermG = cst2C;		
}		

extern "C" void endLouvainGPU(GraphElem* edgeEndC, GraphWeight* edgeWeightC, GraphElem* edgeEndG, GraphWeight* edgeWeightG, GraphElem* edgeRangeG, GraphElem* degreeG, 
								GraphElem* commInfoSizeG, GraphElem* commInfoDegreeG,  
								GraphElem* inPastCommG, GraphElem* inCurrCommG, GraphElem* inTargetCommG, GraphWeight* sameClusterWeightG, GraphWeight* selfLoopG, double const2ndTermG)
{
	free(edgeEndC);
	free(edgeWeightC);
	cudaFree(edgeEndG);
	cudaFree(edgeWeightG);
	cudaFree(edgeRangeG);
	cudaFree(degreeG);
	cudaFree(commInfoSizeG);
	cudaFree(commInfoDegreeG);
	cudaFree(inPastCommG);
	cudaFree(inCurrCommG);
	cudaFree(inTargetCommG);
	cudaFree(sameClusterWeightG);
	cudaFree(selfLoopG);
}



int	initLoadPartition(const GraphStruct &g, GraphElem* &temp1, GraphElem* &temp2)
{
		const int nv = g.numVertices;
		const int ne = g.numEdges;
	

		int chunk = (double)ne/(double)(BLOCK_SIZE*BLOCK_NUM/32);
		if ( ne < (BLOCK_SIZE*BLOCK_NUM/32) )
		{
		   chunk = 32; 
		}
		
		int chunkcount = 1;
		int maxdegree = 0;		

		for (int i = 0; i < nv; i++ )
		{
			int degree = g.edgeListIndexes[i+1] - g.edgeListIndexes[i];
			if ( degree > maxdegree ) maxdegree = degree;
		}

		chunk = (chunk < maxdegree) ? maxdegree:chunk;
	
		//printf("Testing: degree \n");
		//for ( int i = 0; i < nv; i++  )
		//{ 
			//int cdegree = g.edgeListIndexes[i+1] - g.edgeListIndexes[i]; 
			//printf("%d %d %d\n", g.edgeListIndexes[i+1], g.edgeListIndexes[i], cdegree);
		//}
		//printf("\n");
		int tempchunk = 0;
		for ( int i = 0; i < nv; i++ )
		{
			int cdegree = g.edgeListIndexes[i+1] - g.edgeListIndexes[i];
		
			if ( (tempchunk + cdegree) <= chunk ) 
			{
				tempchunk += cdegree;
			}
			else 
			{
				chunkcount++;
				tempchunk = cdegree;
			}
		}

		int * loadPart = (int *) malloc( (chunkcount+1)*sizeof(GraphElem));
		int * testG;
		int trsize = (chunkcount+1)*sizeof(int);
		//printf("%d trsize chunkcount %d \n", trsize, chunkcount);
		if ( cudaMalloc( (void**)&testG, trsize) != cudaSuccess )
		{
			printf("Error\n");
			exit(1);
		}


		loadPart[0] = 0;
		chunkcount = 1;

		tempchunk = 0;
		for ( int i = 0; i < nv; i++ )
		{
			int cdegree = g.edgeListIndexes[i+1] - g.edgeListIndexes[i];
			if ( (tempchunk + cdegree) <= chunk ) 
			{
				tempchunk += cdegree;
			}
			else 
			{
				loadPart[chunkcount] = i;
				chunkcount++;
				tempchunk = cdegree;
			}
		}
		loadPart[chunkcount] = nv;

		//for ( int i = 0; i < chunkcount+1; i++ )
		//{
			//printf("Testing: loadPart[%d] %d\n", i, loadPart[i]);
		//}		

		//printf("chunk count %d loadPartG %d loadPart %d kind %d sizeof %d (chunkcount+1)*sizeof(GraphElem)  %d\n", chunkcount, testG, loadPart, cudaMemcpyHostToDevice, sizeof(GraphElem), (chunkcount+1)*sizeof(GraphElem) );
		//int * test = (int *) malloc(7*sizeof(int));
		cudaMemcpy(testG, (loadPart), trsize, cudaMemcpyHostToDevice);
		//cudaMemcpy(testG, test, 1, (chunkcount+1)*sizeof(GraphElem), cudaMemcpyHostToDevice);
		cudaThreadSynchronize();
	
	 	cudaError_t code = cudaPeekAtLastError();
		if (code != cudaSuccess)
   	{ 
      fprintf(stderr,"GPUassert: %s\n", cudaGetErrorString(code));
      if (abort) exit(code);
   	}

		temp1 = loadPart;
		temp2 = testG;	
		return chunkcount;
}

double louvainMethodGPU(const GraphStruct &g, CommunityArray &cvect, const double lower)
{
	GraphElem * degreeG; // an int type
	GraphElem* commInfoSizeG;
	GraphElem* commInfoDegreeG;


	GraphElem* inPastCommG;
	GraphElem* inCurrCommG;
	GraphElem* inTargetCommG;


	GraphWeight* sameClusterWeightG;
	GraphElem* eixG;
	GraphElem* axG;

	GraphElem* edgeRangeG;

	GraphElem* edgeEndG;
	GraphWeight* edgeWeightG;
	GraphWeight* selfLoopG;
	
	GraphElem* edgeEndC;
	GraphWeight* edgeWeightC;	

	GraphElem nv, ne;
	nv = g.numVertices;
	ne = g.numEdges;
	

	printf("Entering louvain method, # edge %d, # nodes %d\n", ne, nv);
	//if ( cvect != NULL ) free(cvect);

	GraphElem* inCurrCommC = (GraphElem *)malloc(nv*sizeof(GraphElem));

	GraphElem ** loadPartP;
	GraphElem ** loadPartGP;

	GraphElem * loadPart;
	GraphElem * loadPartG;

	double const2ndTermG;
	const double threshMod = 0.000001;

	//init every array & initialize community info


	loadPartP = &loadPart;
	loadPartGP = &loadPartG;

	printf("%ld before loadPartG\n", loadPartG);

	int warpnum = initLoadPartition(g, loadPart, loadPartG);
	
	printf("%ld after loadPartG\n", loadPartG);

	printf("Testing: before running initLouvain, warpnum %d\n", warpnum);

	

	initLouvainGPU(g, edgeEndC, edgeWeightC, edgeEndG, edgeWeightG, edgeRangeG, degreeG, 
								commInfoSizeG, commInfoDegreeG, 
								inPastCommG, inCurrCommG, inTargetCommG, sameClusterWeightG, selfLoopG, const2ndTermG,
								loadPartG, warpnum);

	//stopped here
	cudaMalloc((void **)&eixG, sizeof(GraphElem)*nv);
	cudaMalloc((void **)&axG, sizeof(GraphElem)*nv);

	double prevpMod, prevMod;
	double *modG;
	cudaMalloc((void **)&modG, sizeof(double));
	prevMod = -1.0; 
	prevpMod = -1.0;

	int iter=0;
	int numIters=0;

	do {
		numIters++;
		//loop init
		/* inTargetto -1*/

		GraphElem * edgeEndGtest, *edgeRangeGtest, *loadPartGtest, *CurrCommGtest, *comminfosizeTest, *comminfodegreeTest;
		float * eixTest, *axTest, *edgeWeightTest;
		double cst1; 
		GraphWeight * sameclusterTest;
		sameclusterTest = (GraphWeight *)malloc(nv*sizeof(GraphWeight));
		edgeEndGtest = (GraphElem * )malloc(ne*sizeof(GraphElem));
		edgeRangeGtest= (GraphElem*)malloc((nv+1)*sizeof(GraphElem));
		comminfosizeTest = (GraphElem*)malloc((nv)*sizeof(GraphElem));
		comminfodegreeTest = (GraphElem*)malloc((nv)*sizeof(GraphElem));
		eixTest= (float*)malloc((nv+1)*sizeof(GraphElem));
		axTest= (float*)malloc((nv+1)*sizeof(GraphElem));
		CurrCommGtest= (GraphElem*)malloc(nv*sizeof(GraphElem));
		edgeWeightTest = (float*)malloc(ne*sizeof(float));

		gpuErrchk(cudaMemcpy(edgeEndGtest, edgeEndG, ne*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(edgeRangeGtest, edgeRangeG, (nv+1)*sizeof(GraphElem) , cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(CurrCommGtest, inTargetCommG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(comminfosizeTest, commInfoSizeG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(comminfodegreeTest, commInfoDegreeG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(eixTest, eixG, nv*sizeof(float), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(axTest, axG, nv*sizeof(float), cudaMemcpyDeviceToHost));
		//for ( int i = 0; i < nv; i++ ) {
			////printf("%d eix %lf ax %lf\n", i, eixTest[i], axTest[i]);
			//printf("Before sorting node %d range in list %d-%d \n", i, edgeRangeGtest[i], edgeRangeGtest[i+1]);
			//for ( int j = edgeRangeGtest[i]; j < edgeRangeGtest[i+1]; j++ )
			//{
				//printf("Test sorting: edge tail %d comm %d\n", edgeEndGtest[j], CurrCommGtest[edgeEndGtest[j]]);
			//}
			//printf("n");
		//}

		printf("Testing: before sortByComm \n");
		sortByComm(edgeRangeG, edgeEndG, inCurrCommG, nv, ne, loadPartG, warpnum, 
				inTargetCommG, eixG, edgeWeightG); //last three arrays used as temporary storage

		gpuErrchk(cudaMemcpy(edgeEndGtest, edgeEndG, ne*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(edgeRangeGtest, edgeRangeG, (nv+1)*sizeof(GraphElem) , cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(CurrCommGtest, inCurrCommG, (nv)*sizeof(GraphElem) , cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(edgeWeightTest, edgeWeightG, ne*sizeof(float), cudaMemcpyDeviceToHost));
		for ( int i = 0; i < nv; i++ ) {
			//printf("%d eix %lf ax %lf\n", i, eixTest[i], axTest[i]);
			printf("After sorting node %d range in list %d-%d \n", i, edgeRangeGtest[i], edgeRangeGtest[i+1]);
			for ( int j = edgeRangeGtest[i]; j < edgeRangeGtest[i+1]; j++ )
			{
				printf("Test sorting: edge tail %d comm %d weight %f\n", edgeEndGtest[j], CurrCommGtest[edgeEndGtest[j]], edgeWeightTest[j]);
			}
			printf("n");
		}




		printf("Testing: before calcEixAx \n");//write a testing function here to check if the elements are indeeded sorted by commid
		calcEixAx(degreeG, inCurrCommG, commInfoDegreeG, edgeRangeG, edgeEndG, edgeWeightG, eixG, axG, nv, ne, loadPartG, warpnum, sameClusterWeightG);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());

		printf("Testing: before calcTarget cst %lf\n", const2ndTermG); //write a testing function to check if eix and ax are calculated correctly, compare it with the original output. 
		calcTarget(edgeRangeG, edgeEndG, edgeWeightG, degreeG, inCurrCommG, inTargetCommG, commInfoSizeG, commInfoDegreeG, nv, ne, eixG, axG, loadPartG, warpnum, const2ndTermG); //check if target is calculated correclty in the first iteration
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());

		gpuErrchk(cudaMemcpy(sameclusterTest, sameClusterWeightG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		for ( int i = 0; i < nv; i++ )
			printf("samecluster for %d is %f\n",i, sameclusterTest[i] );

		printf("Testing: before calcModularity \n");
		calcModularity(inCurrCommG, commInfoSizeG, commInfoDegreeG, sameClusterWeightG, const2ndTermG, modG, prevMod, loadPartG, warpnum, nv);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());


				gpuErrchk(cudaMemcpy(edgeEndGtest, edgeEndG, ne*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(edgeRangeGtest, edgeRangeG, (nv+1)*sizeof(GraphElem) , cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(CurrCommGtest, inTargetCommG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(comminfosizeTest, commInfoSizeG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(comminfodegreeTest, commInfoDegreeG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(eixTest, eixG, nv*sizeof(float), cudaMemcpyDeviceToHost));
		gpuErrchk(cudaMemcpy(axTest, axG, nv*sizeof(float), cudaMemcpyDeviceToHost));
		printf("Testing: Iter %d currMod %lf prevMod %lf cst %lf\n", numIters, prevMod, prevpMod, const2ndTermG);
		//for ( int i = 0; i < nv; i++ ) {
			////printf("%d eix %lf ax %lf\n", i, eixTest[i], axTest[i]);
			//printf("node %d range in list %d-%d \n", i, edgeRangeGtest[i], edgeRangeGtest[i+1]);
			//for ( int j = edgeRangeGtest[i]; j < edgeRangeGtest[i+1]; j++ )
			//{
				//printf("Test sorting: edge tail %d comm %d\n", edgeEndGtest[j], CurrCommGtest[edgeEndGtest[j]]);
			//}
			//printf("n");
		//}
		for ( int i = 0; i < nv; i++ ) {
					printf("Testing: node %d targetcomm %d\n", i, CurrCommGtest[i]);
		}
		printf("\n");
		for ( int i = 0; i < nv; i++ ) {
					printf("Testing: comm %d size %d degree %d\n", i, comminfosizeTest[i], comminfodegreeTest[i]);
		}


		//exit(1);
		printf("Testing: before potential revertComm prevMond %lf prevpMod %lf threshMod %lf \n", prevMod, prevpMod, threshMod);
		if ( (prevMod - prevpMod) < threshMod)
		{//revert back
			revertComm(inCurrCommG, inPastCommG, inTargetCommG, nv);
			gpuErrchk(cudaPeekAtLastError());
			gpuErrchk(cudaDeviceSynchronize());
			break;
		}
		if ( prevMod < lower ) prevMod = lower;
		prevpMod = prevMod;
		
		printf("Testing: before updateTarget \n");
		updateTarget(inCurrCommG, inTargetCommG, inPastCommG, commInfoSizeG, commInfoDegreeG, degreeG, nv, ne);
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaDeviceSynchronize());



		//exit(1);
		//if ( iter == 1 ) exit(1);
		//exit(1);
		iter++;
	} while(true);

	
	printf("\n Moving on to next outer loop iteration\n");

	cudaMemcpy(inCurrCommC, inCurrCommG, nv*sizeof(GraphElem), cudaMemcpyDeviceToHost);

	endLouvainGPU(edgeEndC, edgeWeightC, edgeEndG, edgeWeightG, edgeRangeG, degreeG, 
								commInfoSizeG, commInfoDegreeG,  
								inPastCommG, inCurrCommG, inTargetCommG, sameClusterWeightG, selfLoopG, const2ndTermG);

	cudaFree(eixG);
	cudaFree(axG);

	for ( int i = 0; i < nv; i++) 
	{
		cvect[i] = inCurrCommC[i];
	}

	free(inCurrCommC);
	return prevpMod;
}

double louvainMethodC(const GraphStruct &g, CommunityArray &cvect, const double lower)
{
    CommunityArray pastComm, currComm, targetComm;
    GraphElemArray vdegree;
    GraphElemArray clusterWeight;
    CommArray cinfo;
    CommArray cupdate;
    clmapv = NULL;

    const GraphElem nv = g.numVertices;
    const GraphElem ne = g.numEdges;
    const double threshMod = 0.000001;

    double constantForSecondTerm;
    double prevMod = -1.0;
    double currMod = -1.0;
    int numIters = 0;

    std::cout << g.numVertices << " " << g.numEdges << std::endl;
    initLouvain(g, cvect, pastComm, currComm, vdegree, clusterWeight, cinfo, cupdate,
            constantForSecondTerm);
    targetComm = (CommunityArray)malloc(sizeof(GraphElem)*nv);
    double total_time_cuda = 0;

    do {
        numIters++;

        GraphElem maxPartition = 7000;
        double time1 = mytimer();
            louvain_execution_cuda(g.numVertices, g.numEdges, g.edgeList, 
                    g.edgeListIndexes, currComm, cinfo, maxPartition, (float)constantForSecondTerm, vdegree);
        std::cout << std::endl;
        double time2 = mytimer();
        total_time_cuda += time2-time1;
#pragma omp parallel default(none), shared(clusterWeight, cupdate, g, currComm, vdegree, cinfo, targetComm), firstprivate(constantForSecondTerm)
        {
            if(!clmapv)
                clmapv = (ClusterLocalMapA)malloc(sizeof(KeyValueComm)*nv);//not efficient

#pragma omp for schedule(static)
            for (GraphElem i = 0L; i < nv; i++) {
                clusterWeight[i] = 0L;
                cupdate[i].degree = 0L;
                cupdate[i].size = 0L;
            }

#pragma omp for schedule(guided)
            for (GraphElem i = 0L; i < nv; i++)
                executeLouvainIteration(i, g, currComm, 
                        vdegree, cinfo, cupdate, constantForSecondTerm,
                        clusterWeight, true, &targetComm);
        }

        currMod = computeModularity(g, cinfo, clusterWeight, currComm, constantForSecondTerm, false);
        //if not enough difference dont change and break
        if ((currMod - prevMod) < threshMod) {
            for(int i = 0; i < nv; i++){
                currComm[i] = pastComm[i];
            }
            break;
        }

        prevMod = currMod;
        if (prevMod < lower)
            prevMod = lower;

#pragma omp parallel default(none), shared(cinfo, cupdate, pastComm, currComm, targetComm)
        {
            updatecinfo(cinfo, cupdate, nv);

#pragma omp for schedule(static)
            for (GraphElem i = 0L; i < nv; i++) {
                GraphElem tmp;

                tmp = pastComm[i];
                pastComm[i] = currComm[i];
                currComm[i] = targetComm[i];
                targetComm[i] = tmp;
            }
        }
			printf("\n");
    } while(true);
    std::cout << "GPU TIME:" << total_time_cuda << std::endl;

    for(int i = 0 ; i < nv; i++){
        cvect[i] = currComm[i];
    }
    free(clusterWeight);
    free(vdegree);
    free(pastComm);
    free(currComm);
    free(targetComm);
    free(cinfo);
    free(cupdate);
    return prevMod;
} // louvainMethod

void sumVertexDegree(const GraphStruct &g, GraphElemArray &vdegree, CommArray &cinfo)
{
    const GraphElem nv = g.numVertices;

#pragma omp parallel for default(none), shared(g, vdegree, cinfo), schedule(guided)
    for (GraphElem i = 0; i < nv; i++) {
        GraphElem e0, e1;
        GraphElem tw = 0L;

        getEdgeRangeForVertex(&g,i, e0, e1);

        for (GraphElem k = e0; k < e1; k++) {
            const Edge &edge = g.edgeList[k];

            tw += edge.weight;
        }

        vdegree[i] = tw;
        cinfo[i].degree = tw;
        cinfo[i].size = 1L;
    }
} // sumVertexDegree

double calcConstantForSecondTerm(const GraphElemArray &vdegree, const size_t vsz)
{
    GraphElem totalEdgeWeightTwice = 0L;
    GraphElem localWeight = 0L;

    //const size_t vsz = vdegree.size();

#pragma omp parallel for default(none), shared(vdegree), reduction(+: localWeight), schedule(static)
    for (GraphElem i = 0; i < vsz; i++)
        localWeight += vdegree[i];

    totalEdgeWeightTwice = localWeight;

    return (1.0 / static_cast<double>(totalEdgeWeightTwice));
} // calcConstantForSecondTerm

void initComm(CommunityArray &pastComm, CommunityArray &currComm, size_t csz)
{
#pragma omp parallel for default(none), shared(pastComm, currComm, csz), schedule(static)
    for (GraphElem i = 0L; i < csz; i++) {
        pastComm[i] = i;
        currComm[i] = i;
    }
} // initComm

GraphElem buildLocalMapCounter(const GraphElem e0, const GraphElem e1,
        ClusterLocalMapA &clmap, DoubleArray &counter,
        const GraphStruct &g, CommunityArray &currComm,
        const GraphElem vertex)
{
    GraphElem numUniqueClusters = 1L;
    GraphElem selfLoop = 0L;

    for (GraphElem j = e0; j < e1; j++) {
        const Edge &edge = g.edgeList[j];

        if (edge.tail == vertex)
            selfLoop += edge.weight;

        KeyValueComm kv;

        kv.commId = currComm[edge.tail];
        kv.cnt = numUniqueClusters;

        // storedAlready = clmap.find(currComm[edge.tail]);
        // ClusterLocalMapV::const_iterator bound = clmap.cbegin() + e0 + vertex + clsz;
        //ClusterLocalMapV::const_iterator bound = clmap.cbegin() + clsz;
        int bound = clsz;

        // storedAlready = std::find(clmap.cbegin() + e0 + vertex, bound, kv);
        int storedAlready;
        for(storedAlready = 0; storedAlready < bound; storedAlready++){
            if(clmap[storedAlready] == kv)
                break;
        }
        //storedAlready = std::find(clmap.cbegin(), bound, kv);
        if (storedAlready != bound)
            counter[clmap[storedAlready].cnt] += edge.weight;
        else {
            // clmap[currComm[edge.tail]] = numUniqueClusters;
            //       if ((e0 + vertex + clsz) >= (e1 + vertex + 1)) {
            // 	std::cout << "Out of bounds: e0: " << e0 << ", e1: " << e1 << ", clsz: " << clsz <<
            // 	  std::endl;
            // 	std::cout << "storedAlready: " << storedAlready->commId << ", bound: " <<
            // 	  bound->commId << std::endl;
            // 	std::cout << "Looking for: " << kv.commId << std::endl;
            // 	for (auto iter = clmap.cbegin() + e0 + vertex; iter != bound; iter++)
            // 	  std::cout << iter->commId << " ";
            // 	std::cout << std::endl;
            //       }

            assert((e0 + vertex + clsz) < (e1 + vertex + 1));
            clmap[clsz] = kv;
            counter[clsz] = static_cast<double>(edge.weight);
            clsz++;
            numUniqueClusters++;
        }
    }

    return selfLoop;
} // buildLocalMapCounter

GraphElem getMaxIndex(const ClusterLocalMapA &clmap, const DoubleArray &counter,
        const GraphElem selfLoop, const CommArray &cinfo,
        const GraphElem vdegree, const GraphElem currComm,
        const double constant, const GraphElem e0, const GraphElem e1,
        const GraphElem vertex)
{
    int storedAlready, bound;
    GraphElem maxIndex = currComm;
    double curGain = 0.0, maxGain = 0.0;
    double eix = counter[0] - static_cast<double>(selfLoop);
    double ax = cinfo[currComm].degree - vdegree;
    double eiy = 0.0, ay = 0.0;
    std::cout << "comm " << currComm << " counter[0] " << counter[0] << std::endl;

    // storedAlready = clmap.begin() + e0 + vertex;
    storedAlready = 0;
    // bound = clmap.begin() + e0 + clsz + vertex;
    bound = clsz;

    do {
        if (currComm != clmap[storedAlready].commId) {
            ay = cinfo[clmap[storedAlready].commId].degree;
            eiy = counter[clmap[storedAlready].cnt];
            curGain = 2.0 * (eiy - eix) - 2.0 * vdegree * (ay - ax) * constant;
            std::cout << "(" <<  vertex << ", " << clmap[storedAlready].commId << ", " << curGain 
                << ") ";

            if ((curGain > maxGain) ||
                    ((curGain == maxGain) && (curGain != 0.0) && (clmap[storedAlready].commId < maxIndex))) {
                maxGain = curGain;
                maxIndex = clmap[storedAlready].commId;
            }
        }
        storedAlready++;
    } while (storedAlready != bound);

    std::cout << "\nv " << vertex << " max " << maxIndex << " " << maxGain << std::endl;
    if ((cinfo[maxIndex].size == 1) && (cinfo[currComm].size == 1) && (maxIndex > currComm))
        maxIndex = currComm;
    std::cout << "v " << vertex << " max " << maxIndex << " " << maxGain << std::endl;

    return maxIndex;
} // getMaxIndex

void initLouvain(const GraphStruct &g, const CommunityArray &cvect, CommunityArray &pastComm,
        CommunityArray &currComm, GraphElemArray &vdegree,
        GraphElemArray &clusterWeight, CommArray &cinfo, CommArray &cupdate,
        double &constantForSecondTerm)
{
    const GraphElem nv = g.numVertices;
    const GraphElem ne = g.numEdges;

    vdegree = (GraphElemArray)malloc(sizeof(GraphElem)*nv);
    cinfo = (CommArray)malloc(sizeof(Comm)*nv);
    cupdate = (CommArray)malloc(sizeof(Comm)*nv);
    pastComm = (CommunityArray)malloc(sizeof(GraphElem)*nv);
    currComm = (CommunityArray)malloc(sizeof(GraphElem)*nv);
    clusterWeight = (GraphElemArray)malloc(sizeof(GraphElem)*nv);

    sumVertexDegree(g, vdegree, cinfo);
    constantForSecondTerm = calcConstantForSecondTerm(vdegree, nv);

    for(int i = 0; i < nv; i++){
        currComm[i] = cvect[i];
    }
    initComm(pastComm, currComm, (size_t)nv);
} // initLouvain

void executeLouvainIteration(const GraphElem i, const GraphStruct &g,
        CommunityArray &currComm, const GraphElemArray &vdegree,
        const CommArray &cinfo, CommArray &cupdate,
        const double constantForSecondTerm, GraphElemArray &clusterWeight,
        const bool updateWeight, CommunityArray *const targetComm)
{
    int localTarget = -1;
    GraphElem e0, e1, selfLoop = 0L;
    // ClusterLocalMapV clmap;
    DoubleArray counter = (DoubleArray)malloc(sizeof(double)*g.numVertices);//inefficent

    getEdgeRangeForVertex(&g, i, e0, e1);
    clsz = 0;
    const GraphElem deg = (e1 - e0);


    /*
       if (clmapv->size() < (deg + 1))
       clmapv->resize(deg + 1);
       */

    if (e0 != e1) {
        // clmap.reserve(e1 - e0);

        KeyValueComm kv;

        kv.commId = currComm[i];
        kv.cnt = 0;

        // clmapv.at(e0 + i + clsz) = kv;
        clmapv[clsz] = kv;
        clsz++;
        // clmap.push_back(kv);
        counter[0] = 0.0;

        selfLoop = buildLocalMapCounter(e0, e1, clmapv, counter, g, currComm, i);

        if (updateWeight)
            clusterWeight[i] += static_cast<GraphElem>(counter[0]);
        localTarget = getMaxIndex(clmapv, counter, selfLoop, cinfo, vdegree[i], currComm[i],
                constantForSecondTerm, e0, e1, i);
    }
    else
        localTarget = -1;

    if ((localTarget != currComm[i]) && (localTarget != -1)) {
#pragma omp atomic update
        cupdate[localTarget].degree += vdegree[i];
#pragma omp atomic update
        cupdate[localTarget].size++;
#pragma omp atomic update
        cupdate[currComm[i]].degree -= vdegree[i];
#pragma omp atomic update
        cupdate[currComm[i]].size--;
    }
    if (!targetComm)
        currComm[i] = localTarget;
    else
        (*targetComm)[i] = localTarget;
    free(counter);
} // executeLouvainIteration

void updatecinfo(CommArray &cinfo, const CommArray &cupdate, size_t csz)
{
    //size_t csz = cinfo.size();
    //maybe not all needed csz != nv check later

#pragma omp for schedule(static)
    for (GraphElem i = 0L; i < csz; i++) {
        cinfo[i].size += cupdate[i].size;
        cinfo[i].degree += cupdate[i].degree;
    }
    for(int j = 0; j < csz; j++){
        std::cout << "v " << j << " size " << cinfo[j].size  
            << " degree " << cinfo[j].degree << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;

} // updatecinfo

double computeModularity(const GraphStruct &g, CommArray &cinfo, GraphElemArray &clusterWeight,
        const CommunityArray &currComm, const double constantForSecondTerm,
        const bool useColoring)
{
    const GraphElem nv = g.numVertices;

    if (useColoring) {
#pragma omp parallel default(none), shared(clusterWeight, g, currComm)
        {
#pragma omp for schedule(static)
            for (GraphElem i = 0L; i < nv; i++)
                clusterWeight[i] = 0L;

#pragma omp for schedule(guided), nowait
            for (GraphElem i = 0L; i < nv; i++) {
                GraphElem e0, e1;

                getEdgeRangeForVertex(&g, i, e0, e1);

                for (GraphElem j = e0; j < e1; j++) {
                    const Edge &edge = g.edgeList[j];

                    if (currComm[edge.tail] == currComm[i])
                        clusterWeight[i] += static_cast<GraphElem>(edge.weight);
                }
            }
        }
    }

    double e_xx = 0.0, a2_x = 0.0;

    //assert((cinfo.size() == nv) && (clusterWeight.size() == nv));

#pragma omp parallel for default(none), shared(clusterWeight, cinfo), reduction(+: e_xx), reduction(+: a2_x), schedule(static)
    for (GraphElem i = 0L; i < nv; i++) {
        e_xx += clusterWeight[i];
        a2_x += static_cast<double>(cinfo[i].degree) * static_cast<double>(cinfo[i].degree);
    }

    double currMod = (e_xx * constantForSecondTerm) - (a2_x * constantForSecondTerm *
            constantForSecondTerm);

    std::cout << "e_xx: " << e_xx << ", a2_x: " << a2_x << ", currMod: " << currMod << std::endl;

    return currMod;
} // computeModularity

inline void cleanCWandCU(const GraphElem nv, GraphElemArray &clusterWeight, CommArray &cupdate)
{
#pragma omp for schedule(static)
    for (GraphElem i = 0L; i < nv; i++) {
        clusterWeight[i] = 0L;
        cupdate[i].degree = 0L;
        cupdate[i].size = 0L;
    }
} // cleanCWandCU
