#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <cassert>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <string>

#include <omp.h>

#include "buildNextPhase.h"
#include "distanceOneVertexColoring.h"
#include "graph.h"
#include "loadBinary.h"
#include "loadDimacs.h"
#include "loadMetis.h"
#include "loadSimple.h"
#include "louvainMethod.h"
#include "louvainMethodC.h"
#include "timer.h"
#include "test.cuh"

static std::string inputFileName;

static bool matrixMarketFormat = false;
static bool dimacsFormat = false;
static bool pajekOnceFormat = false;
static bool pajekTwiceFormat = false;
static bool metisFormat = false;
static bool simpleFormat = false;
static bool binaryFormat = false;
static bool coloring = false;
static bool output = false;
static bool vertexFollowing = false;
static bool newVersion = false;

static void parseCommandLine(const int argc, char * const argv[]);

int main(int argc, char *argv[])
{
  parseCommandLine(argc, argv);

  // Only DIMACS, Metis, simple & binary formats supported for now
  assert(dimacsFormat || metisFormat || simpleFormat || binaryFormat);

  Graph *g = NULL, *gnew = NULL;

  rusage rus;

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  if (dimacsFormat)
    loadDimacsFile(g, inputFileName);
  else if (metisFormat)
    loadMetisFile(g, inputFileName);
  else if (simpleFormat)
    loadSimpleFile(g, inputFileName);
  else if (binaryFormat)
    loadBinaryFile(g, inputFileName);
  else {
    std::cerr << "Format not specified correctly!" << std::endl;
    if (g)
      delete g;
    return 1;
  }

  int nthreads = omp_get_max_threads();
  //int nthreads = 1;
  //omp_set_num_threads(1);

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Running on: " << nthreads << " threads" << std::endl;
  std::cout << "Loaded graph from file: " << inputFileName << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  assert(g);
    ColorVector colors;
  ColorElem numColors;
  double t0, t1, t2, t3, ti = 0.0;
  double ts,te, tl=0.0; //ts: temp start time, te, temp end time, tl, total louvain time

  t0 = mytimer();
  if (coloring)
    numColors = distanceOneVertexColoring(*g, colors) + 1;
  t1 = mytimer();

  getrusage(RUSAGE_SELF, &rus);
  std::cout << "Coloring time: " << (t1 - t0) << std::endl;
  std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    std::endl;

  const double threshold = 0.000001;
  const GraphElem nv = g->getNumVertices();

  double prevMod = -1.0;
  double currMod = -1.0;

  int phase = 1;
  CommunityVector cvect(nv);
  CommunityVector vassign(nv);

  if (output) {
    GraphElem i = 0;

    //std::for_each(vassign.begin(), vassign.end(),
//		  [&i] (GraphElem &va) { va = i++; });
    for(std::vector<GraphElem>::iterator temp = vassign.begin(); temp != vassign.end(); ++temp) {
	*temp = i++;
    }
  }

  tl = 0.0;

  do {
    t0 = mytimer();
#pragma omp parallel for default(none), shared(cvect), schedule(static)
    for (GraphElem i = 0L; i < nv; i++)
      cvect[i] = -1L;

    prevMod = currMod;

    ts = mytimer();
    if (coloring) {
      currMod = louvainWithDistOneColoring(*g, cvect, colors, numColors, currMod, 0.01);
      coloring = false;
    }
    else{   
        if(newVersion){
            CommunityArray cv = (CommunityArray)&(cvect[0]);
            GraphStruct G;
            G.numVertices = g->numVertices;

            //std::cout << G.numVertices << " !!!!!\n";
            G.numEdges = g->numEdges;
            G.edgeListIndexes = &(g->edgeListIndexes[0]);
            G.edgeList= &(g->edgeList[0]);
            //for(int i = 0; i< G.numVertices; i++){
            //    std::cout << G.edgeListIndexes[i] << " ";
            //}
            //std::cout << std::endl;


            //printf("size of graph elem %d\n", sizeof(GraphElem));
            currMod = louvainMethodGPU(G, cv, currMod);
            g->numVertices = G.numVertices;
            g->numEdges = G.numEdges;
            g->edgeListIndexes = EdgeIndexes(G.edgeListIndexes, G.edgeListIndexes+G.numVertices+1);
            g->edgeList = EdgeList(G.edgeList, G.edgeList+G.numEdges);
        }
        else{
          currMod = louvainMethod(*g, cvect, currMod);
        }
    }
    te = mytimer();

    //std::cout << "currMod: " << currMod << ", prevMod: " << prevMod << std::endl;
    //std::cout << "Louvain time: " << (t3 - t2) << std::endl;
    tl += (te  - ts);
   // t2 = mytimer();
    GraphElem numClusters = renumberClustersContiguously(cvect);
    //t3 = mytimer();

    if (output)
#pragma omp parallel for default(none), shared(cvect, vassign), schedule(static)
      for (GraphElem i = 0L; i < nv; i++)
	vassign.at(i) = cvect.at(vassign.at(i));

   // std::cout << "numClusters: " << numClusters << ", time: " << (t3 - t2) << std::endl;

    if ((currMod - prevMod) < threshold)
      break;

    getrusage(RUSAGE_SELF, &rus);
    //std::cout << "Next phase: " << (phase + 1) << std::endl;
    //std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
    //  std::endl;
    phase++;

    //t2 = mytimer();
    buildNextLevelGraph(*g, gnew, cvect, numClusters);
    //t3 = mytimer();
    //std::cout << "Build next level time: " << (t3 - t2) << std::endl;
    cvect.resize(numClusters);

		

    delete g;
    g = gnew;
    gnew = NULL;

    t1 = mytimer();

    ti += (t1 - t0);

    getrusage(RUSAGE_SELF, &rus);
    //std::cout << "Iteration time: " << (t1 - t0) << std::endl;
    //std::cout << "Memory used: " << (static_cast<double>(rus.ru_maxrss) * 1024.0) / 1048576.0 <<
     // std::endl;
  } while (true);

  if (g)
    delete g;
  if (gnew)
    delete gnew;

  std::cout << "Total iteration time: " << ti << std::endl;
  std::cout << "Total louvain time: " << tl << std::endl;

  if (output) {
    std::ofstream ofs("clusters.out");
    //std::for_each(vassign.begin(), vassign.end(),
	//	  [&ofs] (const GraphElem &ga) { ofs << ga << std::endl; });
    for(std::vector<GraphElem>::iterator temp = vassign.begin(); temp != vassign.end(); ++temp) {
		ofs << *temp << std::endl;
	}
  }

  return 0;
} // main

void parseCommandLine(const int argc, char * const argv[])
{
  int ret;

  while ((ret = getopt(argc, argv, "f:mdptesbcovz")) != -1) {
    switch (ret) {
    case 'f':
      inputFileName.assign(optarg);
      break;
    case 'm':
      matrixMarketFormat = true;
      break;
    case 'd':
      dimacsFormat = true;
      break;
    case 'p':
      pajekOnceFormat = true;
      break;
    case 't':
      pajekTwiceFormat = true;
      break;
    case 'e':
      metisFormat = true;
      break;
    case 's':
      simpleFormat = true;
      break;
    case 'b':
      binaryFormat = true;
      break;
    case 'c':
      coloring = true;
      break;
    case 'o':
      output = true;
      break;
    case 'v':
      vertexFollowing = true;
      break;
    case 'z':
      newVersion = true;
      break;
    default:
      assert(0 && "Should not reach here!!");
      break;
    }
  }

  if ((matrixMarketFormat || dimacsFormat || pajekOnceFormat || pajekTwiceFormat ||
       metisFormat || simpleFormat || binaryFormat) == false) {
    std::cerr << "Must select a file format for the input file!" << std::endl;
    exit(EXIT_FAILURE);
  }
  const bool fileFormat[] = { matrixMarketFormat, dimacsFormat, pajekOnceFormat,
			      pajekTwiceFormat, metisFormat, simpleFormat, binaryFormat };
  const int numFormats = sizeof(fileFormat) / sizeof(fileFormat[0]);

  int numTrue = 0;
  for (int i = 0; i < numFormats; i++)
    if (numTrue == 0 && fileFormat[i])
      numTrue++;
    else if (numTrue > 0 && fileFormat[i]) {
      std::cerr << "Must select a SINGLE file format for the input file!" << std::endl;
      exit(EXIT_FAILURE);
    }

  if (inputFileName.empty()) {
    std::cerr << "Must specify a file name with -f" << std::endl;
    exit(EXIT_FAILURE);
  }
} // parseCommandLine
